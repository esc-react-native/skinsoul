/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, Text, View, Modal} from 'react-native';
import Button from './src/components/button';
import Input from './src/components/input';
import {Auth} from 'aws-amplify';
import SplashScreen from 'react-native-splash-screen';


export default class Authentication extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        email: '',
        password: '',
        confirmPassword: '',
        confirmationCode: '',
        modalVisible: false,
        selectedIndex: 0,
        isLoading: true,
        session: null,
    };

    this.getSession();
  }

  getSession = async () => {
    let session;

    try{
      session = await Auth.currentAuthenticatedUser();
      //this.setState({session});
      console.log("There is a Session??");
      console.log(session);
      this.setState({session, isLoading: false});



      if(session.signInUserSession.accessToken.jwtToken)
        this.props.navigation.navigate('Home');

      return session;
    }catch(_err){
      console.log("error");
      console.log(_err);
    }
  }

  componentDidMount = () => {
  }

  handleSignIn = async () => {
    const { email, password } = this.state;
    console.log("SIGN");

    try{
      const user = await Auth.signIn(email, password);
      console.log("Login Session Result");
      console.log(user);

      if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
        const {requiredAttributes} = user.challengeParam;
        //const {username, email, phone_number} = getInfoFromUserInput();
        const loggedUser = await Auth.completeNewPassword(
          user,               // the Cognito User Object
          password,           // the new password
        );

        console.log("Login Session Result");
        console.log(loggedUser);
      }

        this.props.navigation.navigate('Home');
      } catch (err) {
        console.log("ERROR");
        if (err.code === 'UserNotConfirmedException') {
          alert("UserNotConfirmedException");
          console.log('UserNotConfirmedException');
          console.log(err);
            // The error happens if the user didn't finish the confirmation step when signing up
            // In this case you need to resend the code and confirm the user
            // About how to resend the code and confirm the user, please check the signUp part
        } else if (err.code === 'PasswordResetRequiredException') {
          alert("PasswordResetRequiredException");
          console.log('PasswordResetRequiredException');
          console.log(err);

            // The error happens when the password is reset in the Cognito console
            // In this case you need to call forgotPassword to reset the password
            // Please check the Forgot Password part.
        } else if (err.code === 'NotAuthorizedException') {
          alert("NotAuthorizedException");
          console.log("NotAuthorizedException");
          console.log(err);

            // The error happens when the incorrect password is provided
        } else if (err.code === 'UserNotFoundException') {
          alert("UserNotFoundException");
          console.log("UserNotFoundException");
          console.log(err);

            // The error happens when the supplied username/email does not exist in the Cognito user pool
        } else {
            console.log(err);
        }
    }
  }

  handleSignUp = () => {
    // alert(JSON.stringify(this.state));
    const {email, password, confirmPassword} = this.state;
    // Make sure passwords match
    if (password === confirmPassword) {
      Auth.signUp({
        username: email,
        password,
        attributes: {email},
      })
        // On success, show Confirmation Code Modal
        .then(() => this.setState({modalVisible: true}))
        // On failure, display error in console
        .catch(err => console.log(err));
    } else {
      alert('Passwords do not match.');
    }
  };

  handleConfirmationCode = () => {
    const {email, confirmationCode} = this.state;
    Auth.confirmSignUp(email, confirmationCode, {})
      .then(() => {
        this.setState({modalVisible: false});
        this.props.navigation.navigate('Home');
      })
      .catch(err => console.log(err));
  };

  render() {
    return (
      <View style={styles.container}>

        <Text>{JSON.stringify(this.state.session)}</Text>
        <Input
          label="Email"
          //leftIcon={{type: 'font-awesome', name: 'envelope'}}
          placeholder="my@email.com"
          onChangeText={
            // Set this.state.email to the value in this Input box
            value => this.setState({email: value})
          }
        />
        <Input
          label="Password"
          //leftIcon={{type: 'font-awesome', name: 'lock'}}
          placeholder="p@ssw0rd123"
          onChangeText={
            // Set this.state.password to the value in this Input box
            value => this.setState({password: value})
          }
          secureTextEntry
        />
        <Input
          label="Confirm Password"
          //leftIcon={{type: 'font-awesome', name: 'lock'}}
          placeholder="p@ssw0rd123"
          onChangeText={
            // Set this.state.confirmPassword to the value in this Input box
            value => this.setState({confirmPassword: value})
          }
          secureTextEntry
        />
        <Button title="Submit" onPress={this.handleSignUp} />
        <Modal visible={this.state.modalVisible}>
          <View style={styles.container}>
            <Input
              label="Confirmation Code"
              leftIcon={{type: 'font-awesome', name: 'lock'}}
              onChangeText={
                // Set this.state.confirmationCode to the value in this Input box
                value => this.setState({confirmationCode: value})
              }
            />
            <Button title="Submit" onPress={this.handleConfirmationCode} />
          </View>
        </Modal>

        <Input
          label="Email"
          leftIcon={{type: 'font-awesome', name: 'envelope'}}
          onChangeText={
            // Set this.state.email to the value in this Input box
            value => this.setState({email: value})
          }
          placeholder="my@email.com"
        />
        <Input
          label="Password"
          leftIcon={{type: 'font-awesome', name: 'lock'}}
          onChangeText={
            // Set this.state.email to the value in this Input box
            value => this.setState({password: value})
          }
          placeholder="p@ssw0rd123"
          secureTextEntry
        />
        <Button title="Submit" onPress={this.handleSignIn} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
