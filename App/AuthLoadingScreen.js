import React from 'react';
import {View, Text} from 'react-native';
// import Loading from '../src/Components/Loading';
// import SplashScreen from 'react-native-splash-screen'

import AsyncStorage from '@react-native-community/async-storage';
import {Auth} from 'aws-amplify';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    try {
      let sessionToken = await AsyncStorage.getItem('sessionToken');
      let session = await Auth.currentAuthenticatedUser();

      console.log('* sessionToken *');
      console.log(sessionToken);

      console.log('* session *');
      console.log(session);

      if (sessionToken) {
        this.props.navigation.navigate('App');
      } else {
        this.props.navigation.navigate('Auth');
      }


    } catch (_err) {
      console.log('Error Loading Session');
      console.log(_err);
      this.props.navigation.navigate('Auth');
    }
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <Text>Loading...</Text>
        {/* <Loading /> */}
      </View>
    );
  }
}
