import React from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import Styles from './Styles/TabBarComponent';

export default class TabBarComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
    };
  }

  render() {
    const {
      navigation,
      renderIcon,
      activeTintColor,
      inactiveTintColor,
    } = this.props;
    const {routes} = navigation.state;

    return (
      <SafeAreaView style={Styles.tabbar}>
        {routes &&
          routes.map((route, index) => {
            const focused = index === navigation.state.index;
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            return (
              <TouchableWithoutFeedback
                key={route.key}
                style={Styles.tab}
                onPress={() => {
                  if (route.key == 'Settings') {
                    this.setState({modalVisible: true});
                  } else {
                    navigation.navigate(route);
                  }
                }}>
                <View style={Styles.tab}>
                  {renderIcon({route, index, focused, tintColor})}
                  <Modal
                    visible={false}
                    animationIn="fadeInUp"
                    animationOut="fadeOutDown"
                    backdropOpacity={0.5}
                    onBackdropPress={() => this.setState({modalVisible: false})}
                    onBackButtonPress={() =>
                      this.setState({modalVisible: false})
                    }
                    style={Styles.bottomModal}
                    useNativeDriver={true}>
                    <View style={Styles.modal}>
                        <Text>Modal Content</Text>
                    </View>
                  </Modal>
                </View>
              </TouchableWithoutFeedback>
            );
          })}
      </SafeAreaView>
    );
  }
}
