import React from 'react';
import {View, Text, Image, Linking} from 'react-native';
import {Colors} from '../src/Styles';

import Icon from 'react-native-vector-icons/FontAwesome';
import IconI from 'react-native-vector-icons/Ionicons';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {StackActions, NavigationActions} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';

//Multilanguage
import i18 from '../src/Localize/I18n';

import AuthLoadingScreen from './AuthLoadingScreen';

import ScreenLanguage from '../src/Containers/Language/ScreenLanguage';

import ScreenLogin from '../src/Containers/Login/ScreenLogin';
import ScreenHome from '../src/Containers/Home/ScreenHome';

import ScreenPatientNumber from '../src/Containers/Home/ScreenPatientNumber';
import ScreenSettings from '../src/Containers/Settings/ScreenSettings';
import ScreenSearch from '../src/Containers/Help/ScreenHelp';
import ScreenConsultation from '../src/Containers/Home/ScreenConsultation';
import ScreenPhotoInfo from '../src/Containers/Home/ScreenPhotoInfo';

import {handlerLogout} from '../src/Handlers/Logout';
import {HandleLogout} from '../src/libs/awsAuth';

import TabBarComponentLogout from './TabBarComponent';

//
// Application Stack Logued Screens
const AuthStack = createStackNavigator(
  {
    Landing: {
      screen: ScreenLogin,
      navigationOptions: {
        headerTitle: 'Sign In',
        initialRouteName: 'Login',
        headerMode: 'none',
      },
    },
    Login: {
      screen: ScreenLogin,
      //screen: ScreenPhotoInfo,
      navigationOptions: {
        header: null,
      },
    },
    CreateAccount: {
      screen: ScreenLogin,
      navigationOptions: {
        headerTitle: 'Create Account',
      },
    },
    ForgotPassword: {
      screen: ScreenLogin,
      navigationOptions: {
        headerTitle: 'Forgot Password',
      },
    },
    ResetPassword: {
      screen: ScreenLogin,
      navigationOptions: {
        headerTitle: 'Reset Password',
      },
    },
  },
  {
    initialRouteName: 'Login',
  },
);

//!

const HomeStack = createStackNavigator({
  Home: {
    screen: ScreenHome,
    //screen: ScreenPhotoInfo,
    navigationOptions: {
      header: null,
    },
  },
  PatientNumber: {
    screen: ScreenPatientNumber,
    navigationOptions: {
      header: null,
    },
  },
});

const HelpStack = createStackNavigator({
  Search: {
    screen: ScreenSearch,
    navigationOptions: {
      header: null,
    },
  },
  Details: {
    screen: ScreenSearch,
    navigationOptions: {
      header: null,
    },
  },
});

const LanguageStack = createStackNavigator({
  Search: {
    screen: ScreenLanguage,
    navigationOptions: {
      header: null,
    },
  },
  Details: {
    screen: ScreenSearch,
    navigationOptions: {
      header: null,
    },
  },
});

const SettingStack = createStackNavigator({
  Setting: {
    screen: ScreenSettings,
    navigationOptions: {
      header: null,
    },
  },
  Logout: {
    screen: ScreenSettings,
    navigationOptions: {
      headerMode: null,
      mode: 'modal',
    },
  },
});

//!Home
const MainTabs = createBottomTabNavigator(
  {
    Language: {
      screen: LanguageStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="language" color={tintColor} size={24} />
        ),
      }),
    },
    Home: {
      screen: HomeStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" color={tintColor} size={24} />
        ),
      }),
    },
    Help: {
      screen: HelpStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="question-circle-o" color={tintColor} size={24} />
        ),
        tabBarOnPress: ({navigation, scene, jumpToIndex}) => {
          Linking.openURL(
            'https://api.whatsapp.com/send?phone=18298665388&text=Hola,%20mi%20consulta%20es...',
          );
        },
      }),
    },
    Setting: {
      screen: SettingStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <IconI name="md-exit" color={tintColor} size={24} />
        ),
        tabBarLabel: 'Logout',
        tabBarOnPress: ({navigation, scene, jumpToIndex}) => {
          HandleLogout();

          const resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'AuthStack',
                params: {},
              }),
            ],
          });

          const goLogin = NavigationActions.navigate({
            routeName: 'Login',
            params: {},
          });

          navigation.dispatch(resetAction);
          navigation.dispatch(goLogin);
        },
      }),
    },
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      showLabel: true, // hide labels
      activeTintColor: '#5E9FB4', // active icon color
      inactiveTintColor: '#6D6D6D', // inactive icon color
      style: {
        backgroundColor: '#FFFFFF', // TabBar background
      },
    },
    //tabBarComponent: TabBarComponentLogout,
  },
);

const SettingsStack = createStackNavigator({
  SettingsList: {
    screen: ScreenSettings,
    navigationOptions: {
      headerTitle: 'Settings List',
    },
  },
  Profile: {
    screen: ScreenSettings,
    navigationOptions: {
      headerTitle: 'Profile',
    },
  },
});

const ConsultationStack = createStackNavigator({
  Consultation: {
    screen: ScreenConsultation,
    navigationOptions: {
      header: null,
      tabBarVisible: false,
    },
  },
  PhotoInfo: {
    screen: ScreenPhotoInfo,
    navigationOptions: {
      header: null,
      tabBarVisible: false,
    },
  },
});

const MainDrawer = createDrawerNavigator(
  {
    MainTabs: MainTabs,
    Consultation: ConsultationStack,
    Settings: SettingsStack,
  },
  {
    initialRouteName: 'MainTabs',
    headerMode: 'none',
  },
);

const AppModalStack = createStackNavigator(
  {
    App: MainDrawer,
    PhotoInfo: {
      screen: ScreenPhotoInfo,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

const AppStack = createStackNavigator(
  {
    MainTabs: MainTabs,
    Consultation: ConsultationStack,
    Settings: SettingsStack,
  },
  {
    initialRouteName: 'MainTabs',
    headerMode: 'none',
  },
);

//
const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: {
      screen: AuthLoadingScreen,
    },
    App: AppStack,
    Auth: {
      screen: AuthStack,
    },
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
