import {Storage} from 'aws-amplify';

export async function s3Upload(file, fileType) {
  console.log('FILE');
  console.log(file);
  const filename = `${Date.now()}-${file.name}`;

  const stored = await Storage.vault.put(filename, file, {
    progressCallback(progress) {
      console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
    },
  });

  console.log('return s3put');
  console.log(JSON.stringify(stored));

  return stored.key;
}

export async function s3Get(filename) {
  try {
    let returnValue = await Storage.vault.get(filename);
    console.log('return s3Get');
    console.log(returnValue);

    return returnValue;
  } catch (e) {
    return null;
  }

  // console.log('return s3Get');
  // console.log(returnValue);
}
