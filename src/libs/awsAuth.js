import {StackActions, NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {Auth} from 'aws-amplify';

//.
export async function HandleLogout(file, fileType) {
  console.log('SignOut...');

  await AsyncStorage.removeItem('sessionToken');
  await Auth.signOut();

  return;
}
