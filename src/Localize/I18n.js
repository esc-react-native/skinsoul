import React from 'react';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize'; // Use for caching/memoize for better performance
import AsyncStorage from '@react-native-community/async-storage';

import {
  I18nManager,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  es: () => require('./languages/es.json'),
  en: () => require('./languages/en.json'),
  // fr: () => require('./languages/fr.json'),
  pt: () => require('./languages/pt.json'),
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

const setLanguage = async language => {
  await AsyncStorage.setItem('language', language);

  return;
};

const setI18nConfig = async language => {
  // fallback if no available language fits

  if (language === undefined || language === null) {
    language = 'en';
  }

  const fallback = {languageTag: language, isRTL: false};

  setLanguage(language);

  const {languageTag, isRTL} =
    // RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  // clear translation cache
  translate.cache.clear();
  // update layout direction
  I18nManager.forceRTL(isRTL);
  // set i18n-js config
  i18n.translations = {[languageTag]: translationGetters[languageTag]()};
  i18n.locale = languageTag;
  console.log(languageTag);
};

export default {
  config: setI18nConfig,
  t: translate,
};
