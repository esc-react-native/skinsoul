import {Dimensions, Platform, StatusBar, NativeModules} from 'react-native';
const {StatusBarManager} = NativeModules;

const {width, height} = Dimensions.get('window');
const widthScreen = Dimensions.get('screen').width;
const heightScreen = Dimensions.get('screen').height;

const StatusBarHeight = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT; //StatusBar.currentHeight;

// const screenWidth = width < height ? width : height;
// const screenHeight = width < height ? height : width;

const screenWidth = widthScreen;
const screenHeight = heightScreen;

const screenWidthUnit = screenWidth / 100;
const screenHeightUnit = screenHeight / 100;
const marginHorizontal = screenWidthUnit;
const marginVertical = screenHeightUnit;

const sectionWidth = screenWidth / 2;
const sectionHeight = screenHeight / 2;

const marginFactorNormal = 2;
const marginFactorLarge = 4;
const marginFactorSmall = 1;

const headerHeight = screenHeight * (10 / 100);

const TABBAR_DEFAULT_HEIGHT = 49;
const TABBAR_COMPACT_HEIGHT = 29;

const TabBarHeight = TABBAR_DEFAULT_HEIGHT;
const BodyHeight = screenHeight - TabBarHeight;
const BodyHeightNoHeader = screenHeight - TabBarHeight - headerHeight;

// Used via Metrics.baseMargin
const metrics = {
  windowDesignHeight: screenHeight / 667,
  screenDesignWidth: screenWidth / 375,
  screenDesignHeight: 1,

  windowHeight: height,


  statusBarHeight: StatusBarHeight,

  //Margin
  marginHorizontal: marginHorizontal * marginFactorNormal,
  marginVertical: marginVertical * marginFactorNormal,
  marginHorizontalLarge: marginHorizontal * marginFactorLarge,
  marginHorizontalSmall: marginHorizontal * marginFactorSmall,
  marginVerticalLarge: marginVertical * marginFactorLarge,
  marginVerticalSmall: marginVertical * marginFactorSmall,
  //.Section
  sectionWidth: sectionWidth,
  sectionHeight: sectionHeight,
  sectionHeightUnit: sectionHeight / 10,
  //.
  horizontalLineHeight: 1,
  //.
  screenWidth: screenWidth,
  screenHeight: screenHeight,
  widthUnit: screenWidthUnit,
  heightUnit: screenHeightUnit,
  screenWidthNoMargin: screenWidth - marginHorizontal * marginFactorLarge * 2,
  homeBodyHeight: screenHeight - headerHeight * 2,
  bodyHeight: BodyHeight,
  bodyHeightNoHeader: BodyHeightNoHeader,
  buttonSmallHeight: headerHeight / 2,
  buttonMediumHeight: headerHeight * (2 / 3),
  buttonLargeHeight: headerHeight,
  buttonSmallWidht: screenWidth / 3,
  buttonMediumWidht: screenWidth / 2,
  buttonLargeWidht: screenWidth - marginHorizontal * 2,

  subtitleWidthMedium: screenWidth * (2 / 3),
  subtitleHeightMedium: headerHeight * (5 / 10),
  subtitleWidthSmall: screenWidth * (2 / 3),
  subtitleHeightSmall: headerHeight * (3 / 10),
  //.
  headerHeight: headerHeight,
  navBarHeight: Platform.OS === 'ios' ? screenHeightUnit * 3 : 0,
  //.
  shadowElevation: Platform.OS === 'ios' ? 2 : 10,
  buttonRadius: screenWidthUnit * 2,
  inputRadius: screenWidthUnit,
  //.
  cardMarginHorizontal: marginHorizontal * 15,
  //.
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50,
  },
  //.
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 70,
  },
};

export default metrics;
