import {Dimensions, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');

const type = {
  //base: 'Avenir-Book',
  base: 'Montserrat-Regular',
  //bold: 'Avenir-Black',
  bold: 'Montserrat-Bold',
  emphasis: 'Montserrat-Italic',
};

const size = {
  titleBig: 50,
  h1: 40,
  h2: 50,
  h3: 40,
  h4: 30,
  h5: 25,
  h6: 20,
  h7: 13,
  input: 30,
  button: 30,
  regular: 18,
  medium: 40,
  small: 20,
  tiny: 10,
  subtitleSmall: 13,
  subtitleMedium: 20,
  subtitleLarge: 25,
  cardTitle: 18,
  cardTextNormal: 16,
  textarea: 12,
  loginTitle: 70,
  loginSubtitle: 12,
  loginInput: 14,
  buttonText: 20,
  loginLink: 14,
  cardLabelTitle: 10,
  cardLabelText: 8,
  homeButton: 18,
};

const style = {
  titleBig: {
    fontFamily: type.base,
    fontSize: size.titleBig,
    fontWeight: 'bold',
  },

  LoginTitle: {
    fontFamily: type.base,
    fontSize: size.loginTitle,
    color: '#FFF',
  },

  LoginSubtitle: {
    fontFamily: type.base,
    fontSize: size.loginSubtitle,
    color: '#FFF',
  },

  LoginInput: {
    fontFamily: type.base,
    fontSize: size.loginInput,
    color: '#000',
  },

  HomeButton: {
    fontFamily: type.base,
    fontSize: size.homeButton,
    color: '#000',
  },

  cardTitle: {
    fontFamily: type.base,
    fontSize: size.cardTitle,
    color: '#000000',
    lineHeight: 22,
  },

  cardTextNormal: {
    fontFamily: type.base,
    fontSize: size.cardTextNormal,
    color: '#6D6D6D',
    lineHeight: 24,
  },


  cardLabelTitle: {
    fontFamily: type.base,
    fontSize: size.cardLabelTitle,
    color: '#000000',
    //lineHeight: 22,
  },

  cardLabelText: {
    fontFamily: type.base,
    fontSize: size.cardLabelText,
    color: '#000000',
    //lineHeight: 22,
  },


  textarea: {
    fontFamily: type.base,
    fontSize: size.textarea,
    color: 'black',
    //lineHeight: 6,
  },


  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
    fontWeight: 'bold',
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
  },
  normalBold: {
    fontFamily: type.bold,
    fontSize: size.regular,
  },

  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
};

export default {
  type,
  size,
  style,
};
