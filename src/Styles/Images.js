const images = {
  logo: require('../Assets/Logos/logo.png'),
  language_en: require('../Assets/Icons/language_en.png'),
  language_es: require('../Assets/Icons/language_es.png'),
  language_pr: require('../Assets/Icons/language_pr.png'),
  language_fr: require('../Assets/Icons/language_fr.png'),


  icon_consultant: require('../Assets/Images/sc_consultant.png'),
  icon_biopsy: require('../Assets/Images/sc_biopsy.png'),
  photoinfo: require('../Assets/Images/photoinfo.png'),

  loginBackground: require('../Assets/Images/loginBackground.jpg'),

  //background: require('../../Assets/Images/Backgrounds/background4.png'),
  //buttonBackground: require('../../Assets/Images/Backgrounds/buttonBackgroundDark.png'),
};

export default images;
