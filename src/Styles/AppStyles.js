import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';

const AppStyles = {
  screen: {
    homeContainer: {
      flex: 1,
      backgroundColor: Colors.background,
      //paddingTop: Metrics.navBarHeight,
    },

    mainContainer: {
      flex: 1,
      backgroundColor: Colors.transparent,
    },

    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },

    container: {
      flex: 1,
//      paddingTop: Metrics.marginVertical,
      //backgroundColor: Colors.background,
      //paddingBottom: 20,
    },

    borderRadius: {
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },

    card: {
      backgroundColor: '#FFF',
      width: Metrics.screenDesignWidth * 320,
      height: Metrics.windowDesignHeight * 525,
      marginTop: (Metrics.windowDesignHeight * 35) + Metrics.statusBarHeight,

      borderRadius: 5,
      borderWidth: 0,
      borderColor: '#fff',
      shadowColor: Colors.shadowCard,
      shadowOpacity: 0.9,
      shadowRadius: 4,
      shadowOffset: {
        height: 3,
        width: 0,
      },

      //backgroundColor: 'red',

    },

    cardTitle: {
      backgroundColor: Colors.app,
      height: Metrics.screenDesignHeight * 59,

      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      borderWidth: 0,
      borderColor: '#fff',
      justifyContent: 'center',
    },

    cardTitleText: {
      ...Fonts.style.normal,
      color: 'white',
      fontSize: 18,
      marginLeft: Metrics.screenDesignWidth * 25,
    },

    cardTextMarginLeft: {
      marginLeft: Metrics.screenDesignWidth * 25,
    },

    cardContainer: {
      justifyContent: 'space-between',
      height: Metrics.windowDesignHeight * 480,
      //backgroundColor: 'yellow',
    },

    cardNoTitleBar: {
      justifyContent: 'space-between',
      //backgroundColor: 'red',
      height: Metrics.screenHeight * 0.8,
    },

    cardBody: {
      marginHorizontal: Metrics.screenDesignWidth * 20,
      marginTop: Metrics.screenDesignHeight * 15,
    },

    cardBodyTitle: {
      ...Fonts.style.normal,

      marginBottom: Metrics.marginVertical,
      fontSize: 18,
    },

    cardFooter: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      //marginHorizontal: Metrics.marginHorizontalSmall,
      height: Metrics.screenDesignHeight * 40,
      marginBottom: Metrics.screenDesignHeight * 6,
      //backgroundColor: 'blue',
    },

    cardBodyTextNormal: {
      ...Fonts.style.cardTextNormal,

      marginVertical: Metrics.marginVertical,
    },

    section: {
      margin: Metrics.section,
      padding: Metrics.marginHorizontal,
    },

    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.marginVertical,
      color: Colors.text,
      marginVertical: Metrics.marginVerticalSmall,
      textAlign: 'center',
    },

    subtitle: {
      color: Colors.snow,
      padding: Metrics.marginVerticalSmall,
      marginBottom: Metrics.marginVerticalSmall,
      marginHorizontal: Metrics.marginHorizontal,
    },

    titleText: {
      ...Fonts.style.h2,
      color: Colors.text,
    },
  },

  darkLabelContainer: {
    padding: Metrics.marginVerticalSmall,
    paddingBottom: Metrics.marginVertical,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.marginHorizontal,
  },

  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow,
  },

  groupContainer: {
    margin: Metrics.marginHorizontal,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.bluePolice,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.marginVertical,
    marginTop: Metrics.marginVertical,
    marginHorizontal: Metrics.marginHorizontal,
    borderWidth: 1,
    borderColor: Colors.border,
    alignItems: 'center',
    textAlign: 'center',
  },

  icon: {
    width: Metrics.headerHeight / 2,
    height: Metrics.headerHeight / 2,
  },

  text: {
    ...Fonts.style.normal,

    color: Colors.snow,
  },
};

export default AppStyles;
