/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../Styles';

const styles = StyleSheet.create({
  ...AppStyles.screen,

  logoRow: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 20,
    height: ( Metrics.screenHeight / 3 ) - Metrics.navBarHeight,
  },

  form: {
    //paddingBottom: 20,
    height: ( Metrics.screenHeight / 3 ) + Metrics.navBarHeight,
  },

  input: {
    //marginBottom: Metrics.marginVerticalLarge,
    //height: ( Metrics.screenHeight / 3 ) + Metrics.navBarHeight,

  },
  button: {
    //marginBottom: Metrics.marginVerticalLarge,
    //height: ( Metrics.screenHeight / 3 ) + Metrics.navBarHeight,
    width: Metrics.screenWidth - Metrics.marginVerticalLarge,
    borderRadius: Metrics.buttonRadius,
  },

});

export default styles;
