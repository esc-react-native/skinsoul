import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const CARD_HEIGHT = 274;
const CARD_WIDTH = 301;
const CARD_MARGIN_TOP = 75;

const CARDHEADER_HEIGHT = 52;
const CARDHEADER_WIDTH = 230;
const CARDHEADER_PADDING_TOP = 20;

const RADIOBUTTON_HEIGHT = 125;
const RADIOBUTTON_WIDTH = 205;

const CARDBODY_HEIGHT = 170 * Metrics.screenDesignHeight;
const CARDBODY_WIDTH = CARD_WIDTH * Metrics.screenDesignWidth;

const FOOTER_HEIGHT = 47;

const styles = StyleSheet.create({
  container: {
    ...AppStyles.screen.container,
    alignItems: 'center',
    backgroundColor: Colors.background,
  },

  cardContainer: {
    backgroundColor: Colors.white,
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    marginTop: CARD_MARGIN_TOP * Metrics.screenDesignHeight,

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
    borderRadius: 5,
  },

  cardTitleContainer: {
    alignItems: 'center',
  },

  cardTitle: {
    height: CARDHEADER_HEIGHT,
    width: CARDHEADER_WIDTH,
    flexDirection: 'row',
    borderBottomColor: '#6D6D6D',
    borderBottomWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: CARDHEADER_PADDING_TOP,
    //backgroundColor: 'blue',
  },

  cardTitleIcon: {
    height: CARDHEADER_HEIGHT,

    flexDirection: 'row',
    backgroundColor: 'red',
  },

  cardTitleText: {
    fontSize: 20,
    marginLeft: 10,
  },

  cardBody: {
    //justifyContent: 'center',
    alignItems: 'center',
    height: CARDBODY_HEIGHT,
    width: CARD_WIDTH,
    // backgroundColor: 'cyan',
  },

  radioButtonContainer: {
    height: 180,
    width: CARD_WIDTH,
    //justifyContent: 'space-between',
    alignItems: 'center',
    //backgroundColor: 'red',
  },

  radioItemContainer: {
    height: 25,
    marginTop: 27,
    width: 203,
    justifyContent: 'center',
    // backgroundColor: 'red',
  },

  cardFooter: {
    alignItems: 'center',
    height: CARDBODY_HEIGHT,
    width: CARDBODY_WIDTH,
    // backgroundColor: 'red',
  },

  button: {
    backgroundColor: '#FFFFFF',
    height: 26,
  },

  buttonText: {
    color: Colors.app,
    fontSize: 16,
  },
});

export default styles;
