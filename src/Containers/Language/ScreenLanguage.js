import React, {Component} from 'react';
import {
  View,
  Text,
} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';

import Icon from 'react-native-vector-icons/FontAwesome';

import {API} from 'aws-amplify';
import {Button, RadioButton} from '../../Components/Base';

import AsyncStorage from '@react-native-community/async-storage';
import {HideNavigationBar} from 'react-native-navigation-bar-color';

// Styles
import Styles from './Styles/Language';

//Multilanguage
import i18 from '../../Localize/I18n';
import {Colors} from '../../Styles';

export default class ScreenLanguage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSaving: false,

      language: null,
    };
  }

  //.
  async componentDidMount() {
    let language = await AsyncStorage.getItem('language');

    this.setState({isLoading: false, language});
  }

  //.
  handleSubmit = async => {
    this.setState({isSaving: true});

    try {
      i18.config(this.state.language);

      this.forceUpdate();
    } catch (e) {
      alert(e.message);
    }

    this.setState({isSaving: false});
  };

  //.
  handleRadioButton = language => {
    this.setState({language});

    this.forceUpdate();
  };

  //.
  render() {
    const prevLanguage = this.state.language;
    HideNavigationBar();

    let languageOptions = [
      {
        label: i18.t('portuguese'),
        value: 'pt',
        selected: prevLanguage === 'pt' ? true : false,
      },
      {
        label: i18.t('english'),
        value: 'en',
        selected: prevLanguage === 'en' ? true : false,
      },
      {
        label: i18.t('spanish'),
        value: 'es',
        selected: prevLanguage === 'es' ? true : false,
      },
    ];

    console.log('isNewPatient');
    console.log(this.isNewPatient);

    return (
      <View style={Styles.container}>
        <View style={Styles.cardContainer}>
          <View style={Styles.cardTitleContainer}>
            <View style={Styles.cardTitle}>
              <Icon name="language" color={Colors.app} size={24} />
              <Text style={Styles.cardTitleText}>
                {i18.t('language-selection')}
              </Text>
            </View>
          </View>
          <View style={Styles.cardBody}>
            <RadioButton
              style={Styles.radioButtonContainer}
              styleContainer={Styles.radioItemContainer}
              options={languageOptions}
              onPress={this.handleRadioButton}
              value={this.state.language}
            />
            <Button
              inverted
              style={Styles.button}
              styleButton={Styles.button}
              styleText={Styles.buttonText}
              text={i18.t('apply-changes')}
              textLoading={i18.t('loading') + '...'}
              onPress={this.handleSubmit}
            />
          </View>
        </View>
      </View>
    );
  }
}
