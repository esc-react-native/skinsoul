import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const styles = StyleSheet.create({
  ...AppStyles.screen,

  header: {
    height: Metrics.bodyHeight / 3,
    width: Metrics.screenWidth,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },

  headerText: {
    fontSize: Fonts.size.h5,
    fontWeight: 'bold',
    color: Colors.text,
  },

  body: {
    height: 'auto',
    width: Metrics.screenWidth,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingTop: Metrics.marginVerticalLarge,
  },

  button: {
    marginHorizontal: Metrics.marginHorizontal,
  },

  buttonText: {
    fontSize: Fonts.size.h6,
    fontWeight: 'bold',
  }
});

export default styles;
