import React, {Component} from 'react';
import {View} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Content, Button, Text} from 'native-base';
import {Auth} from 'aws-amplify';

// Styles
import Styles from './Styles/Settings';
//Multilanguage
import i18 from '../../Localize/I18n';
import ButtonFull from '../../Components/ButtonFull';

export default class ScreenSettings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      button: false,
    };
  }

  asyncStorageRemove = key => {
    try {
      AsyncStorage.removeItem(key);
      return true;
    } catch (exception) {
      return false;
    }
  };

  handleSignout = () => {
    console.log('SignOut...');
    try {
      AsyncStorage.removeItem('sessionToken');
      Auth.signOut();

      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'AuthStack',
            params: {},
          }),
        ],
      });

      const goLogin = NavigationActions.navigate({
        routeName: 'Login',
        params: {},
      });

      this.props.navigation.dispatch(resetAction);
      this.props.navigation.dispatch(goLogin);

    } catch (error) {
      console.log(error);
      return;
    }


  };

  render() {
    return (
      <View>
        <View style={[Styles.header]}>
          <Text style={[Styles.headerText]}>{i18.t('settings')}</Text>
        </View>
        <View style={[Styles.body]}>
          <Button
            style={[Styles.button]}
            block
            onPress={() => this.handleSignout()}>
            <Text style={[Styles.buttonText]}>{i18.t('exit')}</Text>
          </Button>

        </View>
      </View>
    );
  }
}
