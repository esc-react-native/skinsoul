import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const LOGIN_TITLE_MARGIN_TOP = 200 * Metrics.screenDesignHeight;
const LOGIN_TITLE_MARGIN_LEFT = 69 * Metrics.screenDesignWidth;

const LOGIN_FORM_MARGIN_TOP = 20 * Metrics.screenDesignHeight;

const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    marginTop: 0,
    paddingTop: 0,
  },

  image: {
    resizeMode: 'cover',
    width: Metrics.screenWidth * 2,
    height: Metrics.screenHeight,
    //height: '100%',
  },

  header: {
    marginTop: LOGIN_TITLE_MARGIN_TOP,
    marginLeft: LOGIN_TITLE_MARGIN_LEFT,
  },

  loginForm: {
    marginTop: LOGIN_FORM_MARGIN_TOP,
  },

  errorInput: {
    borderColor: '#FF0000',
    borderWidth: 1,
    borderRadius: 0,
  },

  errorText: {
    ...Fonts.style.normal,
    color: '#FF0000',
    textAlign: 'center',
    textAlignVertical: 'center',
    width: Metrics.screenWidth,
    fontSize: 12,
    fontWeight: 'bold',
    height: 24 * Metrics.screenDesignHeight,
  },

  input: {
    borderColor: '#6D6D6D',
    borderWidth: 0,
    borderRadius: 0,

    elevation: 0,
    shadowRadius: 0,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 0},
  },

  focusOn: {
    marginTop: '25%',
  },

  button: {
    marginTop: Metrics.screenDesignHeight * 27,
  },

  noAccount: {
    justifyContent: 'flex-start',
  },

  subtitle: {
    ...Fonts.style.LoginSubtitle,
    color: Colors.white,
  },
});

export default styles;
