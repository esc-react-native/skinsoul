import React, {Component} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
} from 'react-native';
import {Images, Metrics} from '../../Styles';
import Styles from './Styles/Login';

import {Auth} from 'aws-amplify';

import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['Warning: -[RCTRootView cancelTouches]']);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

// Styles

//Multilanguage
import i18 from '../../Localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../../Components/Logo';
import LoginTitle from '../../Components/LoginTitle';
import Subtitle from '../../Components/Subtitle';
import InputText from '../../Components/LoginInput';
import LanguageBar from '../../Components/LanguageBar';
import Button from '../../Components/Button';
import Link from '../../Components/Link';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      button: false,
      onFocus: false,
      buttonSubmitDisabled: true,
      isSigning: false,

      ErrorMessage: null,
    };

    this.handlePress = this.handlePress.bind();
    this.handleFocus = this.handleFocus.bind();
    this.handleUnfocus = this.handleUnfocus.bind();
    this.handleSignIn = this.handleSignIn.bind();
    this.handleErrorMessage = this.handleErrorMessage.bind();
  }

  componentDidMount = () => {
    SplashScreen.hide();

    HideNavigationBar();
  };

  componentWillUnmount = () => {

  }

  handleSignIn = async () => {
    const {username, password} = this.state;
    console.log('SIGN');

    if (!this.formValidate()) return;

    this.setState({isSigning: true, isFocusOn: false});

    try {
      const user = await Auth.signIn(username.toLowerCase(), password);
      console.log('Login Session Result');
      console.log(user);

      if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
        const {requiredAttributes} = user.challengeParam;
        //const {username, email, phone_number} = getInfoFromUserInput();
        const loggedUser = await Auth.completeNewPassword(
          user, // the Cognito User Object
          password, // the new password
        );

        console.log('Login Session Result');
        console.log(loggedUser);
      }

      await AsyncStorage.setItem(
        'sessionToken',
        user.signInUserSession.accessToken.jwtToken,
      );

      this.props.navigation.navigate('Home');
    } catch (err) {
      console.log('ERROR');
      this.setState({isSigning: false});
      if (err.code === 'UserNotConfirmedException') {
        this.handleErrorMessage(i18.t('user-not-confirmed'));
        console.log('UserNotConfirmedException');
        console.log(err);
        // The error happens if the user didn't finish the confirmation step when signing up
        // In this case you need to resend the code and confirm the user
        // About how to resend the code and confirm the user, please check the signUp part
      } else if (err.code === 'PasswordResetRequiredException') {
        this.handleErrorMessage(i18.t('password-reset-required'));
        console.log('PasswordResetRequiredException');
        console.log(err);

        // The error happens when the password is reset in the Cognito console
        // In this case you need to call forgotPassword to reset the password
        // Please check the Forgot Password part.
      } else if (err.code === 'NotAuthorizedException') {
        this.handleErrorMessage(i18.t('user-not-confirmed'));
        console.log('NotAuthorizedException');
        console.log(err);

        // The error happens when the incorrect password is provided
      } else if (err.code === 'UserNotFoundException') {
        this.handleErrorMessage(i18.t('user-not-found'));
        console.log('UserNotFoundException');
        console.log(err);

        // The error happens when the supplied username/email does not exist in the Cognito user pool
      } else {
        console.log(err);
      }
    }
  };

  handleErrorMessage = errorMessage => {
    this.setState({ErrorMessage: errorMessage});

    setTimeout(() => {
      this.setState({ErrorMessage: null});
    }, 3000);
  };

  formValidate = () => {
    return this.state.username.length && this.state.password.length;
  };

  //
  handleChangeText = (type, text) => {
    this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: !this.formValidate()});
    });
  };

  handleLanguage = () => {
    this.forceUpdate();
  };

  handlePress = () => {
    AsyncStorage.setItem('sessionToken', 'A');

    this.props.navigation.navigate('Home');
  };

  handleFocus = () => {
    this.setState({onFocus: true});
  };

  handleUnfocus = () => {
    this.setState({onFocus: false});
    Keyboard.dismiss;
  };

  render() {
    const isFocusOn = this.state.onFocus;

    return (
      <View style={Styles.container}>
        <ImageBackground
          source={Images.loginBackground}
          style={Styles.image}
          blurRadius={Platform.OS === 'ios' ? 10 : 2}>
          <StatusBar translucent backgroundColor="transparent" />
          <KeyboardAvoidingView
            style={Styles.container}
            behavior="padding"
            enabled>
            {!isFocusOn && (
              <View style={Styles.header}>
                <LoginTitle text="SKIN" />
                <LoginTitle text="SOUL" />
                <Subtitle
                  text={i18.t('data-gathering-version').toLowerCase()}
                  style={Styles.subtitle}
                />
              </View>
            )}
            <View
              style={[
                Styles.loginForm,
                isFocusOn ? Styles.focusOn : Styles.focusOff,
              ]}>
              <InputText
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                placeholder={i18.t('username')}
                value={this.state.username}
                styleView={[
                  Styles.input,
                  this.state.ErrorMessage !== null
                    ? Styles.errorInput
                    : Styles.none,
                ]}
                onChangeText={text => this.handleChangeText('username', text)}
                onFocus={this.handleFocus}
                onUnfocus={this.handleUnfocus}
                onSubmitEditing={() => this.handleUnfocus()}
              />
              <InputText
                autoCapitalize="none"
                secureTextEntry
                placeholder={i18.t('password')}
                onChangeText={text => this.handleChangeText('password', text)}
                value={this.state.password}
                onFocus={this.handleFocus}
                styleView={[
                  Styles.input,
                  this.state.ErrorMessage !== null
                    ? Styles.errorInput
                    : Styles.none,
                ]}
                onUnfocus={this.handleUnfocus}
                onSubmitEditing={() => this.handleSignIn()}
              />
              <Text style={Styles.errorText}>{this.state.ErrorMessage}</Text>
              <Button
                style={Styles.button}
                text={'Login'}
                textLoading={i18.t('loading') + '...'}
                onPress={this.handleSignIn}
                disabled={this.state.buttonSubmitDisabled}
                isLoading={this.state.isSigning}
              />
            </View>
          </KeyboardAvoidingView>
        </ImageBackground>
      </View>
    );
  }
}
