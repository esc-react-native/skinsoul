/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView, StatusBar} from 'react-native';

import Moment from 'moment';

import {API} from 'aws-amplify';
import TitleTab from '../../Components/TitleTab';
import GeneralLabelValue from '../../Components/GeneralLabelValue';
import ScreenPhotoInfo from './ScreenPhotoInfo';

// Styles
import Styles from './Styles/Consultation';

//Multilanguage
import i18 from '../../Localize/I18n';

import Button from '../../Components/Button';
import CardTitle from '../../Components/CardTitle';
import LesionValue from '../../Components/LesionValue';
import {DropdownModal} from '../../Components/Base';
import ButtonFooter from '../../Components/ButtonFooter';
import CardLesionItem from '../../Components/CardLesionItem';
import CardLesionInfo from '../../Components/CardLesionInfo';
import BottomHomeButton from '../../Components/BottomHomeButton';
import ModalExitPatient from '../../Components/ModalExitPatient';
import ModalButtonHome from '../../Components/ModalButtonHome';
import ModalNotification from '../../Components/ModalNotification';
import {StackActions, NavigationActions} from 'react-navigation';

import Icon from 'react-native-vector-icons/FontAwesome';
import {Colors, Images} from '../../Styles';
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import {s3Upload, s3Get} from '../../libs/awsLib';


export default class ScreenConsultation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isConfirmation: false,

      SearchType: this.props.navigation.getParam('SearchType', 'NO-ID'),
      patient: this.props.navigation.getParam('Patient', 'NO-ID'),
      patientNumber: this.props.navigation.getParam('PatientNumber', 'NO-ID'),
      consultations: [],

      isWizzard: false,
      actualTab: 0,

      isConsultationDetail: false,
      consultation: null,

      isLesionDetail: false,
      lesionUpdate: {},
      lesionUpdateNum: '0',
      biopsyDiagnosis: '',
      lesionUpdateButton: false,

      diagnosisOptions: [
        {id: 'merkel-cell-carcinoma', label: i18.t('merkel-cell-carcinoma'), value: 'merkel-cell-carcinoma'},
        {id: 'malignant-melanoma-amelanotic', label: i18.t('malignant-melanoma-amelanotic'), value: 'malignant-melanoma-amelanotic'},
        {id: 'magliant-melanoma-pigmented', label: i18.t('magliant-melanoma-pigmented'), value: 'magliant-melanoma-pigmented'},
        {id: 'malignant-cutaneous-lymphoma-tcell', label: i18.t('malignant-cutaneous-lymphoma-tcell'), value: 'malignant-cutaneous-lymphoma-tcell'},
        {id: 'malignant-cutaneous-lymphome-bcell', label: i18.t('malignant-cutaneous-lymphome-bcell'), value: 'malignant-cutaneous-lymphome-bcell'},
        {id: 'malignant-epidermal-basal-cell-carcinoma', label: i18.t('malignant-epidermal-basal-cell-carcinoma'), value: 'malignant-epidermal-basal-cell-carcinoma'},
        {id: 'malignant-epidermal-basal-squamous', label: i18.t('malignant-epidermal-basal-squamous'), value: 'malignant-epidermal-basal-squamous'},
        {id: 'malignant-dermal-angiosarcoma', label: i18.t('malignant-dermal-angiosarcoma'), value: 'malignant-dermal-angiosarcoma'},
        {id: 'malignant-dermal-merkel', label: i18.t('malignant-dermal-merkel'), value: 'malignant-dermal-merkel'},
        {id: 'bening-dermal-cyst', label: i18.t('bening-dermal-cyst'), value: 'bening-dermal-cyst'},
        {id: 'bening-dermal-fibroma', label: i18.t('bening-dermal-fibroma'), value: 'bening-dermal-fibroma'},
        {id: 'bening-dermal-lipoma', label: i18.t('bening-dermal-lipoma'), value: 'bening-dermal-lipoma'},
        {id: 'bening-melanoctyc-blue', label: i18.t('bening-melanoctyc-blue'), value: 'bening-melanoctyc-blue'},
        {id: 'bening-melanoctyc-solar', label: i18.t('bening-melanoctyc-solar'), value: 'bening-melanoctyc-solar'},
        {id: 'bening-melanoctyc-café', label: i18.t('bening-melanoctyc-café'), value: 'bening-melanoctyc-café'},
        {id: 'bening-epidermal-milia', label: i18.t('bening-epidermal-milia'), value: 'bening-epidermal-milia'},
        {id: 'bening-epidermal-seborrheic', label: i18.t('bening-epidermal-seborrheic'), value: 'bening-epidermal-seborrheic'},
        {id: 'neoplastic-genodermatosis-congenital', label: i18.t('neoplastic-genodermatosis-congenital'), value: 'neoplastic-genodermatosis-congenital'},
        {id: 'neoplastic-genodermatosis-tuberous', label: i18.t('neoplastic-genodermatosis-tuberous'), value: 'neoplastic-genodermatosis-tuberous'},
        {id: 'neoplastic-inflamatory-psoriasis', label: i18.t('neoplastic-inflamatory-psoriasis'), value: 'neoplastic-inflamatory-psoriasis'},
        {id: 'neoplastic-inflamatory-stevens', label: i18.t('neoplastic-inflamatory-stevens'), value: 'neoplastic-inflamatory-stevens'},
        {id: 'neoplastic-inflamatory-rosacea', label: i18.t('neoplastic-inflamatory-rosacea'), value: 'neoplastic-inflamatory-rosacea'},
        {id: 'neoplastic-inflamatory-acne', label: i18.t('neoplastic-inflamatory-acne'), value: 'neoplastic-inflamatory-acne'},
        {id: 'neoplastic-inflamatory-abrasion', label: i18.t('neoplastic-inflamatory-abrasion'), value: 'neoplastic-inflamatory-abrasion'},
        {id: 'neoplastic-blistering-vulgaris', label: i18.t('neoplastic-blistering-vulgaris'), value: 'neoplastic-blistering-vulgaris'},
        {id: 'neoplastic-blistering-bullous', label: i18.t('neoplastic-blistering-bullous'), value: 'neoplastic-blistering-bullous'},
        {id: 'neoplastic-blistering-foliaceus', label: i18.t('neoplastic-blistering-foliaceus'), value: 'neoplastic-blistering-foliaceus'},
        {id: 'subungueal-melanoma', label: i18.t('subungueal-melanoma'), value: 'subungueal-melanoma'},
        {id: 'melanonychia', label: i18.t('melanonychia'), value: 'melanonychia'},
      ],

      modalVisible: false,
      modalBiopsyUpatedVisible: false,
    };

    this.handleChangeTab = this.handleChangeTab.bind();
    this.handleBiopsyResult = this.handleBiopsyResult.bind();
    this.lesionDetail = this.lesionDetail.bind();

    this.handleConsultationDetailBack = this.handleConsultationDetailBack.bind();
    this.handleLesionDetail = this.handleLesionDetail.bind();
  }

  recordUpdate = (id, body) => {
    // console.log(`PUT /patients/${id}`);
    // console.log(body);

    return API.put('patients', `/patients/${id}`, {
      body: body,
    });
  };

  //.
  handleSubmit = async patient => {
    this.setState({isSaving: true});

    // console.log('ScreenConsultations --> handleSubmit --> Saving...');
    // console.log(JSON.stringify(patient));

    try {
      await this.recordUpdate(patient.patientNumber, patient);
      // console.log('Updated');

      this.setState({isSaving: false, patient, isWizzard: false});

      //Reload and Refresh...
    } catch (e) {
      alert(e.message);
      // console.log('Error Updating');
      // console.log(e);
      // console.log(JSON.stringify(e));

      this.setState({isSaving: false, isWizzard: false});
    }
  };

  //.
  handleNew = () => {
    console.log('handleNew');
    this.setState({isWizzard: true});
  };

  //.
  handleChangeTab = (tab) => {
    let actualTab = tab;

    this.setState({actualTab});

  };

  //.
  handleFinish = () => {
    //Call to Save Patient Record
    const patient = {
      ...this.state.patient,

      lesions: this.state.lesions,
    };

    console.log('record to save');
    console.log(JSON.stringify(patient));

    this.handleSubmit(patient);
  };

  //.
  renderHeaderTable = () => {
    return (
      <View style={Styles.consultationHeader}>
        <View style={Styles.consultationDateViewHeader}>
          <Text style={Styles.consultationDateTextHeader}>{i18.t('date')}</Text>
        </View>
        <View style={Styles.consultationNoteViewHeader}>
          <Text style={Styles.consultationNoteHeader}>{'Consultation Data'}</Text>
        </View>
      </View>
    );
  };

  //.
  handleValidate = () => {
    if(this.state.biopsyDiagnosis !== null && this.state.biopsyDiagnosis.length > 0 )
      this.setState({lesionUpdateButton: true});
  }

  //.
  handleChangeValue = async (type, text) => {
    await this.setState({[type]: text}, () => {
      this.handleValidate();
    });
  };

  //.
  handleBiopsyResult =  (consultationObj) => {
    let consultation = consultationObj;
    
    console.log('consultation X');
    console.log(JSON.stringify(consultation));

    // for( let i = 0 ; i < consultation.lesions.length ; i++ ){
    //   consultation.lesions[i].photoURL = s3Get(consultation.lesions[i].photo);
    // }

    console.log('consultation XX');
    console.log(consultation);

    this.setState({consultation: consultationObj}, this.setState({isConsultationDetail : true, isLesionDetail: false, lesionUpdateButton: false}));
  }

  //.
  lesionDetail = (lesion, num) => {
    this.setState({isLesionDetail: true, 
      lesionUpdate: lesion, 
      lesionUpdateNum: num.toString(),
      biopsyDiagnosis: lesion.biopsyDiagnosis,
      lesionUpdateButton: false});

  }

  //.
  handleBackLesion = () => {
    this.setState({isLesionDetail: false,
      lesionUpdate: null,
      lesionUpdateNum: '0',
      biopsyDiagnosis: '',
      lesionUpdateButton: false,
    });

  }


  //.
  handleUpdateLesion = async () => {
    let lesion = this.state.lesionUpdate;
    let lesions = this.state.consultation.lesions;
    let consultation = this.state.consultation;
    let consultations = this.state.patient.consultations;
    let patient = this.state.patient;

    lesion.biopsyDiagnosis = this.state.biopsyDiagnosis;

    console.log('New Lesion');
    console.log(lesion);

    console.log('old lesions');
    console.log(lesions);

    console.log('indexLesion');
    const indexLesion = lesions.findIndex(x => x.photo === lesion.photo);
    console.log(indexLesion);

    console.log('indexConsultation');
    const indexConsultation = consultations.findIndex(x => x.date === consultation.date);
    console.log(indexConsultation);

    console.log('patient transformation');
    console.log(JSON.stringify(patient));
    consultation.lesions[indexLesion] = lesion;
    consultations[indexConsultation] = consultation;
    patient.consultations = consultations;

    this.setState({patient, consultations, consultation});

    try {
      await this.recordUpdate(patient.patientNumber, patient);
      console.log('Updated');

      //Reload and Refresh...
    } catch (e) {
      console.log('Error Updating');
      console.log(e);
      console.log(JSON.stringify(e));

      this.setState({isSaving: false});
    }

    console.log(JSON.stringify(patient));
    this.setState({isLesionDetail: false,
      lesionUpdateNum: '0'});
  }

  //.
  handleLesionUpdate = async (lesionUpdate, biopsyDiagnosis) => {
    let lesion = lesionUpdate;
    let lesions = this.state.consultation.lesions;
    let consultation = this.state.consultation;
    let consultations = this.state.patient.consultations;
    let patient = this.state.patient;

    lesion.biopsyDiagnosis = biopsyDiagnosis;

    console.log('New Lesion');
    console.log(lesion);

    console.log('old lesions');
    console.log(lesions);

    console.log('indexLesion');
    const indexLesion = lesions.findIndex(x => x.photo === lesion.photo);
    console.log(indexLesion);

    console.log('indexConsultation');
    const indexConsultation = consultations.findIndex(x => x.date === consultation.date);
    console.log(indexConsultation);

    console.log('patient transformation');
    console.log(JSON.stringify(patient));
    consultation.lesions[indexLesion] = lesion;
    consultations[indexConsultation] = consultation;
    patient.consultations = consultations;

    this.setState({patient, consultations, consultation});

    try {
      await this.recordUpdate(patient.patientNumber, patient);
      console.log('Updated');
      this.handleModalBiopsyUpdated();

      //Reload and Refresh...
    } catch (e) {
      console.log('Error Updating');
      console.log(e);
      console.log(JSON.stringify(e));

      this.setState({isSaving: false});
    }

    console.log(JSON.stringify(patient));
    this.setState({isLesionDetail: false,
      lesionUpdateNum: '0'});
  }


  //.
  handleConsultationDetailBack = () => {
    this.setState({
      isConsultationDetail : false, 
      isLesionDetail: false,
      lesionUpdate: null,
      lesionUpdateNum: null,
      biopsyDiagnosis: null,
      lesionUpdateButton: false});

      console.log('handleConsultationDetailBack');
  }

  //.
  handleLesionDetail = (lesion, lesionNum) => {
    this.setState({isLesionDetail: true, 
      lesionUpdate: lesion, 
      lesionUpdateNum: lesionNum.toString(),
      biopsyDiagnosis: lesion.biopsyDiagnosis,
      lesionUpdateButton: false});
  }

  //.
  handleLesionDetailBack = () => {
    this.setState({isLesionDetail: false, 
      lesionUpdate: null, 
      lesionUpdateNum: null,
      biopsyDiagnosis: null,
      lesionUpdateButton: false});

      console.log('BACK HANDLE LESION');
  }

  //.
  handleLesionDetailUpdate = () => {

  }

  //.
  handleButtonHome = () => {



    this.setState({modalVisible: true});
  };

  //.
  handleModalExit = () => {
    this.setState({modalVisible: false});
  };

  //.
  handleModalBiopsyUpdated = () => {
    this.setState({modalBiopsyUpatedVisible: true});

    setTimeout(() => {
      this.setState({modalBiopsyUpatedVisible: false});
    }, 3000);
  };


  //.
  handleModalOK = () => {
    console.log('screenconsultation -> handleExitHome');

    this.setState({modalVisible: false});

    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'MainTabs',
          params: {},
        }),
      ],
    });

    const goLogin = NavigationActions.navigate({
      routeName: 'Home',
      params: {},
    });

    this.props.navigation.dispatch(resetAction);
    this.props.navigation.dispatch(goLogin);
  };


  //.
  handleNewConsultationHome = () => {
    console.log('screenconsultation -> handleNewConsultationHome');

    this.setState({modalVisible: false});

    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'MainTabs',
          params: {},
        }),
      ],
    });

    const goLogin = NavigationActions.navigate({
      routeName: 'Home',
      params: {},
    });

    this.props.navigation.dispatch(resetAction);
    this.props.navigation.dispatch(goLogin);
  };

  //.
  renderLesions = () => {
    let lesions = this.state.consultation.lesions;

    console.log('renderLesions --> lesions');
    console.log(lesions);

    //lesions = this.getS3Images(lesions);

    console.log('renderLesions --> lesions XX');
    console.log(lesions);

    return lesions.map( (lesion, i) => (
      <CardLesionItem index={i} item={lesion} photo={lesion.photo} s3Load 
        onBack={this.handlerConsultationDetailBack}
        onSelect={this.handleLesionDetail}
        isBiopsy={this.state.SearchType === 'biopsy' ? true : false}
      />
    ));
  };

  //.
  renderConsultationNumLesions = (lesions) => {
    let counterSendForBiopsy = 0;
    let counterBiopsyResults = 0;
    for(let i = 0 ; i < lesions.length ; i++ ){

      console.log(`${lesions[i].sendForBiopsy}`);

      if(lesions[i].sendForBiopsy && lesions[i].biopsyDiagnosis === undefined)
        counterSendForBiopsy++;

      if(lesions[i].sendForBiopsy && lesions[i].biopsyDiagnosis !== undefined)
        counterBiopsyResults++;
    }

    return (
      <View style={Styles.consultationNumView}>
        <Text style={Styles.consultationNumText}>{lesions.length.toString() + '/' + counterSendForBiopsy + '/' + counterBiopsyResults}</Text>
      </View>
    );
  };

  //.
  renderConsultationLesionsNote = (lesions) => {
    const numLesions = lesions.length;
    let numBiopsy = 0;
    let numResults = 0;
    console.log(`Lesion ${lesions.length}`);
    console.log(lesions);

    for (let i = 0; i < numLesions; i++){
      console.log(`ENTER Lesion ${i}`);
      console.log(lesions);

      if (lesions[i].sendForBiopsy){
        if(lesions[i].biopsyDiagnosis){
          numResults++;

          console.log(`numResults ${numResults}`);
        } else {
          numBiopsy = numBiopsy + 1;

          console.log(`numBiopsy ${numBiopsy}`);
        }
      }
    }

    return (
      <View style={Styles.consultationData}>
        <View style={Styles.consultationDataItem}>
          <Text style={Styles.consultationDataItemText}>{numLesions}</Text>
          <Icon name={'camera'} color={Colors.app} size={20} />
        </View>
        <View style={Styles.consultationDataItem}>
          <Text style={Styles.consultationDataItemText}>{numBiopsy}</Text>
          <Image source={Images.icon_biopsy} style={Styles.consultationDataItemIcon}/>
        </View>
        <View style={Styles.consultationDataItem}>
          <Text style={Styles.consultationDataItemText}>{numResults}</Text>
          <Icon name={'list-alt'} color={Colors.app} size={20} />
        </View>
      </View>
    );
  }

  //.
  renderConsultations = () => {
    console.log('this.state.patient.consultations');
    console.log(JSON.stringify(this.state.patient.consultations));

    const bps = this.state.patient.consultations;

    const consultations = bps.sort(function(a, b) {
      a = new Date(a.date);
      b = new Date(b.date);
      return a>b ? -1 : a<b ? 1 : 0;
    });

    Moment.locale('en');

    console.log('CONSULTATIONS this.state.patient.consultations');
    console.log(JSON.stringify(this.state.patient.consultations));

      return consultations.map((item, i) => (
        <TouchableOpacity
          onPress={() => this.handleBiopsyResult(item)}
          key={i} style={Styles.consultationView}>
          <View key={i} style={Styles.consultationDateView}>
            <Text style={Styles.consultationDateText}>{Moment(item.date).format('YYYY-MM-DD')}</Text>
          </View>
          <View style={Styles.consultationNoteView}>
            {this.renderConsultationLesionsNote(item.lesions)}
          </View>
          </TouchableOpacity>
      ));
  };

  //.
  render() {
    let SearchType = this.props.navigation.getParam('SearchType', 'NO-ID');

    // console.log('this.state.patient');
    // console.log(this.state.patient);
    HideNavigationBar();
    

    return (
      <View style={Styles.container}>
        <StatusBar translucent backgroundColor="transparent" barStyle="light-content"/>
        {this.state.isWizzard && !this.state.isConsultationDetail && (
          <ScreenPhotoInfo
            isExistingPatient={true}
            patientNumber={this.state.patientNumber}
            patient={this.state.patient}
            step={1}
            onSubmit={this.handleSubmit}
            onExitHome={this.handleModalOK}
          />
        )}
        {!this.state.isWizzard && !this.state.isConsultationDetail && (
          <View style={Styles.card}>
            <TitleTab
              onPress={this.handleChangeTab}
              actualTab={this.state.actualTab}
              tabText1={i18.t('general-info')}
              tabText2={i18.t('consultations')}
            />
            <View style={Styles.cardContainer}>
              <View style={Styles.cardBody}>
                <Text style={Styles.cardBodyTitle}>
                  {this.state.patientNumber}
                </Text>
                {this.state.actualTab === 0 && (
                  <View>
                    <View style={Styles.consultationGeneralInfo}>
                      <GeneralLabelValue style={Styles.labelValue} label={i18.t('gender')} text={i18.t(this.state.patient.gender)}/>
                      <GeneralLabelValue label={i18.t('age')} text={this.state.patient.age}/>
                      <GeneralLabelValue label={i18.t('phototype')} text={this.state.patient.phototype}/>
                      <GeneralLabelValue label={i18.t('immuno') + '-'} secondLine={i18.t('compromised')} text={this.state.patient.inmuno? i18.t('yes'): i18.t('no')}/>
                    </View>
                  </View>
                )}
                {this.state.actualTab === 1 && (
                  <View style={Styles.consultationDataBody}>
                    <View>
                      {this.renderHeaderTable()}
                      <ScrollView style={Styles.scrollView}>
                      {this.renderConsultations()}
                      </ScrollView>
                    </View>
                  </View>
                )}
              </View>
              { this.state.actualTab === 0 && SearchType === 'consultation' && (
                      <View style={Styles.buttonContainer}>
                        <Button
                          inverted
                          style={Styles.button}
                          text={i18.t('new-consultation')}
                          textLoading={i18.t('loading') + '...'}
                          onPress={this.handleNew}
                          disabled={this.state.buttonSubmitDisabled}
                          isLoading={this.state.isProcessing}
                        />
                      </View>
                  )}
              { this.state.actualTab === 1 && (
                  <View style={Styles.consultationDataConventions}>
                    <View style={Styles.consultationDataConventionItem}>
                      <Icon name={'camera'} color={Colors.app} size={20} />
                      <Text style={Styles.consultationDataConventionText}>{'Lesions Acquired'}</Text>
                    </View>
                    <View style={Styles.consultationDataConventionItemExtended}>
                      <Image source={Images.icon_biopsy} style={Styles.consultationDataItemIcon}/>
                      <Text style={Styles.consultationDataConventionText}>{'Biopsies Waiting results'}</Text>
                    </View>
                    <View style={Styles.consultationDataConventionItem}>
                      <Icon name={'list-alt'} color={Colors.app} size={20} />
                      <Text style={Styles.consultationDataConventionText}>{'Results obtained'}</Text>
                    </View>
                  </View>
                )}
            </View>
          </View>
        )}

        {!this.state.isWizzard && this.state.isConsultationDetail &&
        (
          <View>
            { !this.state.isLesionDetail && (
              <View style={Styles.card}>
                <CardTitle
                  iconName={'list-alt'
                  }
                  title={this.state.patientNumber}
                  subtitle={`${i18.t('consultation')} ${Moment(this.state.consultation.date).format('YYYY-MM-DD')}`}
                />
                <View style={Styles.cardContainer}>
                  <View style={Styles.cardBody}>
                    <ScrollView style={Styles.scrollView}>
                        {this.renderLesions()}
                      </ScrollView>
                  </View>
                  <View style={Styles.lesionFooter}>
                    <ButtonFooter
                      back
                      text={i18.t('back').toUpperCase()}
                      onPress={this.handleConsultationDetailBack}
                    />
                  </View>
                </View>
              </View>
            )}
            { this.state.isLesionDetail && (
              <CardLesionInfo
                patientNumber={this.state.patient.patientNumber}
                consultation={this.state.consultation}
                lesion={this.state.lesionUpdate}
                lesionNumber={this.state.lesionUpdateNum}
                onBack={this.handleLesionDetailBack}
                onLesionUpdate={this.handleLesionUpdate}
                biopsyResultsEditable={this.state.SearchType === 'biopsy' ? true : false}
                isBiopsy={this.state.SearchType === 'biopsy' ? true : false}
                age={this.state.consultation.age}
                immuno={this.state.consultation.immuno}
                />
            )}
          </View>
        )}
        {(!this.state.isWizzard || this.state.isConsultationDetail) && (
            <View style={Styles.homeButton}>
                {/* <BottomHomeButton onPress={this.handleButtonHome} /> */}
                <BottomHomeButton onPress={this.handleModalOK} />
            </View>
        )}
        <ModalButtonHome
          visible={this.state.modalVisible}
          onPressOK={this.handleModalOK}
          onPressCancel={this.handleModalExit}
          icon={"trash"}
          question={i18.t('return-to-home') + '?'}
          disclaimer={i18.t('any-changes-discarded')}
        />
        <ModalNotification
          visible={this.state.modalBiopsyUpatedVisible}
          icon={"microscope"}
          message={i18.t('biopsy-result-updated')}
        />
      </View>
    );
  }
}
