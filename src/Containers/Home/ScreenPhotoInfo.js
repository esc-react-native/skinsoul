import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Alert,
  ScrollView,
} from 'react-native';

import {Colors} from '../../Styles';

import {StackActions, NavigationActions} from 'react-navigation';
import RadioGroup from 'react-native-radio-buttons-group';
import {Container, Header, Content, Item, Picker} from 'native-base';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ButtonFooter from '../../Components/ButtonFooter';
import ProgressCircles from '../../Components/ProgressCircles';
import {API} from 'aws-amplify';
import {s3Upload, s3Get} from '../../libs/awsLib';

// Styles
import Styles from './Styles/PhotoInfo';

//Multilanguage
import i18 from '../../Localize/I18n';

import ImageSample from '../../Assets/Images/consultant.png';
import WizardLesion from './WizardLesion';
import Button from '../../Components/Button';
import CardTitle from '../../Components/CardTitle';
import CardLesionItem from '../../Components/CardLesionItem';

import {HideNavigationBar} from 'react-native-navigation-bar-color';

export default class ScreenPhotoInfo extends Component {
  constructor(props) {
    super(props);

    console.log('ScreenPhotoInfo props');
    // console.log(JSON.stringify(this.props));

    this.state = {
      isLoading: false,
      isConfirmation: false,

      patient: this.props.isExistingPatient ? this.props.patient : null,
      patientNumber: this.props.isExistingPatient
        ? this.props.patient.patientNumber
        : this.props.navigation.getParam('PatientNumber', 'NO-ID'),
      isNewPatient: this.props.isExistingPatient ? false : true,

      numLesions: 0,
      isWizzard: this.props.isExistingPatient ? true : false,
      lesions: [],

      uploadingImagesTotal: 0,
      uploadingImagesActual: 0,

      patientEdit: null,
      isSaving: false,
      isSavedOK: false,
    };

    this.pushLesion = this.pushLesion.bind();
    this.handleNew = this.handleNew.bind();
    this.handleFinish = this.handleFinish.bind();
    this.handleList = this.handleList.bind();
    this.handleSubmit = this.handleSubmit.bind();
    this.handleUploadFiles = this.handleUploadFiles.bind();
  }

  recordCreate = body => {
    return API.post('patients', '/patients', {
      body: body,
    });
  };

  handleSubmit = async patient => {
    this.setState({isSaving: true});

    console.log('Saving...');
    console.log(patient);

    try {
      if (this.state.isNewPatient) {
        await this.recordCreate(patient);

        // console.log('Saved');
        // console.log(JSON.stringify(patient));
        // console.log('GO TO CONSULTATION');

        this.setState({isSavedOK: true});

        setTimeout(() => {
          this.props.navigation.navigate('Consultation', {
            PatientNumber: patient.patientNumber,
            Patient: patient,
            SearchType: this.props.navigation.getParam('SearchType', 'NO-ID'),
          });

          this.setState({isSaving: false, isSavedOK: false});
        }, 3000);

        return;
      } else {
        // console.log('SEND TO SCREEN CONSULTATION');
        this.setState({isSaving: false});
        this.props.onSubmit(patient);
        return;
      }
    } catch (e) {
      // console.log('Error Creating');
      // console.log(e);
      // console.log(JSON.stringify(e));

      this.setState({isLoading: false, isSaving: false});
    }
  };

  pushLesion = objLesion => {
    const {
      photo,
      bleedingLession,
      sendForBiopsy,
      location,
      diagnosis,
      notes,
    } = objLesion;

    let lesions = this.state.lesions;

    // console.log('before lesions');
    // console.log(lesions);

    lesions.push({
      photo,
      bleedingLession,
      sendForBiopsy,
      location,
      diagnosis,
      notes,
    });

    this.setState({lesions});

    // console.log('lesions');
    // console.log(this.state.lesions);
  };

  handleNew = () => {
    // console.log('handleNew');
    this.setState({isWizzard: true});
  };

  handleUploadFiles = async (argLesions, argAge, argImmuno) => {
    try {
      let retLesions = argLesions;
      let response = null;
      let blob = null;
      let fileName = null;

      this.setState({
        uploadingImagesTotal: retLesions.length,
        uploadingImagesActual: 0,
      });

      for (let i = 0; i < retLesions.length; i++) {
        // eslint-disable-next-line prettier/prettier
        //console.log(`upload photo: ${retLesions[i].photo} | iteractions: ${i}/${retLesions.length})`);
        this.setState({
          uploadingImagesActual: i,
        });

        response = await fetch(retLesions[i].photo);
        blob = await response.blob();
        fileName = `${this.state.patientNumber} ${Date.now()} Lesion #${i}`;

        retLesions[i].photo = await s3Upload(
          blob,
          {contentType: 'image/jpeg'},
          fileName,
        );

        //retLesions[i].photoURL = await s3Get(retLesions[i].photo);
        retLesions[i].age = argAge;
        retLesions[i].immuno = argImmuno;

        // eslint-disable-next-line prettier/prettier
        // console.log(`Finish photo: ${retLesions[i].photo} | iteractions: ${i}/${retLesions.length})`);
        // console.log(JSON.stringify(retLesions[i].photo));
        // console.log(JSON.stringify(retLesions[i].photoURL));
      }

      this.setState({lesions: retLesions});
    } catch (e) {
      alert(e);
    }
  };

  handleInputUnfocus = () => {};

  //.
  handleFinish = async () => {
    //Call to Save Patient Record
    console.log('handleFinish screenPhoto this.state.patient');
    console.log(this.state.patient);

    let patient;
    let newConsultations = [];

    this.setState({isSaving: true});

    await this.handleUploadFiles(
      this.state.lesions,
      this.state.patient.age,
      this.state.patient.inmuno,
    );

    // console.log('\nhandleFinish screenPhoto isNewPatient?');
    // console.log(this.state.isNewPatient);

    if (
      this.state.patient.consultations !== undefined &&
      !this.state.isNewPatient
    ) {
      newConsultations = this.state.patient.consultations;

      // console.log('\nhandleFinish screenPhoto newConsultations...');
      // console.log(JSON.stringify(this.state.patientEdit));
      // console.log(JSON.stringify(this.state.patient));

      await this.setState({patient: this.state.patientEdit}, () =>
        console.log('finally pased'),
      );
      // console.log('passing');
    }

    // console.log('handleFinish newConsultations.push');
    // console.log(newConsultations);

    console.log('handleFinish this.state.patient.age');
    console.log(this.state.patient.age);

    console.log('handleFinish NOW!!! newConsultations.push');

    await newConsultations.push({
      age: this.state.patient.age,
      phototype: this.state.patient.phototype,
      inmuno: this.state.patient.inmuno,

      lesions: this.state.lesions,
      quantity: this.state.lesions.length,
      date: Date.now(),
    });

    // console.log('handleFinish after newConsultations.push');
    // console.log(newConsultations);

    //   // eslint-disable-next-line prettier/prettier
    //   console.log('handleFinish screenPhoto isNewPatient? NO newConsultations');
    //   console.log(JSON.stringify(newConsultations));
    // }

    console.log('handleFinish this.state.patient');
    console.log(this.state.patient);

    patient = {
      ...this.state.patient,

      consultations: newConsultations,
    };

    console.log('handleFinish screenPhoto patient END toHandleSubmit');
    console.log(JSON.stringify(patient));

    await this.handleSubmit(patient);
  };

  //.
  handleList = (lesion, patient) => {
    console.log('handle List lesion');
    console.log(lesion);

    //Attach
    if (lesion !== null) {
      this.pushLesion(lesion);
    }

    console.log('ScreenPhotoInfo -> handleList -> this.state.patient');
    console.log(this.state.patient);

    if (this.state.isNewPatient) {
      if (patient !== null) {
        this.setState({patient, isWizzard: false});
      } else {
        this.setState({isWizzard: false});
      }
    } else {
      // console.log('handleList patientEdit');
      // console.log(patient);

      if (patient !== null) {
        this.setState({isWizzard: false, patientEdit: patient});
      } else {
        this.setState({isWizzard: false});
      }
    }
  };

  renderLesions = () => {
    return this.state.lesions.map((lesion, i) => (
      <CardLesionItem index={i} item={lesion} photo={lesion.photo} />
    ));
  };

  handleExitHome = () => {
    console.log('screenphotoInfo -> handleExitHome');

    if (this.props.isExistingPatient) {
      this.props.onExitHome();
      return;
    } else {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'MainTabs',
            params: {},
          }),
        ],
      });

      const goLogin = NavigationActions.navigate({
        routeName: 'Home',
        params: {},
      });

      this.props.navigation.dispatch(resetAction);
      this.props.navigation.dispatch(goLogin);
    }
  };

  render() {
    HideNavigationBar();

    console.log('screenphotoInfo -> render');

    return (
      <View style={[Styles.container]}>
        {(this.state.isWizzard || this.state.lesions.length === 0) && (
          <WizardLesion
            onExitHome={this.handleExitHome}
            isNewPatient={
              this.state.isNewPatient && this.state.lesions.length === 0
            }
            patientNumber={this.state.patientNumber}
            step={
              this.state.lesions.length > 0
                ? 2
                : this.state.isNewPatient
                ? 0
                : 1
            }
            onSubmit={this.handleList}
            isNewConsultation={
              !this.state.isNewPatient && this.state.lesions.length === 0
            }
            patient={
              !this.state.isNewPatient && this.state.lesions.length === 0
                ? this.state.patient
                : null
            }
            isCreatePatient={this.state.isNewPatient}
          />
        )}
        {!this.state.isWizzard && this.state.lesions.length > 0 && (
          <View>
            {!this.state.isSaving && (
              <View style={Styles.card}>
                <CardTitle
                  noIcon
                  title={i18.t('lesions-acquired')}
                  styleContainer={{justifyContent: 'flex-start'}}
                />
                <View style={Styles.cardContainer}>
                  <View style={Styles.cardBody}>
                    <View>
                      <ScrollView style={Styles.scrollView}>
                        {this.renderLesions()}
                      </ScrollView>
                    </View>
                    <Button
                      inverted
                      styleContainer={Styles.button}
                      styleButton={Styles.buttonInner}
                      text={i18.t('add-lesion')}
                      textLoading={i18.t('loading') + '...'}
                      onPress={this.handleNew}
                      disabled={this.state.buttonSubmitDisabled}
                      isLoading={this.state.isSigning}
                    />
                  </View>
                  <View style={Styles.cardFooterLesions}>
                    <ButtonFooter text=" " />
                    <ProgressCircles step={3} />
                    <ButtonFooter
                      next
                      text={i18.t('finish')}
                      onPress={this.handleFinish}
                    />
                  </View>
                </View>
              </View>
            )}
            {this.state.isSaving && (
              <View style={[Styles.card, Styles.uploadingContainer]}>
                {!this.state.isSavedOK && (
                  <View style={[Styles.card, Styles.uploadingContainer]}>
                    <Icon
                      name="cloud-upload-outline"
                      color={Colors.app}
                      size={90}
                    />
                    <Text style={Styles.uploadingDisclaimer}>
                      {i18.t('uploading-image')}
                    </Text>
                    <Text style={Styles.uploadingText}>{`${i18.t(
                      'currently-uploading',
                    )} ${(
                      this.state.uploadingImagesActual + 1
                    ).toString()} ${i18.t(
                      'out-of',
                    )} ${this.state.uploadingImagesTotal.toString()}`}</Text>
                  </View>
                )}
                {this.state.isSavedOK && (
                  <View style={[Styles.card, Styles.uploadingContainer]}>
                    <Icon
                      name="check-circle-outline"
                      color={Colors.app}
                      size={90}
                    />
                    <Text style={Styles.uploadingDisclaimer}>
                      {i18.t('upload-confirmation')}
                    </Text>
                  </View>
                )}
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
