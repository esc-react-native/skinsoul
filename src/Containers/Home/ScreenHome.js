import React, {Component} from 'react';
import {View, Text, ImageBackground, Platform, StatusBar} from 'react-native';
import {Images} from '../../Styles';
import ButtonHome from '../../Components/ButtonHome';
import Subtitle from '../../Components/Subtitle';
import {withNavigationFocus} from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';
import {HideNavigationBar} from 'react-native-navigation-bar-color';

// Styles
import Styles from './Styles/Home';

//Multilanguage
import i18 from '../../Localize/I18n';

class ScreenHome extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,

      button: false,
    };

    this.handleOption = this.handleOption.bind();
  }

  //.
  componentDidMount = () => {
    SplashScreen.hide();
    HideNavigationBar();
  };

  //.
  handleOption = option => {
    this.props.navigation.navigate('PatientNumber', {
      SearchType: option,
    });
  };

  //.
  render() {
    HideNavigationBar();

    return (
      <View style={[Styles.container]}>
        <StatusBar translucent backgroundColor="transparent" />
        <View>
          <ImageBackground
            source={Images.loginBackground}
            style={Styles.image}
            blurRadius={Platform.OS === 'ios' ? 10 : 2}>
            <View style={Styles.header}>
              <Text style={Styles.headerTitle}>SKIN</Text>
              <Text style={Styles.headerTitle}>SOUL</Text>
              <Subtitle
                text={i18.t('data-gathering-version')}
                style={Styles.subtitle}
              />
              <Text style={Styles.title}> </Text>
            </View>
            <View style={Styles.background} />
            <View style={Styles.buttonBar}>
              <View>
                <ButtonHome
                  style={Styles.buttonMarginLeft}
                  source={Images.icon_consultant}
                  text={i18.t('patients-consultations')}
                  onPress={() => this.handleOption('consultation')}
                />
              </View>
              <View>
                <ButtonHome
                  style={Styles.buttonMarginRight}
                  source={Images.icon_biopsy}
                  text={i18.t('add-biopsy-results')}
                  onPress={() => this.handleOption('biopsy')}
                />
              </View>
            </View>
          </ImageBackground>
        </View>
      </View>
    );
  }
}

export default withNavigationFocus(ScreenHome);
