/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';

import Moment from 'moment';


import {API} from 'aws-amplify';
import TitleTab from '../../Components/TitleTab';
import GeneralLabelValue from '../../Components/GeneralLabelValue';
import ScreenPhotoInfo from './ScreenPhotoInfo';

import {s3Get} from '../../libs/awsLib';

import {HideNavigationBar} from 'react-native-navigation-bar-color';

// Styles
import Styles from './Styles/Consultation';

//Multilanguage
import i18 from '../../Localize/I18n';


import Button from '../../Components/Button';
import CardTitle from '../../Components/CardTitle';

export default class ScreenConsultation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isConfirmation: false,

      SearchType: this.props.navigation.getParam('SearchType', 'NO-ID'),
      patient: this.props.navigation.getParam('Patient', 'NO-ID'),
      patientNumber: this.props.navigation.getParam('PatientNumber', 'NO-ID'),
      consultations: [],

      isWizzard: false,
      actualTab: 0,

      isConsultationDetail: false,
      consultation: null,

      isLesionDetail: false,

    };

    this.handleChangeTab = this.handleChangeTab.bind();
    this.handleBiopsyResult = this.handleBiopsyResult.bind();
    this.lesionDetail = this.lesionDetail.bind();
  }

  recordUpdate = (id, body) => {
    console.log(`PUT /patients/${id}`);
    console.log(body);

    return API.put('patients', `/patients/${id}`, {
      body: body,
    });
  };

  handleSubmit = async patient => {
    this.setState({isSaving: true});

    console.log('ScreenConsultations --> handleSubmit --> Saving...');
    console.log(JSON.stringify(patient));

    try {
      await this.recordUpdate(patient.patientNumber, patient);
      console.log('Updated');

      this.setState({isSaving: false, patient, isWizzard: false});

      //Reload and Refresh...
    } catch (e) {
      console.log('Error Updating');
      console.log(e);
      console.log(JSON.stringify(e));

      this.setState({isSaving: false});
    }
  };

  handleNew = () => {
    console.log('handleNew');
    this.setState({isWizzard: true});
  };

  handleChangeTab = (tab) => {
    let actualTab = tab;

    this.setState({actualTab});

  };

  handleFinish = () => {
    //Call to Save Patient Record
    const patient = {
      ...this.state.patient,

      lesions: this.state.lesions,
    };

    console.log('record to save');
    console.log(JSON.stringify(patient));

    this.handleSubmit(patient);
  };

  renderHeaderTable = () => {
    return (
      <View style={Styles.consultationHeader}>
        <View style={Styles.consultationDateViewHeader}>
          <Text style={Styles.consultationDateText}>{i18.t('date')}</Text>
        </View>
        <View style={Styles.consultationNumView}>
          <Text style={Styles.consultationNumText}>{'#Num'}</Text>
        </View>
        <View style={Styles.consultationNoteView}>
          <Text style={Styles.consultationNoteHeader}>{i18.t('notes')}</Text>
        </View>
      </View>
    );
  };

  //.
  handleBiopsyResult = (consultation) => {
    console.log('consultation');
    console.log(consultation);
    this.setState({consultation}, this.setState({isConsultationDetail : true}));
  }

  //.
  lesionDetail = (lesion) => {
    // this.setState({isLesionDetail: true});

  }

  //.
  renderLesions = () => {
    let lesions = this.state.consultation.lesions;

    console.log('renderLesions --> lesions');
    console.log(lesions);


    return lesions.map((lesion, i) => (
      <View key={i} style={Styles.cardItem}>
        <Image source={{uri: lesion.photoURL}} style={[Styles.cardImage]} />
        <View style={Styles.cardLabel}>
          <Text style={Styles.cardLabelTitle}>{`${i18.t('lesion')} #${i +
            1}`}</Text>
          <Text style={Styles.cardLabelDescription}>{`${
            lesion.bleedingLession ? i18.t('bleeding') : i18.t('not-bleeding')
          } ${i18.t('card-disclaimer-1')} ${lesion.location} ${i18.t(
            'card-disclaimer-2',
          )} ${lesion.diagnosis}.`}</Text>
          <Text style={Styles.cardLabelFooter}>{`${
            lesion.sendForBiopsy
              ? i18.t('lesion-sent-biopsy')
              : i18.t('lesion-not-sent-biopsy')
          }`}</Text>
          <View style={Styles.cardButtomFooterView}>
            <TouchableOpacity onPress={this.lesionDetail(lesion)} >
              <Text>UPDATE</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    ));
  };

  renderConsultations = () => {
    console.log('this.state.patient.consultations');
    console.log(JSON.stringify(this.state.patient.consultations));

    const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    const consultations = this.state.patient.consultations;

    Moment.locale('en');

    console.log('CONSULTATIONS');
    console.log(JSON.stringify(this.state.patient.consultations));

    if(this.state.SearchType === 'consultation'){
      return consultations.map((item, i) => (
        <View key={i} style={Styles.consultationView}>
          <View style={Styles.consultationDateView}>
            <Text style={Styles.consultationDateText}>{Moment(item.date).format('YYYY-MM-DD')}</Text>
          </View>
          <View style={Styles.consultationNumView}>
            <Text style={Styles.consultationNumText}>{item.quantity}</Text>
          </View>
          <View style={Styles.consultationNoteView}>
            <Text style={Styles.consultationNoteText}>{`${
              item.lesions[0].bleedingLession ? i18.t('bleeding') : i18.t('not-bleeding')}. ${item.lesions[0].location} ${item.lesions[0].diagnosis}.`}</Text>
          </View>
        </View>
      ));
    } else {
      return consultations.map((item, i) => (
        <TouchableOpacity
          onPress={() => this.handleBiopsyResult(item)}
          key={i} style={Styles.consultationView}>
          <View style={Styles.consultationDateView}>
            <Text style={Styles.consultationDateText}>{Moment(item.date).format('YYYY-MM-DD')}</Text>
          </View>
          <View style={Styles.consultationNumView}>
            <Text style={Styles.consultationNumText}>{item.quantity}</Text>
          </View>
          <View style={Styles.consultationNoteView}>
            <Text style={Styles.consultationNoteText}>{`${
              item.lesions[0].bleedingLession ? i18.t('bleeding') : i18.t('not-bleeding')}. ${item.lesions[0].location} ${item.lesions[0].diagnosis}.`}</Text>
          </View>
          </TouchableOpacity>
      ));
    }
  };

  render() {
    let SearchType = this.props.navigation.getParam('SearchType', 'NO-ID');

    HideNavigationBar();

    console.log('this.state.patient');
    console.log(this.state.patient);

    return (
      <View style={Styles.container}>
        {this.state.isWizzard && !this.state.isConsultationDetail && (
          <ScreenPhotoInfo
            isExistingPatient={true}
            patientNumber={this.state.patientNumber}
            patient={this.state.patient}
            step={1}
            onSubmit={this.handleSubmit}
          />
        )}
        {!this.state.isWizzard && !this.state.isConsultationDetail && (
          <View style={Styles.card}>
            <TitleTab
              onPress={this.handleChangeTab}
              actualTab={this.state.actualTab}
              tabText1={i18.t('general-info')}
              tabText2={i18.t('consultations')}
            />
            <View style={Styles.cardContainer}>
              <View style={Styles.cardBody}>
                <Text style={Styles.cardBodyTitle}>
                  {this.state.patientNumber}
                </Text>
                {this.state.actualTab === 0 && (
                  <View>
                    <GeneralLabelValue style={Styles.labelValue} label={i18.t('gender')} text={i18.t(this.state.patient.gender)}/>
                    <GeneralLabelValue label={i18.t('age')} text={this.state.patient.age}/>
                    <GeneralLabelValue label={i18.t('phototype')} text={this.state.patient.phototype}/>
                    <GeneralLabelValue label={i18.t('immuno') + '-'} secondLine={i18.t('compromised')} text={this.state.patient.inmuno? i18.t('yes'): i18.t('no')}/>
                    {/* <GeneralLabelValue label={i18.t('compromised')} text={''}/> */}
                  </View>
                )}
                {this.state.actualTab === 1 && (
                  <View style={Styles.consultationBody}>
                    <View>
                      {this.renderHeaderTable()}
                      <ScrollView style={Styles.scrollView}>
                      {this.renderConsultations()}
                      </ScrollView>
                    </View>
                    { SearchType === 'consultation' && (
                      <View style={Styles.buttonContainer}>
                        <Button
                          inverted
                          style={Styles.button}
                          text={'New Consultation'}
                          textLoading={i18.t('loading') + '...'}
                          onPress={this.handleNew}
                          disabled={this.state.buttonSubmitDisabled}
                          isLoading={this.state.isProcessing}
                        />
                      </View>
                    )}
                  </View>
                )}
              </View>

            </View>
          </View>
        )}

        {!this.state.isWizzard && this.state.isConsultationDetail && this.state.SearchType === 'biopsy' && (
          <View style={Styles.card}>
            <CardTitle
              iconName={'list-alt'
              }
              title={this.state.patientNumber}
              subtitle={`${i18.t('consultation')} ${Moment(this.state.consultation.date).format('YYYY-MM-DD')}`}
            />
            <View style={Styles.cardContainer}>
              <View style={Styles.cardBody}>
                <Text style={Styles.cardBodyTitleConsultation}>
                  {`${i18.t('consultation')} ${Moment(this.state.consultation.date).format('YYYY-MM-DD')}`}
                </Text>
                <ScrollView style={Styles.scrollView}>
                    {this.renderLesions()}
                  </ScrollView>
              </View>

            </View>
          </View>
        )}

      </View>
    );
  }
}
