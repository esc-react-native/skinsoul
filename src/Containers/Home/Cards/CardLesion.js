import React, {Component} from 'react';
import {View, Text} from 'react-native';

import {Images} from '../../../Styles';

import {
  Switchbox,
  Dropdown,
  DropdownMaterial,
  DropdownModal,
  Textarea,
} from '../../../Components/Base';

import {API} from 'aws-amplify';

import ButtonFooter from '../../../Components/ButtonFooter';
import ProgressCircles from '../../../Components/ProgressCircles';
import InputText from '../../../Components/InputText';

// Styles
import Styles from './Styles/PatientNew';

//Multilanguage
import i18 from '../../../Localize/I18n';
import CardTitle from '../../../Components/CardTitle';

export default class CardLesion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isConfirmation: false,

      bleedingLession: false,
      sendForBiopsy: false,
      location: null,
      diagnosis: null,
      notes: null,

      locations: [
        {id: null, label: i18.t('location'), value: null},
        {id: 'head', label: i18.t('head'), value: 'head'},
        {id: 'neck', label: i18.t('neck'), value: 'neck'},
        {id: 'chest', label: i18.t('chest'), value: 'chest'},
        {id: 'abdomen', label: i18.t('abdomen'), value: 'abdomen'},
        {id: 'back-body', label: i18.t('back-body'), value: 'back-body'},
        {
          id: 'shoulder-left',
          label: i18.t('shoulder-left'),
          value: 'shoulder-left',
        },
        {
          id: 'shoulder-right',
          label: i18.t('shoulder-right'),
          value: 'shoulder-right',
        },
        {id: 'arm-left', label: i18.t('arm-left'), value: 'arm-left'},
        {id: 'arm-right', label: i18.t('arm-right'), value: 'arm-right'},
        {
          id: 'forearm-left',
          label: i18.t('forearm-left'),
          value: 'forearm-left',
        },
        {
          id: 'forearm-right',
          label: i18.t('forearm-right'),
          value: 'forearm-right',
        },
        {id: 'hand-left', label: i18.t('hand-left'), value: 'hand-left'},
        {id: 'hand-right', label: i18.t('hand-right'), value: 'hand-right'},
        {id: 'groin', label: i18.t('groin'), value: 'groin'},
        {id: 'butt', label: i18.t('butt'), value: 'butt'},
        {id: 'thigh-left', label: i18.t('thigh-left'), value: 'thigh-left'},
        {id: 'thigh-right', label: i18.t('thigh-right'), value: 'thigh-right'},
        {id: 'leg-left', label: i18.t('leg-left'), value: 'leg-left'},
        {id: 'leg-right', label: i18.t('leg-right'), value: 'leg-right'},
        {id: 'foot-left', label: i18.t('foot-left'), value: 'foot-left'},
        {id: 'foot-right', label: i18.t('foot-right'), value: 'foot-right'},
      ],

      diagnosisOptions: [
        {
          id: 'merkel-cell-carcinoma',
          label: i18.t('merkel-cell-carcinoma'),
          value: 'merkel-cell-carcinoma',
        },
        {
          id: 'malignant-melanoma-amelanotic',
          label: i18.t('malignant-melanoma-amelanotic'),
          value: 'malignant-melanoma-amelanotic',
        },
        {
          id: 'magliant-melanoma-pigmented',
          label: i18.t('magliant-melanoma-pigmented'),
          value: 'magliant-melanoma-pigmented',
        },
        {
          id: 'malignant-cutaneous-lymphoma-tcell',
          label: i18.t('malignant-cutaneous-lymphoma-tcell'),
          value: 'malignant-cutaneous-lymphoma-tcell',
        },
        {
          id: 'malignant-cutaneous-lymphome-bcell',
          label: i18.t('malignant-cutaneous-lymphome-bcell'),
          value: 'malignant-cutaneous-lymphome-bcell',
        },
        {
          id: 'malignant-epidermal-basal-cell-carcinoma',
          label: i18.t('malignant-epidermal-basal-cell-carcinoma'),
          value: 'malignant-epidermal-basal-cell-carcinoma',
        },
        {
          id: 'malignant-epidermal-basal-squamous',
          label: i18.t('malignant-epidermal-basal-squamous'),
          value: 'malignant-epidermal-basal-squamous',
        },
        {
          id: 'malignant-dermal-angiosarcoma',
          label: i18.t('malignant-dermal-angiosarcoma'),
          value: 'malignant-dermal-angiosarcoma',
        },
        {
          id: 'malignant-dermal-merkel',
          label: i18.t('malignant-dermal-merkel'),
          value: 'malignant-dermal-merkel',
        },
        {
          id: 'bening-dermal-cyst',
          label: i18.t('bening-dermal-cyst'),
          value: 'bening-dermal-cyst',
        },
        {
          id: 'bening-dermal-fibroma',
          label: i18.t('bening-dermal-fibroma'),
          value: 'bening-dermal-fibroma',
        },
        {
          id: 'bening-dermal-lipoma',
          label: i18.t('bening-dermal-lipoma'),
          value: 'bening-dermal-lipoma',
        },
        {
          id: 'bening-melanoctyc-blue',
          label: i18.t('bening-melanoctyc-blue'),
          value: 'bening-melanoctyc-blue',
        },
        {
          id: 'bening-melanoctyc-solar',
          label: i18.t('bening-melanoctyc-solar'),
          value: 'bening-melanoctyc-solar',
        },
        {
          id: 'bening-melanoctyc-café',
          label: i18.t('bening-melanoctyc-café'),
          value: 'bening-melanoctyc-café',
        },
        {
          id: 'bening-epidermal-milia',
          label: i18.t('bening-epidermal-milia'),
          value: 'bening-epidermal-milia',
        },
        {
          id: 'bening-epidermal-seborrheic',
          label: i18.t('bening-epidermal-seborrheic'),
          value: 'bening-epidermal-seborrheic',
        },
        {
          id: 'neoplastic-genodermatosis-congenital',
          label: i18.t('neoplastic-genodermatosis-congenital'),
          value: 'neoplastic-genodermatosis-congenital',
        },
        {
          id: 'neoplastic-genodermatosis-tuberous',
          label: i18.t('neoplastic-genodermatosis-tuberous'),
          value: 'neoplastic-genodermatosis-tuberous',
        },
        {
          id: 'neoplastic-inflamatory-psoriasis',
          label: i18.t('neoplastic-inflamatory-psoriasis'),
          value: 'neoplastic-inflamatory-psoriasis',
        },
        {
          id: 'neoplastic-inflamatory-stevens',
          label: i18.t('neoplastic-inflamatory-stevens'),
          value: 'neoplastic-inflamatory-stevens',
        },
        {
          id: 'neoplastic-inflamatory-rosacea',
          label: i18.t('neoplastic-inflamatory-rosacea'),
          value: 'neoplastic-inflamatory-rosacea',
        },
        {
          id: 'neoplastic-inflamatory-acne',
          label: i18.t('neoplastic-inflamatory-acne'),
          value: 'neoplastic-inflamatory-acne',
        },
        {
          id: 'neoplastic-inflamatory-abrasion',
          label: i18.t('neoplastic-inflamatory-abrasion'),
          value: 'neoplastic-inflamatory-abrasion',
        },
        {
          id: 'neoplastic-blistering-vulgaris',
          label: i18.t('neoplastic-blistering-vulgaris'),
          value: 'neoplastic-blistering-vulgaris',
        },
        {
          id: 'neoplastic-blistering-bullous',
          label: i18.t('neoplastic-blistering-bullous'),
          value: 'neoplastic-blistering-bullous',
        },
        {
          id: 'neoplastic-blistering-foliaceus',
          label: i18.t('neoplastic-blistering-foliaceus'),
          value: 'neoplastic-blistering-foliaceus',
        },
        {
          id: 'subungueal-melanoma',
          label: i18.t('subungueal-melanoma'),
          value: 'subungueal-melanoma',
        },
        {
          id: 'melanonychia',
          label: i18.t('melanonychia'),
          value: 'melanonychia',
        },
      ],

      buttonSubmitDisabled: false,
    };

    this.handleValidate = this.handleValidate.bind();
    this.handleSubmit = this.handleSubmit.bind();
    this.handleChangeValue = this.handleChangeValue.bind();
  }

  handleValidate = () => {
    // console.log(this.state);

    return (
      this.state.location !== null &&
      this.state.diagnosis !== null &&
      // this.state.notes !== null &&
      this.state.location.length > 0 &&
      this.state.diagnosis.length > 0
      // this.state.notes.length > 0
    );
  };

  //.
  handleChangeText = (type, text) => {
    this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: this.handleValidate()});
    });
  };

  //.
  handleChangeValue = async (type, text) => {
    if (text === null) {
      return;
    }

    await this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: this.handleValidate()});
    });
  };

  handleSubmit = () => {
    if (!this.handleValidate()) return;

    const lesion = {
      bleedingLession: this.state.bleedingLession,
      sendForBiopsy: this.state.sendForBiopsy,
      location: this.state.location,
      diagnosis: this.state.diagnosis,
      notes:
        this.state.notes === null || this.state.notes.length === 0
          ? ' '
          : this.state.notes,
    };

    this.props.onSubmit(lesion);

    return;
  };

  render() {
    console.log('CardLesion -> Render -> this.state.location');
    console.log(this.state.location);

    return (
      <View style={Styles.card}>
        <CardTitle
          noIcon
          title={'Lesion Info'}
          styleContainer={Styles.titleLesionInfo}
        />
        <View style={Styles.cardContainer}>
          <View style={[Styles.cardBody, Styles.cardBodyX]}>
            <Switchbox
              yesno
              checked={this.state.bleedingLession}
              text={i18.t('bleeding-lesion')}
              onPress={() => {
                this.setState({
                  bleedingLession: !this.state.bleedingLession,
                });
              }}
              styleContainer={Styles.lesionSwitchContainer}
            />
            <Switchbox
              yesno
              checked={this.state.sendForBiopsy}
              text={i18.t('send-for-biopsy')}
              onPress={() => {
                this.setState({
                  sendForBiopsy: !this.state.sendForBiopsy,
                });
              }}
              styleContainer={Styles.lesionSwitchContainer}
            />
            <DropdownMaterial
              styleContainer={Styles.genderContainer}
              styleView={Styles.genderView}
              stylePlaceholder={Styles.placeholderGender}
              styleFont={Styles.placeholderDiagnosis}
              placeholder={i18.t('location')}
              value={this.state.location}
              options={this.state.locations}
              onValueChange={value => {
                this.handleChangeValue('location', value);
              }}
            />
            <DropdownModal
              placeholderStyle={Styles.placeholderDiagnosis}
              placeholder={i18.t('diagnosis')}
              value={this.state.diagnosis}
              options={this.state.diagnosisOptions}
              onValueChange={value => {
                this.handleChangeValue('diagnosis', value);
              }}
              styleContainer={Styles.lesionDropdownContainer}
            />
            <Textarea
              styleContainer={Styles.lesionTextAreaContainer}
              placeholder={i18.t('notes')}
              onChangeText={text => this.handleChangeText('notes', text)}
              value={this.state.notes}
            />
          </View>
          <View style={Styles.cardFooter}>
            <ButtonFooter
              back
              text={i18.t('prev').toUpperCase()}
              onPress={() => this.props.onBack()}
            />
            <ProgressCircles step={2} />

            <ButtonFooter
              text={
                this.state.buttonSubmitDisabled
                  ? i18.t('add').toUpperCase()
                  : ''
              }
              onPress={this.handleSubmit}
            />
          </View>
        </View>
      </View>
    );
  }
}
