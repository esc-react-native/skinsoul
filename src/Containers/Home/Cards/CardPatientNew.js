import React, {Component} from 'react';
import {View, Text} from 'react-native';

import {Images} from '../../../Styles';

import {
  Switchbox,
  Dropdown,
  DropdownModal,
  DropdownMaterial,
} from '../../../Components/Base';

import Icon from 'react-native-vector-icons/FontAwesome';

import {API} from 'aws-amplify';

import ButtonFooter from '../../../Components/ButtonFooter';
import ProgressCircles from '../../../Components/ProgressCircles';
import InputText from '../../../Components/InputText';
import CardTitle from '../../../Components/CardTitle';
// Styles
import Styles from './Styles/PatientNew';

//Multilanguage
import i18 from '../../../Localize/I18n';

export default class CardPatientNew extends Component {
  constructor(props) {
    super(props);

    console.log('CardPatientNew this.props.patient');
    console.log(this.props.patient);

    this.state = {
      isLoading: false,
      isConfirmation: false,

      patient: this.props.patient,
      patientNumber: this.props.patientNumber,
      age: this.props.patient === null ? null : this.props.patient.age,
      gender: this.props.patient === null ? null : this.props.patient.gender,
      phototype:
        this.props.patient === null ? null : this.props.patient.phototype,
      inmuno: this.props.patient === null ? false : this.props.patient.inmuno,

      buttonEnabled: false,
    };

    this.handleValidate = this.handleValidate.bind();
    this.handleSubmit = this.handleSubmit.bind();
    this.handleChangeValue = this.handleChangeValue.bind();
  }

  componentDidMount = () => {
    if (this.handleValidate()) this.setState({buttonEnabled: true});
  };

  handleValidate = () => {
    // console.log('validate');
    // console.log(`this.state.gender: ${this.state.gender}`);
    // console.log(`this.state.age: ${this.state.age}`);
    // console.log(`this.state.phototype: ${this.state.phototype}`);
    // console.log(
    //   `this.state.patientNumber !== null ${this.state.patientNumber !==
    //     null}\nthis.state.phototype !== null ${this.state.phototype !==
    //     null}\nthis.state.gender !== null ${this.state.gender !==
    //     null}\nthis.state.age !== null ${this.state.age !==
    //     null}\nthis.state.patientNumber.length > 0 ${
    //     this.state.patientNumber === null
    //       ? 'null'
    //       : this.state.patientNumber.length > 0
    //   }\nthis.state.gender.length > 0 ${
    //     this.state.gender === null ? 'null' : this.state.gender.length > 0
    //   }\nthis.state.age.length > 0 ${
    //     this.state.age === null ? 'null' : this.state.age.length > 0
    //   }\nthis.state.phototype.length > 0 ${
    //     this.state.phototype === null ? 'null' : this.state.phototype.length > 0
    //   }`,
    // );
    // console.log(
    //   this.state.patientNumber !== null &&
    //     this.state.phototype !== null &&
    //     this.state.gender !== null &&
    //     this.state.age !== null &&
    //     this.state.patientNumber.length > 0 &&
    //     this.state.gender.length > 0 &&
    //     this.state.age.length > 0 &&
    //     this.state.phototype.length > 0,
    // );

    return (
      this.state.patientNumber !== null &&
      this.state.phototype !== null &&
      this.state.gender !== null &&
      this.state.age !== null &&
      this.state.patientNumber.length > 0 &&
      this.state.gender.length > 0 &&
      this.state.age.length > 0 &&
      this.state.phototype.length > 0
    );
  };

  //.
  handleChangeText = (type, text) => {
    const newText = text.replace(/[^0-9]/g, '').toUpperCase();

    this.setState({[type]: newText}, () =>
      this.setState({buttonEnabled: this.handleValidate()}),
    );
  };

  //.
  handleChangeValue = (type, text) => {
    if (text === null) {
      return;
    }

    this.setState({[type]: text}, () =>
      this.setState({buttonEnabled: this.handleValidate()}),
    );
  };

  handleSubmit = () => {
    const patient = {
      patientNumber: this.state.patientNumber,
      age: this.state.age,
      gender: this.state.gender,
      phototype: this.state.phototype,
      inmuno: this.state.inmuno,
      consultations: [],
    };

    this.props.onSubmit(patient);

    return;
  };

  render() {
    const genders = [
      {id: null, label: i18.t('gender'), value: null},
      {id: 'male', label: i18.t('male'), value: 'male'},
      {id: 'female', label: i18.t('female'), value: 'female'},
    ];

    const phototypes = [
      {id: null, label: i18.t('phototype'), value: null},
      {id: 'I', label: 'I', value: 'I'},
      {id: 'II', label: 'II', value: 'II'},
      {id: 'III', label: 'III', value: 'III'},
      {id: 'IV', label: 'IV', value: 'IV'},
      {id: 'V', label: 'V', value: 'V'},
      {id: 'VI', label: 'VI', value: 'VI'},
    ];

    console.log('cardPatientNew -> Render');

    return (
      <View style={Styles.card}>
        <CardTitle
          iconName={'user-plus'}
          title={
            this.props.editable
              ? i18.t('new-consultation')
              : i18.t('new-patient')
          }
        />
        <View style={Styles.cardContainer}>
          <View style={[Styles.cardBody, Styles.cardBodyX]}>
            <Text style={Styles.cardBodyTitle}>{i18.t('general-info')}</Text>
            <InputText
              styleContainer={Styles.inputContainer}
              styleView={Styles.inputView}
              styleInput={Styles.input}
              transparent
              label={i18.t('patient-id')}
              value={this.state.patientNumber}
              disabled={true}
            />
            {!this.props.editable && (
              <DropdownMaterial
                stylesContainer={Styles.genderContainer}
                styleView={Styles.genderView}
                stylePlaceholder={Styles.placeholderGender}
                placeholder={i18.t('gender')}
                value={this.state.gender}
                options={genders}
                onValueChange={value => {
                  this.handleChangeValue('gender', value);
                }}
              />
            )}
            {this.props.editable && (
              <InputText
                styleContainer={Styles.inputGenderContainer}
                styleView={Styles.inputView}
                styleInput={Styles.input}
                transparent
                label={i18.t('gender')}
                value={i18.t(this.state.gender)}
                disabled={true}
              />
            )}
            <InputText
              styleContainer={Styles.inputContainerAge}
              styleView={Styles.inputView}
              styleInput={Styles.input}
              //transparent
              placeholder={i18.t('age')}
              placeholderTextColor={'#6D6D6D'}
              label={i18.t('age').toUpperCase()}
              value={this.state.age}
              //disabled={false}
              //autoCapitalize="none"
              //autoCorrect={false}
              onChangeText={text => this.handleChangeText('age', text)}
            />
            <DropdownMaterial
              styleContainer={Styles.phototypeContainer}
              styleView={Styles.genderView}
              stylePlaceholder={Styles.placeholderGender}
              placeholder={i18.t('phototype')}
              placeholderTextColor={'#6D6D6D'}
              value={this.state.phototype}
              options={phototypes}
              onValueChange={value => {
                this.handleChangeValue('phototype', value);
              }}
            />
            <Switchbox
              yesno
              styleContainer={Styles.switchContainer}
              checked={this.state.inmuno}
              text={i18.t('immunocompromised')}
              onPress={() => {
                this.setState({
                  inmuno: !this.state.inmuno,
                });
              }}
            />
          </View>
          <View style={Styles.cardFooter}>
            <ButtonFooter text={''} />
            <ProgressCircles step={0} />
            {this.state.buttonEnabled && (
              <ButtonFooter
                text={i18.t('next').toUpperCase()}
                onPress={this.handleSubmit}
              />
            )}
            {!this.state.buttonEnabled && <ButtonFooter text={''} />}
          </View>
        </View>
      </View>
      // </View>
    );
  }
}
