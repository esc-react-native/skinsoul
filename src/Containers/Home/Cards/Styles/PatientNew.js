import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../../Styles';

const INPUT_WIDTH = 250 * Metrics.screenDesignWidth;
const CONTAINER_PAD_VERTICAL = 10 * Metrics.screenDesignHeight;

const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.background,
    height: Metrics.screenHeight,
  },

  backPreview: {
    height: 75,
    width: 45,
  },

  cameraPreview: {
    height: '100%',
    width: '100%',
  },

  photo: {
    resizeMode: 'contain',
    marginTop: Metrics.marginVerticalLarge,
  },

  image: {
    width: Metrics.screenWidth,
    height: (Metrics.bodyHeightNoHeader * 2) / 3,
    resizeMode: 'stretch',
  },

  cardBodyX: {
    marginBottom: 0,
    height: Metrics.screenDesignHeight * 550,
    justifyContent: 'space-between',
  },

  titleLesionInfo: {
    justifyContent: 'flex-start',
  },

  inputContainer: {
    marginBottom: Metrics.screenDesignHeight * 34,
  },

  inputGenderContainer: {
    marginBottom: Metrics.screenDesignHeight * 100,
  },

  genderContainer: {
    marginTop: CONTAINER_PAD_VERTICAL,
    marginBottom: 50 * Metrics.screenDesignHeight,
  },

  phototypeContainer: {
    marginTop: CONTAINER_PAD_VERTICAL,
    marginBottom: 10 * Metrics.screenDesignHeight,
  },

  inputContainerAge: {
    marginTop: Metrics.screenDesignHeight * 10,
    marginBottom: Metrics.screenDesignHeight * 20,
  },

  switchContainer: {
    marginTop: Metrics.screenDesignHeight * 20,
    paddingLeft: Metrics.screenDesignWidth * 12,
  },

  lesionSwitchContainer: {
    marginBottom: Metrics.screenDesignHeight * 20,
    paddingLeft: Metrics.screenDesignWidth * 12,

    borderColor: '#FFF',
    borderWidth: 1,
    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
  },

  lesionDropdownContainer: {
    marginTop: Metrics.screenDesignHeight * 20,
    paddingLeft: Metrics.screenDesignWidth * 12,
  },

  lesionTextAreaContainer: {
    marginTop: Metrics.screenDesignHeight * 14,
  },

  dropdownContainer: {
    marginBottom: Metrics.screenDesignHeight * 20,
  },

  inputView: {
    width: Metrics.screenDesignWidth * 255,
    marginLeft: 0,
  },

  genderView: {
    width: Metrics.screenDesignWidth * 255,
    marginLeft: 0,

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
  },

  input: {
    ...Fonts.style.normal,

    color: '#6D6D6D',
    width: Metrics.screenDesignWidth * 255,
  },

  placeholderGender: {
    ...Fonts.style.normal,

    fontSize: 16,
    color: '#6D6D6D',
  },

  placeholderDiagnosis: {
    ...Fonts.style.normal,

    color: '#6D6D6D',
    fontSize: 14,
  },
});

export default styles;
