import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../../Styles';

const INPUT_WIDTH = 250 * Metrics.screenDesignWidth;
const CONTAINER_PAD_VERTICAL = 10 * Metrics.screenDesignHeight;


const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.background,
    //backgroundColor: 'red',
    height: Metrics.screenHeight,
  },


  backPreview: {
    backgroundColor: 'blue',
    height: 75,
    width: 45,
  },

  cameraPreview: {
    // height: '10%',
    //     width: '10%',
    //backgroundColor: 'red',
    //marginHorizontal: '4%',
    height: '100%',
    width: '100%',
  },

  cardBody: {
    alignItems: 'center'
    //backgroundColor: 'red',
  },

  cardBodyTitle: {

  },

  photo: {
    resizeMode: 'contain',
    marginTop: Metrics.marginVerticalLarge,
  },

  image: {
    width: Metrics.screenWidth,
    height: (Metrics.bodyHeightNoHeader * 2) / 3,
    resizeMode: 'stretch',
  },

  cardBodyX: {
    marginBottom: 0,
  },

  inputContainer: {
    width: Metrics.screenWidth * 0.75,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },

  inputView: {
    width: Metrics.screenWidth * 0.75,
    marginLeft: 0,
  },

  input: {
    width: Metrics.screenWidth * 0.75,
  },
});

export default styles;
