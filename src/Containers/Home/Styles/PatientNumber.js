import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    height: Metrics.bodyHeight,
    width: Metrics.screenWidth,
    alignItems: 'center',
    flexDirection: 'column',
    paddingVertical: Metrics.marginVertical,
    justifyContent: 'center',
    paddingBottom: 100,
    backgroundColor: Colors.background,
  },

  subtitle: {
    ...Fonts.style.normal,

    color: Colors.subtitle,
    fontSize: Fonts.size.h5,
    marginVertical: Metrics.marginVerticalSmall,
  },

  input: {
    ...Fonts.style.normal,

    height: Metrics.navBarHeight * 2,
    marginBottom: Metrics.marginVerticalSmall,
    width: Metrics.screenWidth * (2 / 3),
    color: Colors.textDark,
    backgroundColor: '#FFF',
    fontSize: Fonts.size.h6,
    borderRadius: Metrics.inputRadius,
    paddingLeft: Metrics.marginHorizontal,
  },

  button: {
    marginTop: Metrics.marginVertical,
  },

  buttonText: {
    ...Fonts.style.h6,
    textAlign: 'center',
    color: '#FFF',
  },

});

export default styles;
