import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const CARD_WIDTH = Metrics.screenDesignWidth * 320;
const BUTTON_MARGIN_TOP = 10 * Metrics.screenDesignHeight;

const SCROLL_HEIGHT = 20 * Metrics.screenDesignHeight;

const TITLE_MARGIN_TOP = 19 * Metrics.screenDesignHeight;
const BODY_MARGIN_TOP = 20 * Metrics.screenDesignHeight;

const ROW_DATE_WIDTH = 75 * Metrics.screenDesignWidth;
const ROW_QUANTITY_WIDTH = 63 * Metrics.screenDesignWidth;
const ROW_NOTES_WIDTH = 180 * Metrics.screenDesignWidth;
const ROW_HEIGHT = 30 * Metrics.screenDesignHeight;

const CONSULTATION_BODY_HEIGHT = 340 * Metrics.screenDesignHeight;

const CARD_IMAGE_HEIGHT = 64 * Metrics.screenDesignHeight;
const CARD_IMAGE_WIDTH = 68 * Metrics.screenDesignWidth;

const CARD_ITEM_HEIGHT = 120 * Metrics.screenDesignHeight;
const CARD_ITEM_WIDTH = 300 * Metrics.screenDesignWidth;

const CARD_LABEL_TITLE_WIDTH = 214 * Metrics.screenDesignWidth;
const CARD_LABEL_TITLE_HEIGHT = 20 * Metrics.screenDesignHeight;

const CARD_DESCRIPTION_HEIGHT = 30 * Metrics.screenDesignHeight;
const CARD_FOOTER_HEIGHT = 10 * Metrics.screenDesignHeight;
const CARD_BUTTON_FOOTER_HEIGHT = 22 * Metrics.screenDesignHeight;

const CARD_CONSULTATION_TITLE_HEIGHT = 19 * Metrics.screenDesignHeight;
const CARD_CONSULTATION_TITLE_MARGIN_VERTICAL = 17 * Metrics.screenDesignHeight;

const BUTTON_UPDATE_HEGHT = 20 * Metrics.screenDesignHeight;
const BUTTON_UPDATE_WIDTH = 75 * Metrics.screenDesignWidth;
const BUTTON_UPDATE_MARGIN_RIGHT = 20 * Metrics.screenDesignWidth;

const CARD_LESION_DESC_HEIGHT = 45 * Metrics.screenDesignHeight;
const CARD_LESION_DESC_WIDTH = 190 * Metrics.screenDesignWidth;

const LESION_BODY_HEIGHT = 420 * Metrics.screenDesignHeight;
const LESION_FOOTER_HEIGHT = 50 * Metrics.screenDesignHeight;

const CONTAINER_MARGIN_VERTICAL = 7 * Metrics.screenDesignWidth;
const CONTAINER_PADDING_HORIZONTAL = 10 * Metrics.screenDesignWidth;
const CONTAINER_HEIGHT = 30 * Metrics.screenDesignHeight;
const CONTAINER_WIDTH = Metrics.screenWidth * 0.7;


const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    alignItems: 'center',
    flexDirection: 'column',
    //justifyContent: 'center',
    backgroundColor: Colors.background,
    //backgroundColor: 'red',
    height: Metrics.screenHeight,
  },

  cardBody: {
    alignItems: 'center',
    // backgroundColor: 'yellow',
  },

  cardBodyConsultation: {
    alignItems: 'center',
    width: Metrics.screenWidth,
  },

  cardBodyTitle: {
    ...Fonts.style.normalBold,
    marginTop: Metrics.screenDesignHeight * 15,
    color: Colors.app,
    fontSize: 20,
    //lineHeight: 0,
    // backgroundColor: 'blue',
  },

  consultationHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  consultationView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopColor: '#ECECEC',
    borderBottomColor: '#ECECEC',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingVertical: 1,
    // backgroundColor: 'red',
  },

  consultationDateView: {
    width: ROW_DATE_WIDTH,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Metrics.screenDesignWidth * 5,
    // backgroundColor: 'yellow',
  },

  consultationDateViewHeader: {
    height: ROW_HEIGHT,
    width: ROW_DATE_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Metrics.screenDesignWidth * 5,
    //  backgroundColor: 'red',
  },

  consultationDateTextHeader: {
    ...Fonts.style.normal,

    fontSize: 14,
    // backgroundColor: 'yellow',
    width: Metrics.screenDesignWidth * 75,
    textAlign: 'center',
    textAlignVertical: 'center',
  },

  consultationDateText: {
    ...Fonts.style.normal,

    fontSize: 14,
//    color: '#6D6D6D',
  },

  consultationNumView: {
    //height: ROW_HEIGHT,
    width: ROW_QUANTITY_WIDTH,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'blue',
  },

  consultationNoteViewHeader: {
    width: ROW_NOTES_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Metrics.screenDesignWidth * 10,
    // backgroundColor: 'green',
  },

  consultationNoteView: {
    width: ROW_NOTES_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Metrics.screenDesignWidth * 10,
    // backgroundColor: 'green',
  },

  consultationNoteText: {
    ...Fonts.style.normal,

    width: ROW_NOTES_WIDTH,
    fontSize: 10,
    color: '#6D6D6D',
  },

  consultationNoteHeader: {
    ...Fonts.style.normal,

    width: ROW_NOTES_WIDTH,
    fontSize: 14,
    textAlign: 'center',
  },

  consultationData: {
    width: ROW_NOTES_WIDTH,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Metrics.screenDesignWidth * 20,
    // backgroundColor: 'green',
  },

  consultationDataItem: {
    height: Metrics.screenDesignHeight * 30,
    width: ROW_NOTES_WIDTH / 3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  consultationDataItemIcon: {
    height: 24,
    width: 18,
    resizeMode: 'stretch',
  },

  consultationDataItemText: {
    ...Fonts.style.normal,

    fontSize: 16,
    marginRight: Metrics.screenDesignWidth * 7,
    color: '#6D6D6D',
  },

  consultationDataConventionText: {
    ...Fonts.style.normal,

    fontSize: 9,
    color: '#6D6D6D',
  },

  consultationDataConventions: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: Metrics.screenDesignHeight * 4,
    width: CARD_WIDTH,
  },

  consultationDataConventionItem: {
    //backgroundColor: 'blue',
    paddingTop: Metrics.screenDesignHeight * 3,
    width: Metrics.screenDesignWidth * 90,
    justifyContent: 'center',
    alignItems: 'center',
  },

  consultationDataConventionItemExtended: {
    //backgroundColor: 'blue',
    width: Metrics.screenDesignWidth * 120,
    justifyContent: 'center',
    alignItems: 'center',
  },

  title: {
    ...Fonts.style.normal,

    color: Colors.subtitle,
    fontSize: Fonts.size.h6,
    marginVertical: Metrics.marginVerticalSmall,
  },

  subtitle: {
    ...Fonts.style.normal,

    color: Colors.textDark,
    fontSize: Fonts.size.h7,
    marginVertical: Metrics.marginVerticalSmall,
  },

  input: {
    ...Fonts.style.normal,

    height: Metrics.navBarHeight * 1.5,
    marginBottom: Metrics.marginVerticalSmall,
    width: Metrics.screenWidth * (2 / 3),
    color: Colors.textDark,
    backgroundColor: '#FFF',
    fontSize: Fonts.size.h6,
    borderRadius: Metrics.inputRadius,
    paddingLeft: Metrics.marginHorizontal,
  },

  scrollView: {
    marginTop: Metrics.screenDesignHeight * 10,
    // backgroundColor: 'green',
  },

  button: {
    //marginTop: Metrics.marginVertical,
    width: Metrics.screenWidth / 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonContainer: {
    width: CARD_WIDTH,
    alignItems: 'center',
    //marginTop: BUTTON_MARGIN_TOP,
    marginBottom: Metrics.screenDesignHeight * 32,
    // backgroundColor: 'red',
  },

  buttonText: {
    ...Fonts.style.h6,
    textAlign: 'center',
    color: '#FFF',
  },

  labelValue: {
    marginTop: Metrics.screenDesignHeight * 9,
  },

  consultationBody: {
    //backgroundColor: 'red',
    height: CONSULTATION_BODY_HEIGHT,
  },

  consultationDataBody: {
    //backgroundColor: 'blue',
    width: CARD_WIDTH,
    height: Metrics.screenDesignHeight * 380,
  },


  consultationGeneralInfo: {
    // backgroundColor: 'red',
    height: CONSULTATION_BODY_HEIGHT,
  },

  gender: {
    ...Fonts.style.normal,

    fontSize: 20,
    color: 'black',
    width: Metrics.screenWidth - Metrics.marginHorizontalLarge,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: Metrics.marginVertical,
  },

  phototype: {
    ...Fonts.style.normal,

    fontSize: 20,
    color: 'black',
    width: Metrics.screenWidth - Metrics.marginHorizontalLarge,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: Metrics.marginVertical,
  },

  checkboxView: {
    width: Metrics.screenWidth,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  checkbox: {
    marginRight: Metrics.marginHorizontalLarge,
  },

  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
    height: CARD_ITEM_HEIGHT,
    width: CARD_ITEM_WIDTH,
    //backgroundColor: 'blue',
  },

  cardImageContainer: {
    height: CARD_ITEM_HEIGHT,
    flexDirection: 'column',
    justifyContent: 'center',
  },


  cardImage: {
    resizeMode: 'stretch',
    marginHorizontal: Metrics.marginHorizontal,
    height: CARD_IMAGE_HEIGHT,
    width: CARD_IMAGE_WIDTH,
  },

  cardLabel: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '100%',
    height: CARD_ITEM_HEIGHT,
    //backgroundColor: 'red',
  },

  cardLabelTitle: {
    ...Fonts.style.cardLabelTitle,
    color: Colors.app,
    width: CARD_LABEL_TITLE_WIDTH,
    height: CARD_LABEL_TITLE_HEIGHT,
    fontSize: 15,
  },

  cardLabelDescription: {
    ...Fonts.style.cardLabelText,
    width: CARD_ITEM_WIDTH - CARD_IMAGE_WIDTH - 20,
    height:
      CARD_ITEM_HEIGHT -
      CARD_BUTTON_FOOTER_HEIGHT -
      CARD_LABEL_TITLE_HEIGHT -
      10,
    //height: CARD_LESION_DESC_HEIGHT,
    fontSize: 12,
    lineHeight: 11,
    paddingTop: 5,
    //backgroundColor: 'red',
  },

  cardLabelFooter: {
    ...Fonts.style.cardLabelText,
    width: CARD_LABEL_TITLE_WIDTH,
    height: CARD_FOOTER_HEIGHT,
    fontSize: 10,
  },

  cardButtomFooterView: {
    ...Fonts.style.cardLabelText,
    width: CARD_LABEL_TITLE_WIDTH,
    height: CARD_BUTTON_FOOTER_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  updateButton: {
    height: BUTTON_UPDATE_HEGHT,
    width: BUTTON_UPDATE_WIDTH,
    marginRight: BUTTON_UPDATE_MARGIN_RIGHT,
    borderColor: '#DADADA',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  updateButtonText: {
    ...Fonts.style.normal,

    color: Colors.app,
    fontSize: 12,
  },

  cardBodyTitlePatientData: {
    ...Fonts.style.normalBold,

    width: CARD_ITEM_WIDTH,
    // marginVertical: CARD_CONSULTATION_TITLE_MARGIN_VERTICAL,

    color: 'black',
    fontSize: 16,
    textAlignVertical: 'center',
    textAlign: 'center',
    marginBottom: 5,
    //fontWeight: 'bold',
  },



  cardBodyTitleConsultation: {
    ...Fonts.style.normal,

    height: CARD_CONSULTATION_TITLE_HEIGHT,
    width: CARD_ITEM_WIDTH,
    marginVertical: CARD_CONSULTATION_TITLE_MARGIN_VERTICAL,

    color: 'black',
    fontSize: 16,
    textAlignVertical: 'center',
  },

  lesionUpdateBody: {
    height: LESION_BODY_HEIGHT,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    // backgroundColor: 'red',
  },

  lesionFooter: {
    flexDirection: 'row',
    height: LESION_FOOTER_HEIGHT,
    justifyContent: 'space-between',
    // backgroundColor: 'blue',
  },

  lesionContainer: {
    justifyContent: 'flex-start',
    height: Metrics.screenHeight * 0.7,
    // backgroundColor: 'green',
  },

  componentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: CONTAINER_PADDING_HORIZONTAL,
    paddingVertical: CONTAINER_MARGIN_VERTICAL,
    // height: CONTAINER_HEIGHT,
    width: CONTAINER_WIDTH,
    marginVertical: CONTAINER_MARGIN_VERTICAL,

    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 2,
  },

  homeButton: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    height: Metrics.screenDesignHeight * 50,
    width: Metrics.screenWidth,
  },

});

export default styles;
