import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    alignItems: 'center',
    flexDirection: 'column',
    //justifyContent: 'center',
    backgroundColor: Colors.background,
    //backgroundColor: 'red',
    height: Metrics.screenHeight,
  },

  icon: {
    color: Colors.banner,
  },

  scrollView: {
    height: Metrics.windowDesignHeight * 400,
    paddingTop: Metrics.screenDesignHeight * 5,
    // backgroundColor: 'blue',
  },

  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,

    backgroundColor: Colors.app,
    marginTop: Metrics.screenDesignHeight * 10,
    height: Metrics.screenDesignHeight * 86,
  },

  cardImage: {
    resizeMode: 'stretch',
    marginHorizontal: Metrics.screenDesignWidth * 6,
    height: Metrics.screenDesignHeight * 72,
    width: Metrics.screenDesignHeight * 72,
    // backgroundColor: 'blue',
  },

  cardLabel: {
//    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: Metrics.screenDesignWidth * 172,
    height: Metrics.screenDesignHeight * 86,
    marginRight: Metrics.screenDesignWidth * 6,
    // backgroundColor: 'cyan',
  },

  cardLabelTitle: {
    ...Fonts.style.normal,
    color: Colors.white,
    height: Metrics.screenDesignHeight * 20,
    fontSize: 12,
    // backgroundColor: 'red',
    alignItems: 'center',
    textAlignVertical: 'center',
  },

  cardLabelDescription: {
    ...Fonts.style.normal,
    color: Colors.white,
    height: Metrics.screenDesignHeight * 50,
    fontSize: 9,
    // backgroundColor: 'black',
    alignItems: 'center',
    textAlignVertical: 'center',
  },

  cardLabelFooter: {
    ...Fonts.style.cardLabelText,
    width: '60%',
    marginVertical: Metrics.marginVerticalSmall,
  },

  image: {
    width: Metrics.screenWidth,
    height: (Metrics.bodyHeightNoHeader * 2) / 3,
    resizeMode: 'stretch',
  },

  button: {
    // marginTop: Metrics.marginVerticalLarge,
    width: Metrics.screenDesignWidth * 279,
    //width: Metrics.screenDesignWidth * 20,
    // backgroundColor: 'red',
  },

  buttonInner: {
    height: Metrics.screenDesignHeight * 35,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  cardContainer: {
    //justifyContent: 'space-between',
    //backgroundColor: 'blue',
    height: Metrics.screenDesignHeight * 350,
  },

  cardBody: {
    //marginTop: Metrics.screenDesignHeight * 10,
    marginHorizontal: Metrics.screenDesignWidth * 20,
    // backgroundColor: 'red',
  },

  cardFooterLesions: {
    ...AppStyles.screen.cardFooter,

    marginTop: Metrics.screenDesignHeight * 34,

  },

  uploadingContainer: {
    marginTop: Metrics.screenDesignWidth * 145,
    height: Metrics.screenDesignHeight * 458,
    width: Metrics.screenDesignWidth * 300,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',

    // backgroundColor: 'red'
  },

  uploadingDisclaimer: {
    ...Fonts.style.normal,

    marginTop: Metrics.screenDesignWidth * 40,
    height: Metrics.screenDesignHeight * 201,
    width: Metrics.screenDesignWidth * 256,

    color: Colors.app,
    fontSize: 18,
    textAlign: 'center',
    // backgroundColor: 'blue',
  },

  uploadingText: {
    ...Fonts.style.normal,

    marginTop: Metrics.screenDesignWidth * 20,
    height: Metrics.screenDesignHeight * 48,
    width: Metrics.screenDesignWidth * 256,

    color: '#6D6D6D',
    fontSize: 18,
    textAlign: 'center',
    // backgroundColor: 'cyan',
  },

  uploadingCardBodyContainer: {
    height: Metrics.screenDesignHeight * 458,
    width: Metrics.screenDesignWidth * 300,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',

    backgroundColor: 'red'
  },

  uploadingImageView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Metrics.screenWidth - Metrics.cardMarginHorizontal,
    height: Metrics.screenHeight * 0.7,
    paddingHorizontal: Metrics.marginHorizontalLarge,
  },


});

export default styles;
