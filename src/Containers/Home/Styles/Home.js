import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const HEADER_HEIGHT = 146 * Metrics.screenDesignHeight;
const HEADER_WIDTH = 375 * Metrics.screenDesignWidth;
const BODY_HEIGHT = 521 * Metrics.screenDesignHeight;
const BODY_WIDTH = 375 * Metrics.screenDesignWidth;

const BUTTONBAR_MARGIN_HORIZONTAL = 28 * Metrics.screenDesignWidth;
const BUTTONBAR_MARGIN_TOP = 103 * Metrics.screenDesignWidth;

const BACKGROUND_MARGIN_TOP = 146 * Metrics.screenDesignWidth;
const BACKGROUND_HEIGHT = 521 * Metrics.screenDesignWidth;

const BUTTON_NEW_CONTAINER_HEIGHT = 115 * Metrics.screenDesignWidth;
const BUTTON_NEW_HEIGHT = 42 * Metrics.screenDesignHeight;
const BUTTON_NEW_WIDTH = 250 * Metrics.screenDesignWidth;


const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },

  image: {
    // position: 'absolute',
    resizeMode: 'cover',
    width: Metrics.screenWidth * 2,
    height: Metrics.screenHeight,
  },

  buttonBar: {
    marginTop: BUTTONBAR_MARGIN_TOP,
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: HEADER_WIDTH,
  },

  buttonMarginLeft: {
    marginLeft: BUTTONBAR_MARGIN_HORIZONTAL,
  },

  buttonMarginRight: {
    marginRight: BUTTONBAR_MARGIN_HORIZONTAL,
  },

  header: {
    height: BACKGROUND_MARGIN_TOP,
    width: HEADER_WIDTH,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    // backgroundColor: 'red',

  },

  headerTitle: {
    ...Fonts.style.normal,
    paddingRight: Metrics.marginVerticalSmall,
    marginHorizontal: Metrics.marginHorizontal,
    color: 'white',
    fontSize: 18,
  },

  title: {
    ...Fonts.style.normalBold,
    color: 'white',
    fontSize: 28,
    width: Metrics.screenWidth,
    textAlign: 'center',
    //fontWeight: '700',
  },

  subtitle: {
    ...Fonts.style.normal,
    fontSize: 12,
    color: Colors.snow,
    paddingHorizontal: Metrics.marginVerticalSmall,
    marginBottom: Metrics.marginVerticalLarge,
    marginHorizontal: Metrics.marginHorizontal,
  },

  background: {
    height: BACKGROUND_HEIGHT * 2,
    backgroundColor: Colors.background,
  },

  form: {
    paddingTop: 31 * Metrics.screenDesignHeight,
  },

  buttonSearchView: {
    paddingTop: 17 * Metrics.screenDesignHeight,
  },

  errorText: {
    ...Fonts.style.normal,
    color: '#FF0000',
    textAlign: 'center',
    textAlignVertical: 'center',
    width: Metrics.screenWidth,
    fontSize: 12,
    fontWeight: 'bold',
    height: 24 * Metrics.screenDesignHeight,
  },

  buttonNewContainer: {
    width: Metrics.screenWidth,
    height: BUTTON_NEW_CONTAINER_HEIGHT,
    backgroundColor: Colors.background,
    justifyContent: 'center',
  },

  buttonNew: {
    backgroundColor: Colors.background,
  },

});

export default styles;
