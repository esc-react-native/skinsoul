import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

const CARD_IMAGE_WIDTH = 234 * Metrics.screenDesignWidth;
const CARD_IMAGE_HEIGHT = 382 * Metrics.screenDesignHeight;
const CARD_IMAGE_TOP_MARGIN = 32 * Metrics.screenDesignHeight;

const styles = StyleSheet.create({
  ...AppStyles.screen,

  container: {
    alignItems: 'center',
    flexDirection: 'column',
    //justifyContent: 'center',
    backgroundColor: Colors.background,
    height: Metrics.screenHeight,
    //    backgroundColor: 'yellow',
  },

  homeButton: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    height: Metrics.screenDesignHeight * 50,
    width: Metrics.screenWidth,
  },

  backPreview: {
    backgroundColor: 'blue',
    height: 75,
    width: 45,
  },

  cameraPreview: {
    height: '100%',
    width: '100%',
  },

  photo: {
    resizeMode: 'contain',
    marginTop: Metrics.marginVerticalLarge,
  },

  image: {
    width: Metrics.screenWidth,
    height: (Metrics.bodyHeightNoHeader * 2) / 3,
    resizeMode: 'stretch',
  },

  cardBodyX: {
    marginBottom: 0,
  },

  cardBodyImage: {
    marginTop: 0,
    marginVertical: 0,
    marginHorizontal: 0,
    marginRight: 0,
    paddingLeft: 0,
    justifyContent: 'center',
    alignItems: 'center',

    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#fff',

    height: Metrics.windowDesignHeight * 482,

  },

  cardImage: {
    //resizeMode: 'stretch',
    //    marginHorizontal: Metrics.marginHorizontal,
    height: Metrics.windowDesignHeight * 382,
    width: Metrics.screenDesignWidth * 254,
    marginTop: CARD_IMAGE_TOP_MARGIN,

    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#FFF',
  },

  footerButtonLarge: {
    width: Metrics.screenDesignWidth * 150,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Metrics.screenDesignWidth * 60,

    height: 30 * Metrics.screenDesignHeight,
  },

  cardFooterReview: {
    marginBottom: Metrics.screenDesignHeight * 10,
  },
});

export default styles;
