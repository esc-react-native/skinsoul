import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Button,
  Alert,
} from 'react-native';

import {Images} from '../../Styles';

import {StackActions, NavigationActions} from 'react-navigation';

import Icon from 'react-native-vector-icons/FontAwesome';

// Styles
import Styles from './Styles/WizardLesion';

//Multilanguage
import i18 from '../../Localize/I18n';

//Camera Manager
import CameraKit from '../../Components/Camera';

import {Switchbox, Dropdown, Textarea} from '../../Components/Base';
import ButtonFooter from '../../Components/ButtonFooter';
import ProgressCircles from '../../Components/ProgressCircles';

import ImageSample from '../../Assets/Images/consultant.png';

import CardPatientNew from './Cards/CardPatientNew';
import CardLesion from './Cards/CardLesion';
import BottomHomeButton from '../../Components/BottomHomeButton';
import ModalExitPatient from '../../Components/ModalExitPatient';
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import ModalButtonHome from '../../Components/ModalButtonHome';

const STEP_INTRO = 0;
const STEP_CAPTURE = 2;
const STEP_PREVIEW = 3;
const STEP_LESION = 4;

export default class WizardLesion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,

      photo: null,

      patient:
        this.props.patient === undefined || this.props.patient === null
          ? null
          : this.props.patient,
      step: this.props.step,

      isNewConsultation: this.props.isNewConsultation,

      modalVisible: false,
    };

    this.handleNext = this.handleNext.bind();
    this.handleCamera = this.handleCamera.bind();
    this.handlePatientNewSubmit = this.handlePatientNewSubmit.bind();
  }

  handleCamera = async capturedPhoto => {
    const photo = capturedPhoto;

    await this.setState({photo, step: STEP_PREVIEW});

    this.handleNext();
  };

  handleNext = () => {
    this.setState({step: this.state.step + 1});
  };

  handleBack = () => {
    if (this.state.step === 2) {
      if (this.props.isNewConsultation) {
        this.setState({step: this.state.step - 1});
      }

      if (this.props.isCreatePatient) {
        this.setState({step: 0});
      }
    } else {
      this.setState({step: this.state.step - 1});
    }
  };

  handleSubmit = lesionX => {
    const lesion = {
      photo: this.state.photo,
      bleedingLession: lesionX.bleedingLession,
      sendForBiopsy: lesionX.sendForBiopsy,
      location: lesionX.location,
      diagnosis: lesionX.diagnosis,
      notes:
        lesionX.notes === undefined ||
        lesionX.notes === null ||
        lesionX.notes.length === 0
          ? '...'
          : lesionX.notes,
    };

    this.setState({step: 0});
    this.props.onSubmit(lesion, this.state.patient);
  };

  handleCancel = () => {
    this.props.onSubmit(null);
  };

  handlePatientNewSubmit = patient => {
    this.setState({patient: patient});

    //this.handleNext();
    this.setState({step: 2});
  };

  handlePatientEditSubmit = patient => {
    this.setState({patient: patient});

    //this.handleNext();
    this.setState({step: 2});
  };

  renderStep0_NewPatient = () => {
    return (
      <CardPatientNew
        onBack={this.props.handleBack}
        patientNumber={this.props.patientNumber}
        patient={this.state.patient}
        onSubmit={this.handlePatientNewSubmit}
      />
    );
  };

  renderStep1_EditPatient = () => {
    console.log('renderStep1_EditPatient');
    console.log(this.state.patient);

    return (
      <CardPatientNew
        editable
        onBack={this.props.handleBack}
        patientNumber={this.props.patientNumber}
        patient={this.state.patient}
        onSubmit={this.handlePatientEditSubmit}
      />
    );
  };

  renderStep2_Intro = () => {
    console.log('renderStep2_Intro');

    return (
      <View style={Styles.card}>
        <View style={Styles.cardTitle}>
          <Text style={Styles.cardTitleText}>{i18.t('photo-acquisition')}</Text>
        </View>
        <View style={Styles.cardContainer}>
          <View style={Styles.cardBody}>
            <Text style={Styles.cardBodyTextNormal}>
              {i18.t('photo-instructions')}
            </Text>
          </View>
          <View style={Styles.cardFooter}>
            <ButtonFooter back text={i18.t('back')} onPress={this.handleBack} />
            <ProgressCircles step={1} />
            <ButtonFooter icon="camera" onPress={this.handleNext} />
          </View>
        </View>
      </View>
    );
  };

  renderStep3_Photo = () => {
    return (
      <View style={Styles.camera}>
        <CameraKit onSubmit={this.handleCamera} onCancel={this.handleBack} />
      </View>
    );
  };

  renderStep4_Preview = () => {
    const imagen = this.state.photo;

    return (
      <View style={Styles.card}>
        <View style={[Styles.cardContainer, Styles.cardNoTitleBar]}>
          <View style={[Styles.cardBody, Styles.cardBodyImage]}>
            {imagen !== null && (
              <Image
                source={{uri: imagen}}
                style={[
                  // Styles.cardContainer,
                  // Styles.cardNoTitleBar,
                  Styles.cardImage,
                ]}
              />
            )}
          </View>
          <View style={[Styles.cardFooter, Styles.cardFooterReview]}>
            <ButtonFooter
              back
              text={i18.t('retake-photo').toUpperCase()}
              onPress={this.handleBack}
              styleButton={Styles.footerButtonLarge}
            />
            <ButtonFooter
              next
              text={i18.t('next').toUpperCase()}
              onPress={this.handleNext}
            />
          </View>
        </View>
      </View>
    );
  };

  renderStep5_Lesion = () => {
    return <CardLesion onSubmit={this.handleSubmit} onBack={this.handleBack} />;
  };

  handleExit = () => {
    this.setState({modalVisible: true});
  };

  handleModalExit = () => {
    this.setState({modalVisible: false});
  };

  handleModalOK = () => {
    console.log('wizardlesion -> handleModalOK');
    this.setState({modalVisible: false});

    this.props.onExitHome();
  };

  render() {
    HideNavigationBar();

    return (
      <View style={Styles.container}>
        <View>
          {this.state.step === 0 && this.renderStep0_NewPatient()}
          {this.state.step === 1 && this.renderStep1_EditPatient()}
          {this.state.step === 2 && this.renderStep2_Intro()}
          {this.state.step === 3 && this.renderStep3_Photo()}
          {this.state.step === 4 && this.renderStep4_Preview()}
          {this.state.step === 5 && this.renderStep5_Lesion()}
        </View>
        {this.state.step === 0 && (
          <View style={Styles.homeButton}>
            <BottomHomeButton onPress={this.handleExit} />
          </View>
        )}
        {this.state.step === 0 && (
          <ModalExitPatient
            visible={this.state.modalVisible}
            onPressOK={this.handleModalOK}
            onPressCancel={this.handleModalExit}
          />
        )}
        {this.state.step === 1 && (
          <View style={Styles.homeButton}>
            <BottomHomeButton onPress={this.handleExit} />
          </View>
        )}
        {this.state.step === 1 && (
          <ModalButtonHome
            visible={this.state.modalVisible}
            onPressOK={this.handleModalOK}
            onPressCancel={this.handleModalExit}
            icon={"trash"}
            question={i18.t('leave-new-consultation') + '?'}
            disclaimer={i18.t('any-changes-consultation-discarded')}
          />
        )}
      </View>
    );
  }
}
