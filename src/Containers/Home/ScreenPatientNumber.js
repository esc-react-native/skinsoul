import React, {Component} from 'react';
import {View, Text, ImageBackground, Platform} from 'react-native';
import {Images} from '../../Styles';
import Subtitle from '../../Components/Subtitle';
import {API} from 'aws-amplify';
import {StackActions, NavigationActions} from 'react-navigation';

import Button from '../../Components/Button';
//import InputText from '../../Components/InputText';
import InputText from '../../Components/InputTextPlaceholder';
import {HideNavigationBar} from 'react-native-navigation-bar-color';

// Styles
import Styles from './Styles/Home';

//Multilanguage
import i18 from '../../Localize/I18n';

export default class ScreenPatientNumber extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,

      button: false,
      isNewPatient: false,
      patientNumber: null,

      lastNumber: null,

      isButtonDisabled: true,
      ErrorMessage: null,
    };

    this.handleOption = this.handleOption.bind();
    this.handleErrorMessage = this.handleErrorMessage.bind();
  }

  //.
  componentDidMount = () => {
    HideNavigationBar();
  };

  handleErrorMessage = errorMessage => {
    this.setState({ErrorMessage: errorMessage});
  };

  handleFocus = () => {
    this.setState({ErrorMessage: null});
  };

  //.
  handleValidate = () => {
    this.setState({isButtonDisabled: !(this.state.patientNumber.length > 0)});
  };

  //.
  apiGetPatient() {
    // console.log(`/patients/${this.state.patientNumber}`);
    return API.get('patients', `/patients/${this.state.patientNumber}`);
  }

  //.
  getPatient = async () => {
    try {
      let xPatient = await this.apiGetPatient();
      let patient = {
        ...xPatient,
        patientNumber: xPatient.id,
      };
      console.log('Patient GET: ' + this.state.patientNumber);
      // console.log(patient);
      console.log(JSON.stringify(patient));

      this.setState({patient, founded: true}, () => {
        return true;
      });

      return true;
    } catch (_err) {
      // console.log('Patient GET ERROR: ' + this.state.patientNumber);
      // console.log(JSON.stringify(_err));
      this.setState({patient: null, founded: false});
      return false;
    }
  };

  //.
  handleNew = () => {
    let option;
    option = this.props.navigation.getParam('SearchType', 'NO-ID');

    this.props.navigation.navigate(
      'Consultation',
      {
        PatientNumber: this.state.patientNumber,
        Patient: this.state.patient,
        SearchType: option,
        isNewPatient: true,
      },
      NavigationActions.navigate({
        routeName: 'PhotoInfo',
        params: {
          PatientNumber: this.state.patientNumber,
          Patient: this.state.patient,
          SearchType: option,
          isNewPatient: true,
        },
      }),
    );

    // this.props.navigation.navigate('PhotoInfo', {
    //   PatientNumber: this.state.patientNumber,
    //   Patient: this.state.patient,
    //   SearchType: option,
    //   isNewPatient: true,
    // });
  };

  //.
  handleOption = async () => {
    let option;

    this.setState({
      isProcessing: true,
      lastNumber: this.setState.patientNumber,
      isNewPatient: false,
    });

    option = this.props.navigation.getParam('SearchType', 'NO-ID');

    const existPatient = await this.getPatient();

    if (existPatient) {
      this.setState({isProcessing: false});

      this.props.navigation.navigate('Consultation', {
        PatientNumber: this.state.patientNumber,
        Patient: this.state.patient,
        SearchType: option,
      });
    } else {
      this.handleErrorMessage(i18.t('patient-not-exist'));

      this.setState({isProcessing: false, isNewPatient: true});
    }
  };

  //.
  handleChangeText = (type, text) => {
    if (
      this.state.lastNumber !== null &&
      this.state.isNewPatient &&
      this.state.lastNumber !== this.state.patientNumber
    ) {
      this.setState({
        lastNumber: null,
        isNewPatient: false,
        ErrorMessage: null,
      });
    }

    const newText = text.replace(/[^a-zA-Z0-9]/g, '').toUpperCase();

    this.setState({[type]: newText}, () => this.handleValidate());
  };

  //.
  //.
  render() {
    let SearchType = this.props.navigation.getParam('SearchType', 'NO-ID');

    return (
      <View style={[Styles.container]}>
        <View>
          <ImageBackground
            source={Images.loginBackground}
            style={Styles.image}
            blurRadius={Platform.OS === 'ios' ? 10 : 2}>
            <View style={Styles.header}>
              <Text style={Styles.headerTitle}>SKIN</Text>
              <Text style={Styles.headerTitle}>SOUL</Text>
              <Subtitle
                text={i18.t('data-gathering-version')}
                style={Styles.subtitle}
              />
              <Text style={Styles.title}>
                {SearchType === 'consultation'
                  ? i18.t('patients-consultations')
                  : i18.t('add-biopsy-results')}
              </Text>
            </View>
            <View style={Styles.background}>
              <View style={Styles.form}>
                <InputText
                  transparent
                  //label={i18.t('patient-id')}
                  keyboardType={
                    Platform.OS === 'android' ? 'visible-password' : 'default'
                  }
                  autoCapitalize="none"
                  spellCheck={false}
                  autoCorrect={false}
                  //keyboardType="default"
                  placeholder={i18.t('patient-id')}
                  placeholderTextColor={'#6D6D6D'}
                  value={this.state.patientNumber}
                  onChangeText={text =>
                    this.handleChangeText('patientNumber', text)
                  }
                  onFocus={this.handleFocus}
                />
                <View style={Styles.buttonSearchView}>
                  <Button
                    style={Styles.button}
                    text={i18.t('search')}
                    textLoading={i18.t('loading') + '...'}
                    onPress={this.handleOption}
                    disabled={this.state.isButtonDisabled}
                    isLoading={this.state.isProcessing}
                  />
                </View>
                <Text style={Styles.errorText}>{this.state.ErrorMessage}</Text>
              </View>
              {SearchType === 'consultation' && (
                <View style={Styles.buttonNewContainer}>
                  {this.state.isNewPatient && (
                    <Button
                      inverted
                      styleButton={Styles.buttonNew}
                      text={i18.t('add-new-patient')}
                      textLoading={i18.t('loading') + '...'}
                      onPress={this.handleNew}
                      disabled={this.state.isButtonDisabled}
                      isLoading={this.state.isProcessing}
                    />
                  )}
                </View>
              )}
            </View>
          </ImageBackground>
        </View>
      </View>
    );
  }
}
