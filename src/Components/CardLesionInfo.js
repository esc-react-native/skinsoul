import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import Styles from './Styles/CardLesionInfo';
import CardTitle from '../Components/CardTitle';
import LesionValue from '../Components/LesionValue';
import {DropdownModal, DropdownMaterial} from '../Components/Base';
import ButtonFooter from '../Components/ButtonFooter';
import Moment from 'moment';

import {Images} from '../Styles';

import i18 from '../Localize/I18n';

export default class CardLesionInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      patientNumber: this.props.patientNumber,
      consultation: this.props.consultation,
      lesion: this.props.lesion,
      lesionNumber: this.props.lesionNumber,

      isPhoto: false,

      diagnosisOptions: [
        {
          id: 'merkel-cell-carcinoma',
          label: i18.t('merkel-cell-carcinoma'),
          value: 'merkel-cell-carcinoma',
        },
        {
          id: 'malignant-melanoma-amelanotic',
          label: i18.t('malignant-melanoma-amelanotic'),
          value: 'malignant-melanoma-amelanotic',
        },
        {
          id: 'magliant-melanoma-pigmented',
          label: i18.t('magliant-melanoma-pigmented'),
          value: 'magliant-melanoma-pigmented',
        },
        {
          id: 'malignant-cutaneous-lymphoma-tcell',
          label: i18.t('malignant-cutaneous-lymphoma-tcell'),
          value: 'malignant-cutaneous-lymphoma-tcell',
        },
        {
          id: 'malignant-cutaneous-lymphome-bcell',
          label: i18.t('malignant-cutaneous-lymphome-bcell'),
          value: 'malignant-cutaneous-lymphome-bcell',
        },
        {
          id: 'malignant-epidermal-basal-cell-carcinoma',
          label: i18.t('malignant-epidermal-basal-cell-carcinoma'),
          value: 'malignant-epidermal-basal-cell-carcinoma',
        },
        {
          id: 'malignant-epidermal-basal-squamous',
          label: i18.t('malignant-epidermal-basal-squamous'),
          value: 'malignant-epidermal-basal-squamous',
        },
        {
          id: 'malignant-dermal-angiosarcoma',
          label: i18.t('malignant-dermal-angiosarcoma'),
          value: 'malignant-dermal-angiosarcoma',
        },
        {
          id: 'malignant-dermal-merkel',
          label: i18.t('malignant-dermal-merkel'),
          value: 'malignant-dermal-merkel',
        },
        {
          id: 'bening-dermal-cyst',
          label: i18.t('bening-dermal-cyst'),
          value: 'bening-dermal-cyst',
        },
        {
          id: 'bening-dermal-fibroma',
          label: i18.t('bening-dermal-fibroma'),
          value: 'bening-dermal-fibroma',
        },
        {
          id: 'bening-dermal-lipoma',
          label: i18.t('bening-dermal-lipoma'),
          value: 'bening-dermal-lipoma',
        },
        {
          id: 'bening-melanoctyc-blue',
          label: i18.t('bening-melanoctyc-blue'),
          value: 'bening-melanoctyc-blue',
        },
        {
          id: 'bening-melanoctyc-solar',
          label: i18.t('bening-melanoctyc-solar'),
          value: 'bening-melanoctyc-solar',
        },
        {
          id: 'bening-melanoctyc-café',
          label: i18.t('bening-melanoctyc-café'),
          value: 'bening-melanoctyc-café',
        },
        {
          id: 'bening-epidermal-milia',
          label: i18.t('bening-epidermal-milia'),
          value: 'bening-epidermal-milia',
        },
        {
          id: 'bening-epidermal-seborrheic',
          label: i18.t('bening-epidermal-seborrheic'),
          value: 'bening-epidermal-seborrheic',
        },
        {
          id: 'neoplastic-genodermatosis-congenital',
          label: i18.t('neoplastic-genodermatosis-congenital'),
          value: 'neoplastic-genodermatosis-congenital',
        },
        {
          id: 'neoplastic-genodermatosis-tuberous',
          label: i18.t('neoplastic-genodermatosis-tuberous'),
          value: 'neoplastic-genodermatosis-tuberous',
        },
        {
          id: 'neoplastic-inflamatory-psoriasis',
          label: i18.t('neoplastic-inflamatory-psoriasis'),
          value: 'neoplastic-inflamatory-psoriasis',
        },
        {
          id: 'neoplastic-inflamatory-stevens',
          label: i18.t('neoplastic-inflamatory-stevens'),
          value: 'neoplastic-inflamatory-stevens',
        },
        {
          id: 'neoplastic-inflamatory-rosacea',
          label: i18.t('neoplastic-inflamatory-rosacea'),
          value: 'neoplastic-inflamatory-rosacea',
        },
        {
          id: 'neoplastic-inflamatory-acne',
          label: i18.t('neoplastic-inflamatory-acne'),
          value: 'neoplastic-inflamatory-acne',
        },
        {
          id: 'neoplastic-inflamatory-abrasion',
          label: i18.t('neoplastic-inflamatory-abrasion'),
          value: 'neoplastic-inflamatory-abrasion',
        },
        {
          id: 'neoplastic-blistering-vulgaris',
          label: i18.t('neoplastic-blistering-vulgaris'),
          value: 'neoplastic-blistering-vulgaris',
        },
        {
          id: 'neoplastic-blistering-bullous',
          label: i18.t('neoplastic-blistering-bullous'),
          value: 'neoplastic-blistering-bullous',
        },
        {
          id: 'neoplastic-blistering-foliaceus',
          label: i18.t('neoplastic-blistering-foliaceus'),
          value: 'neoplastic-blistering-foliaceus',
        },
        {
          id: 'subungueal-melanoma',
          label: i18.t('subungueal-melanoma'),
          value: 'subungueal-melanoma',
        },
        {
          id: 'melanonychia',
          label: i18.t('melanonychia'),
          value: 'melanonychia',
        },
      ],

      biopsyDiagnosis: this.props.lesion.biopsyDiagnosis
        ? this.props.lesion.biopsyDiagnosis
        : null,
      lesionUpdateButton: false,
      isUpdating: false,
    };
  }

  handleBack = () => {
    this.props.onBack();
  };

  //.
  handleLesionPhoto = () => {
    this.setState({isPhoto: true});
  };

  //.
  handleLesionPhotoBack = () => {
    this.setState({isPhoto: false});
  };

  //.
  handleChangeValue = async (type, text) => {
    await this.setState({[type]: text}, () => {
      this.handleValidate();
    });
  };

  //.
  handleValidate = () => {
    if (this.state.lesion.sendForBiopsy && this.props.biopsyResultsEditable) {
      if (
        this.state.biopsyDiagnosis !== null &&
        this.state.biopsyDiagnosis.length > 0
      ) {
        this.setState({lesionUpdateButton: true});
      }
    }

    return true;
  };

  //.
  handleUpdate = () => {
    this.setState({lesionUpdateButton: false}, () =>
      this.props.onLesionUpdate(this.state.lesion, this.state.biopsyDiagnosis),
    );
  };

  renderInfo = () => {
    console.log('this.state.biopsyDiagnosis');
    console.log(this.state.biopsyDiagnosis);

    return (
      <View style={Styles.card}>
        <CardTitle
          styleSubtitle={Styles.cardTitleSubtitle}
          iconName={'list-alt'}
          title={this.state.patientNumber}
          subtitle={`${i18.t('consultation')} ${Moment(
            this.state.consultation.date,
          ).format('YYYY-MM-DD')} ${i18.t('lesion')}# ${(
            this.state.lesionNumber + 1
          ).toString()}`}
        />
        <View style={Styles.lesionContainer}>
          <View style={Styles.lesionUpdateBody}>
            <LesionValue label={i18.t('age')} text={this.props.age} />
            <LesionValue
              label={i18.t('immunocompromised')}
              text={
                this.props.immuno
                  ? i18.t('yes').toUpperCase()
                  : i18.t('no').toUpperCase()
              }
            />
            <LesionValue
              label={i18.t('bleeding-lesion')}
              text={
                this.state.lesion.bleedingLession
                  ? i18.t('yes').toUpperCase()
                  : i18.t('no').toUpperCase()
              }
            />
            <LesionValue
              label={i18.t('lesion-sent-biopsy')}
              text={
                this.state.lesion.sendForBiopsy
                  ? i18.t('yes').toUpperCase()
                  : i18.t('no').toUpperCase()
              }
            />
            <LesionValue
              label={i18.t('location')}
              text={i18.t(this.state.lesion.location).toUpperCase()}
            />
            <LesionValue
              secondLine
              label={i18.t('preliminary-diagnosis')}
              text={i18.t(this.state.lesion.diagnosis).toUpperCase()}
            />
            {this.state.lesion.sendForBiopsy &&
              this.props.biopsyResultsEditable && (
                <View style={Styles.biopsyDiagnosisView}>
                  <View style={Styles.biopsyDiagnosisViewIn}>
                    <Text style={Styles.biopsyDiagnosisTextDropdown}>{`${i18.t(
                      'biopsy',
                    )} ${i18.t('diagnosis')}`}</Text>
                    <DropdownModal
                      autoCapitalize={
                        this.state.biopsyDiagnosis &&
                        this.state.biopsyDiagnosis !== null
                          ? true
                          : false
                      }
                      styleContainer={Styles.componentContainer}
                      stylePlaceholder={Styles.dropdownBiopsyPlaceholder}
                      styleView={Styles.dropdownBiopsyBox}
                      placeholder={
                        this.state.biopsyDiagnosis &&
                        this.state.biopsyDiagnosis !== null
                          ? i18.t(this.state.biopsyDiagnosis).toUpperCase()
                          : i18.t('pending-biopsy-results')
                      }
                      value={this.state.biopsyDiagnosis}
                      options={this.state.diagnosisOptions}
                      onValueChange={value => {
                        this.handleChangeValue('biopsyDiagnosis', value);
                      }}
                    />
                  </View>
                </View>
              )}
            {this.state.lesion.sendForBiopsy &&
              !this.props.biopsyResultsEditable && (
                <View>
                  <LesionValue
                    secondLine
                    label={i18.t('biopsy-diagnosis')}
                    text={
                      this.state.lesion.biopsyDiagnosis
                        ? i18.t(this.state.lesion.biopsyDiagnosis).toUpperCase()
                        : i18.t('pending-biopsy-results')
                    }
                  />
                </View>
              )}
            <LesionValue
              textArea
              label={i18.t('notes')}
              text={this.state.lesion.notes}
            />
          </View>
          <View style={Styles.lesionFooter}>
            <ButtonFooter
              back
              text={i18.t('back').toUpperCase()}
              onPress={this.props.onBack}
            />
            <ButtonFooter
              text={
                this.props.isBiopsy
                  ? this.state.lesion.sendForBiopsy
                    ? i18.t('update').toUpperCase()
                    : ''
                  : i18.t('see-lesion').toUpperCase()
              }
              onPress={
                this.props.isBiopsy
                  ? this.state.lesion.sendForBiopsy
                    ? this.handleUpdate
                    : this.handleBackLesion
                  : this.handleLesionPhoto
              }
            />
          </View>
        </View>
      </View>
    );
  };

  renderPhoto = () => {
    console.log('this.state.lesion.photoURL');
    console.log(this.state.lesion.photoURL);

    return (
      <View style={Styles.card}>
        <View style={Styles.lesionContainer}>
          <View style={[Styles.lesionUpdateBody, Styles.lesionPhotoBody]}>
            <View style={[Styles.cardImageContainer]}>
              <Image
                source={{uri: this.state.lesion.photoURL}}
                // source={Images.icon_biopsy}
                style={[
                  this.state.isLoading ? Styles.loadingImage : Styles.cardImage,
                ]}
                onLoadEnd={e => this.setState({isLoading: false})}
              />
              {(this.state.isLoading ||
                typeof this.state.lesion.photoURL !== 'string') && (
                <Text style={[Styles.loadingText]}>Loading...</Text>
              )}
            </View>
          </View>
          <View style={Styles.lesionFooter}>
            <ButtonFooter
              back
              text={i18.t('back').toUpperCase()}
              onPress={this.handleLesionPhotoBack}
            />
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View>
        {!this.state.isPhoto && this.renderInfo()}
        {this.state.isPhoto && this.renderPhoto()}
      </View>
    );
  }
}
