import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert} from 'react-native';
import Styles from './Styles/ModalNotification';
import {Colors} from '../Styles/index';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//Multilanguage
import i18 from '../Localize/I18n';

export default class ModalNotification extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: this.props.visible,
    };
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.visible}>
          <View style={Styles.container}>
            <View style={Styles.modal}>
              <Icon name={this.props.icon} color={Colors.app} size={80} />
              <Text style={Styles.message}>{this.props.message}</Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
