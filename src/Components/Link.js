import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import Styles from './Styles/Link';

class Link extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  render() {
    return (
      <View style={Styles.container}>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={[
            Styles.button,
            this.props.disabled || this.props.isLoading
              ? Styles.disabled
              : Styles.enabled,
            this.props.styleButton,
          ]}
          disabled={this.props.disabled || this.props.isLoading}>
          <Text style={Styles.text}>{this.props.text}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Link;
