import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import Styles from './Styles/CardLesionItem';

import i18 from '../Localize/I18n';
import {s3Upload, s3Get} from '../libs/awsLib';

import {Images} from '../Styles';

export default class CardLesionItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      item: this.props.item,
      photo: this.props.s3Load ? null : this.props.photo,
    };

    this.getImage = this.getImage.bind();
    this.handleSelect = this.handleSelect.bind();

    this.getImage();
  }

  getImage = async () => {
    console.log('this.props.photo');
    console.log(this.props.photo);
    console.log(this.props.s3Load);

    if (this.props.s3Load) {
      this.setState({photo: await s3Get(this.props.photo)});
    }
  };

  handleSelect = () => {
    let item = {
      ...this.props.item,

      photoURL: this.state.photo,
    };

    this.props.onSelect(item, this.props.index);
  };

  render() {
    return (
      <View key={this.props.index} style={Styles.cardItem}>
        <TouchableOpacity
          onPress={this.handleSelect}
          style={[Styles.cardItemTouch]}>
          {this.props.s3Load && (
            <View>
              <Image
                source={{uri: this.state.photo}}
                style={[
                  this.state.isLoading ? Styles.loadingImage : Styles.cardImage,
                ]}
                onLoadEnd={e => this.setState({isLoading: false})}
              />
              {(this.state.isLoading ||
                typeof this.state.photo !== 'string') && (
                <Text style={[Styles.loadingText]}>Loading...</Text>
              )}
            </View>
          )}
          {!this.props.s3Load && (
            <Image
              source={{uri: this.props.photo}}
              style={[Styles.cardImage]}
            />
          )}
          <View style={Styles.cardLabel}>
            <Text style={Styles.cardLabelTitle}>{`${i18.t(
              'lesion',
            )} #${parseInt(this.props.index) + 1}`}</Text>
            <Text style={Styles.cardLabelDescription}>{`${
              this.props.item.bleedingLession
                ? i18.t('bleeding')
                : i18.t('not-bleeding')
            } ${i18.t('card-disclaimer-1')}${i18.t(
              this.props.item.location,
              'card-disclaimer-2',
            )}\n${i18.t(this.props.item.diagnosis)}. \n${
              this.props.item.sendForBiopsy
                ? this.props.item.biopsyDiagnosis
                  ? `${i18.t('biopsy-diagnosis')} ${i18.t(
                      this.props.item.biopsyDiagnosis,
                    )}`
                  : i18.t('lesion-sent-biopsy')
                : i18.t('lesion-not-sent-biopsy')
            }`}</Text>
            {this.props.isBiopsy && this.props.item.sendForBiopsy && (
              <View style={[Styles.cardItemFooter]}>
                <TouchableOpacity
                  onPress={this.handleSelect}
                  style={[Styles.cardItemButton]}>
                  <Text style={Styles.cardItemButtonText}>
                    {i18.t('update')}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
