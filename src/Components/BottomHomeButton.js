/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Styles from './Styles/BottomHomeButton';
import {Images} from '../Styles';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
//Multilanguage
import i18 from '../Localize/I18n';

//export default class Login extends Component {

const BottomHomeButton = props => {
  return (
    <TouchableOpacity style={Styles.container} onPress={props.onPress}>
      <Icon name="home" color={'#6D6D6D'} size={18} />
      <Text style={Styles.text}>{i18.t('home')}</Text>
    </TouchableOpacity>
  );
};

export default BottomHomeButton;
