/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import Styles from './Styles/ButtonFooter';
import {Images, Colors} from '../Styles';

import Icon from 'react-native-vector-icons/FontAwesome';
import IconSimple from 'react-native-vector-icons/Feather';

//import {Container, Button} from 'native-base';
import {Button} from './Base';

const ButtonFooter = props => {
  return (
    <View style={[Styles.container]}>
      <TouchableOpacity
        style={[Styles.button, props.styleButton]}
        onPress={props.onPress}
        {...props}>
        {props.back && <Text style={[Styles.iconText]}>{'<'}</Text>}
        <Text style={[Styles.text, props.styleText]}>{props.text}</Text>
        {props.icon && (
          <View style={[Styles.iconView]}>
            <IconSimple name={props.icon} color={Colors.app} size={26} />
          </View>
        )}
        {props.next && <Text style={[Styles.iconTextFwd]}>{'>'}</Text>}
      </TouchableOpacity>
    </View>
  );
};

export default ButtonFooter;
