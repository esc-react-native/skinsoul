/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text} from 'react-native';
import Styles from './Styles/LesionValue';
import {ScrollView} from 'react-native-gesture-handler';

const LesionValue = props => {
  return (
    <View
      style={[
        Styles.container,
        props.secondLine ? Styles.secondLine : Styles.none,
        props.textArea ? Styles.textAreaContainer : Styles.none,
        props.style,
      ]}>
      <Text style={[Styles.label, props.styleLabel]}>{props.label}</Text>
      {props.textArea && (
        <ScrollView
          contentContainerStyle={[
            Styles.container,
            Styles.textAreaContainer,
            Styles.ScrollView,
          ]}>
          <Text
            style={[
              Styles.text,
              props.secondLine ? Styles.secondLineText : Styles.none,
              props.textArea ? Styles.textAreaText : Styles.none,
              props.styleText,
            ]}>
            {props.text}
          </Text>
        </ScrollView>
      )}
      {!props.textArea && (
        <View>
          <Text
            style={[
              Styles.text,
              props.secondLine ? Styles.secondLineText : Styles.none,
              props.textArea ? Styles.textAreaText : Styles.none,
              props.styleText,
            ]}>
            {props.text}
          </Text>
        </View>
      )}
    </View>
  );
};

export default LesionValue;
