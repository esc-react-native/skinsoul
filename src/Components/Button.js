import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import Styles from './Styles/Button';

class ButtonComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  render() {
    return (
      <View style={[Styles.container, this.props.styleContainer]}>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={[
            Styles.button,
            this.props.inverted ? Styles.inverted : Styles.button,
            this.props.disabled || this.props.isLoading
              ? Styles.disabled
              : Styles.enabled,
            this.props.styleButton,
          ]}
          disabled={this.props.disabled || this.props.isLoading}>
          <Text
            style={[
              Styles.text,
              this.props.inverted ? Styles.invertedText : Styles.text,
              this.props.styleButtonText,
            ]}>
            {this.props.isLoading ? this.props.textLoading : this.props.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ButtonComponent;
