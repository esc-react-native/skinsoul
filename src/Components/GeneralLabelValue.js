/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text} from 'react-native';
import Styles from './Styles/GeneralLabelValue';

const GeneralLabelValue = props => {
  return (
    <View
      style={[
        Styles.container,
        props.secondLine ? Styles.secondLineContainer : Styles.none,
        props.style,
      ]}>
      <View
        style={[
          Styles.labelView,
          props.secondLine ? Styles.secondLineLabelView : Styles.none,
        ]}>
        <Text style={[Styles.label, props.styleLabel]}>{props.label}</Text>
        {props.secondLine && (
          <Text style={[Styles.label, props.styleLabel]}>
            {props.secondLine}
          </Text>
        )}
      </View>
      <View style={[Styles.textView]}>
        <Text style={[Styles.text, props.styleText]}>{props.text}</Text>
      </View>
    </View>
  );
};

export default GeneralLabelValue;
