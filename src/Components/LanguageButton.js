/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text, Image} from 'react-native';
import Styles from './Styles/LanguageButton';
import {Images} from '../Styles';

import {Button} from 'native-base';
//Multilanguage
import i18 from '../Localize/I18n';

//export default class Login extends Component {

const ChangeLanguage = (props, language) => {
  i18.config(language);
  props.onPress();
};

const LanguageButton = props => {
  let flag;
  let langText;
  let lng;

  if (props.pr) {
    flag = Images.language_pr;
    langText = i18.t('portuguese');
    lng = 'pr';
  } else if (props.es) {
    flag = Images.language_es;
    langText = i18.t('spanish');
    lng = 'es';
  } else if (props.fr) {
    flag = Images.language_fr;
    langText = i18.t('french');
    lng = 'fr';
  } else {
    flag = Images.language_en;
    langText = i18.t('english');
    lng = 'en';
  }

  return (
    <Button
      transparent
      style={Styles.container}
      onPress={() => ChangeLanguage(props, lng)}>
      <Image source={flag} style={Styles.button} />
      {props.label && <Text>{langText}</Text>}
    </Button>
  );
};

export default LanguageButton;
