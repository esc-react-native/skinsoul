/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text, Image} from 'react-native';
import Styles from './Styles/ButtonFull';
import {Images} from '../Styles';

//import {Container, Button} from 'native-base';
import {Button} from '../Components/Base';
//Multilanguage
import i18 from '../Localize/I18n';

const ButtonFull = props => {
  return (
    <View style={[Styles.background]}>
      <Button style={[Styles.container]} onPress={props.onPress} {...props}>
        <Text style={[Styles.text, props.styleText]}>{props.text}</Text>
      </Button>
    </View>
  );
};

export default ButtonFull;
