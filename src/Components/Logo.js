/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Image} from 'react-native';
import Styles from './Styles/Logo';
import {Images} from '../Styles';

import {Container} from 'native-base';

const Logo = props => {
  return (
    <View style={Styles.container}>
      <Image source={Images.logo} style={Styles.image} />
    </View>
  );
};

export default Logo;
