//This is an example code for the camera//
import React from 'react';
//import react in our code.
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ActivityIndicator,
  PermissionsAndroid,
  Platform,
  Image,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {Images, Metrics} from '../Styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class CameraKit extends React.Component {
  state = {isPermitted: false};
  constructor(props) {
    super(props);

    this.state = {
      photo: null,

      isTaked: false,

      redButton: false,
      processing: false,
      flashOn: false,
    };
    //this.onPress();
    this.handleRetake = this.handleRetake.bind();
    this.handleExit = this.handleExit.bind();
  }

  handleRetake = () => {
    this.setState({isTaked: false});
    this.camera.resumePreview();
  };

  handleSubmit = () => {
    this.props.onSubmit(this.state.photo);
  };

  handleExit = () => {
    this.props.onCancel();
  };

  render() {
    let buttonBar;

    if (this.state.processing) {
      const imagen = this.state.photo;
      console.log('isTaked Photo State');
      console.log(imagen);

      buttonBar = (
        <View style={styles.buttonBar}>
          <Text style={styles.buttonText}>{'PROCESSING...'}</Text>
        </View>
      );
    } else {
      buttonBar = (
        <View style={styles.buttonBar}>
          <TouchableOpacity onPress={this.handleExit} style={styles.button}>
            <Ionicons name="ios-arrow-back" color={'#FFFFFF'} size={64} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.button}>
            <Icon
              name="circle"
              color={this.state.redButton ? '#FF0000' : '#FFFFFF'}
              size={this.state.redButton ? 80 : 84}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.setState({flashOn: !this.state.flashOn})}
            style={styles.button}>
            <Ionicons
              name={this.state.flashOn ? 'md-flash-off' : 'md-flash'}
              color={'#FFFFFF'}
              size={64}
            />
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <StatusBar translucent backgroundColor="transparent" />
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          //flashMode={RNCamera.Constants.FlashMode.auto}
          flashMode={
            this.state.flashOn
              ? RNCamera.Constants.FlashMode.torch
              : RNCamera.Constants.FlashMode.off
          }
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        {buttonBar}
      </View>
    );
  }

  takePicture = async () => {
    await this.setState({redButton: true});

    setTimeout(() => {
      this.setState({processing: true});
    }, 1000);

    if (this.camera) {
      const options = {
        quality: 1,
        base64: true,
        pauseAfterCapture: true,
        fixOrientation: true,
        forceUpOrientation: true,
      };
      const data = await this.camera.takePictureAsync(options);
      await this.setState({photo: data.uri, processing: true});
      const uri = data.uri;
      console.log(uri);
      this.handleSubmit(this);
      return;
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',

    backgroundColor: 'black',
    height: Metrics.windowHeight + Metrics.statusBarHeight,
  },

  preview: {
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: 'gray',
    height: '100%',
  },

  backPreview: {
    //backgroundColor: 'blue',
    height: 75,
    width: 45,
  },

  camerapreview: {
    backgroundColor: 'white',
    marginHorizontal: '4%',
    height: 70,
    width: 40,
  },

  buttonBar: {
    //position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'black',
    width: Metrics.screenWidth,
    height: Metrics.screenDesignHeight * 120,
    //paddingHorizontal: Metrics.screenDesignWidth * 35,
    //marginBottom: Metrics.screenDesignHeight * 180,
    //backgroundColor: 'red',
  },

  button: {
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.screenDesignHeight * 110,
    width: Metrics.screenDesignWidth * 80,
  },

  retake: {
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.screenDesignHeight * 110,
    width: Metrics.screenDesignHeight * 110,
  },

  buttonText: {
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    //height: Metrics.screenDesignHeight * 110,
    width: Metrics.screenWidth,
    fontSize: 20,
    textAlign: 'center',
  },

  capture: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
