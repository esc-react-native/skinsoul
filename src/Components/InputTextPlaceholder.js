/* eslint-disable keyword-spacing */
import React from 'react';
import {View, TextInput, Keyboard} from 'react-native';
import Styles from './Styles/InputTextPlaceholder';
import {Images} from '../Styles';

import {Container, Item, Label, Input} from 'native-base';

import {HideNavigationBar} from 'react-native-navigation-bar-color';


export default class InputText extends React.Component {
  componentDidMount() {
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );
  }

  componentWillUnmount() {
    this.keyboardDidHideListener.remove();
  }

  keyboardDidHide = () => {
    Keyboard.dismiss();

    HideNavigationBar();

    if (this.props.onUnfocus) this.props.onUnfocus();
  };

  render() {
    return (
      <View
        style={[
          Styles.container,
          this.props.style !== undefined ? this.props.style : Styles.none,
        ]}>
        <View
          style={[
            Styles.item,
            this.props.transparent ? Styles.noWhite : Styles.white,
            this.props.styleView,
          ]}>
          <TextInput
            style={[
              Styles.inputText,
              this.props.transparent ? Styles.noWhite : Styles.white,
              this.props.styleInput,
            ]}
            onChangeText={this.props.onChangeText}
            value={this.props.value}
            placeholder={this.props.placeholder}
            placeholderTextColor={
              this.props.placeholderTextColor
                ? this.props.placeholderTextColor
                : '#000000'
            }
            spellCheck={false}
            autoCorrect={false}
            autoCompleteType="off"
            {...this.props}
          />
        </View>
      </View>
    );
  }
}
