import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert} from 'react-native';
import Styles from './Styles/ModalButtonHome';
import {Colors} from '../Styles/index';

import Icon from 'react-native-vector-icons/Feather';
//Multilanguage
import i18 from '../Localize/I18n';

export default class ModalButtonHome extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: this.props.visible,
    };
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.visible}>
          <View style={Styles.container}>
            <View style={Styles.modal}>
              <Icon name={this.props.icon} color={Colors.app} size={32} />
              <Text style={Styles.question}>{this.props.question}</Text>
              <Text style={Styles.disclaimer}>{this.props.disclaimer}</Text>
              <View style={Styles.buttonBar}>
                <TouchableHighlight
                  onPress={this.props.onPressOK}
                  style={[Styles.button, Styles.buttonDivider]}>
                  <Text style={Styles.buttonText}>{i18.t('yes')}</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  onPress={this.props.onPressCancel}
                  style={Styles.button}>
                  <Text style={Styles.buttonText}>{i18.t('no')}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
