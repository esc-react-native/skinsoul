import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import Styles from './Styles/TitleTab';
import {Colors} from '../Styles';

import Icon from 'react-native-vector-icons/FontAwesome';

class TitleTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  render() {
    // console.log('tab X render');
    // console.log(this.props.actualTab);

    return (
      <View style={[Styles.container, this.props.style]}>
        <TouchableOpacity
          onPress={() => this.props.onPress(0)}
          style={[
            Styles.button,
            this.props.actualTab === 0 ? Styles.enabled : Styles.buttonDisabled,
          ]}>
          <Icon
            name="user-circle"
            color={
              this.props.actualTab === 0
                ? '#FFFFFF'
                : '#DADADA'
            }
            size={24}
          />
          <Text
            style={[
              Styles.text,
              this.props.actualTab === 0
                ? Styles.textEnabled
                : Styles.textDisabled,
              this.props.styleButton,
            ]}>
            {this.props.tabText1}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.onPress(1)}
          style={[
            Styles.button,
            this.props.actualTab === 1 ? Styles.button : Styles.buttonDisabled,
          ]}>
          <Icon
            name="list-alt"
            color={
              this.props.actualTab === 1
                ? '#FFFFFF'
                : '#DADADA'
            }
            size={24}
          />
          <Text
            style={[
              Styles.text,
              this.props.actualTab === 1
                ? Styles.textEnabled
                : Styles.textDisabled,
              this.props.styleButton,
            ]}>
            {this.props.tabText2}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default TitleTab;
