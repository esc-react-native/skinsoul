/* eslint-disable keyword-spacing */
import React from 'react';
import {View} from 'react-native';
import Styles from './Styles/LanguageBar';


import LanguageButton from './LanguageButton';

const LanguageBar = props => {

  return (
    <View style={Styles.container}>
      <LanguageButton label en onPress={() => props.onPress()}/>
      <LanguageButton label es onPress={props.onPress}/>
      <LanguageButton label pr onPress={props.onPress}/>
    </View>
  );
};

export default LanguageBar;
