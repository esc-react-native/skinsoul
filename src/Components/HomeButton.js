/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text, Image} from 'react-native';
import Styles from './Styles/HomeButton';
import {Images} from '../Styles';

import {Button} from 'native-base';

//export default class Login extends Component {

const HomeButton = props => {
  return (
    <Button transparent style={Styles.container} onPress={props.onPress}>
      <Image source={props.source} style={Styles.icon} />
      {props.text && <Text style={Styles.text}>{props.text}</Text>}
    </Button>
  );
};

export default HomeButton;
