import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import Styles from './Styles/ButtonHome';

class ButtonHome extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  render() {
    return (
      <View style={[Styles.container, this.props.style]}>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={[
            Styles.button,
            this.props.disabled || this.props.isLoading
              ? Styles.disabled
              : Styles.enabled,
            this.props.styleButton,
          ]}
          disabled={this.props.disabled || this.props.isLoading}>
          <Image
            source={this.props.source}
            style={[Styles.image, this.props.styleImage]}
          />
          <Text style={[Styles.text, this.props.styleButton]}>
            {this.props.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ButtonHome;
