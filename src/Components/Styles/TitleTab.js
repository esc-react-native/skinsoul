import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics, AppStyles} from '../../Styles/index';

//
export default StyleSheet.create({
  ...AppStyles.screen,

  container: {
    ...AppStyles.screen.cardTitle,
    flexDirection: 'row',
  },

  iconView: {
    width: ( Metrics.screenDesignWidth * 320 ) / 2,
    height: (Metrics.screenHeight * 0.1) / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    width: ( Metrics.screenDesignWidth * 320 ) / 2,
    height: ( Metrics.screenDesignHeight * 59 ),

    borderBottomWidth: 4,
    borderBottomColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonDisabled: {
    borderBottomColor: 0,
  },

  text: {
    ...Fonts.style.normal,
    fontSize: 13,
    color: 'white',
    marginTop: 2,
  },

  textDisabled: {
    color: 'rgba(255, 255, 255, 0.5)',
  },

});
