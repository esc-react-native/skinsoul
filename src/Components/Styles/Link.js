import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: Metrics.screenWidth,
    marginTop: Metrics.marginVerticalSmall,
  },

  button: {
    backgroundColor: Colors.transparent,
    width: (Metrics.screenWidth * 2) / 3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },

  text: {
    ...Fonts.style.LoginInput,
    fontSize: Fonts.size.loginLink,
    textAlign: 'center',
    color: Colors.buttonText,
//    marginVertical: Metrics.marginVertical,
  },

});
