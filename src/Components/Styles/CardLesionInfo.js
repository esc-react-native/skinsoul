/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Metrics, AppStyles, Colors, Fonts} from '../../Styles/index';

const CONTAINER_MARGIN_VERTICAL = 7 * Metrics.screenDesignWidth;
const CONTAINER_PADDING_HORIZONTAL = 10 * Metrics.screenDesignWidth;

//.
export default StyleSheet.create({
  ...AppStyles.screen,

  cardTitleSubtitle: {
    fontSize: 14,
  },

  lesionContainer: {
    justifyContent: 'flex-start',
    width: Metrics.screenDesignWidth * 320,
  },

  photoContainer: {
    justifyContent: 'flex-start',
    width: Metrics.screenDesignWidth * 320,
    backgroundColor: 'white',
  },

  lesionUpdateBody: {
    height: Metrics.windowDesignHeight * 440,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  lesionPhotoBody: {
    height: Metrics.windowDesignHeight * ( 420 + 59 ),
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',

    // backgroundColor: 'red',
  },


  componentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //paddingHorizontal: CONTAINER_PADDING_HORIZONTAL,
    //paddingVertical: CONTAINER_MARGIN_VERTICAL,
    //marginVertical: CONTAINER_MARGIN_VERTICAL,
    width: Metrics.screenDesignWidth * 261,
    height: Metrics.screenDesignHeight * 50,

    //borderColor: '#DADADA',
    //borderWidth: 1,
    //borderRadius: 2,
    // backgroundColor: 'red',
  },

  dropdownBiopsyPlaceholder: {
    //backgroundColor: 'magenta',
  },

  dropdownBiopsyBox: {
    // backgroundColor: 'cyan',
    borderRadius: 0,
    borderColor: '#ECECEC',
    borderWidth: 0,

    marginBottom: 3,
  },


  biopsyDiagnosisTextDropdown: {
    ...Fonts.style.cardLabelText,
    fontSize: 14,

    width: Metrics.screenDesignWidth * 320,
    textAlign: 'center',
    color: Colors.app,
    //marginTop: 10,
    // backgroundColor: 'cyan',

  },

  biopsyDiagnosisText: {
    ...Fonts.style.cardLabelText,
    fontSize: 14,

    width: Metrics.screenDesignWidth * 320,
    textAlign: 'center',
    color: Colors.app,

    // backgroundColor: 'cyan',

  },

  biopsyDiagnosisView: {
    width: Metrics.screenDesignWidth * 320,
    justifyContent: 'center',
    alignItems: 'center',

    // borderColor: '#DADADA',
    // borderWidth: 1,
    // borderRadius: 2,
    // backgroundColor: 'yellow',
  },

  biopsyDiagnosisViewIn: {
    width: Metrics.screenDesignWidth * 263,
    justifyContent: 'center',
    alignItems: 'center',

    borderColor: '#DADADA',
    borderWidth: 1,
    borderRadius: 2,
    // backgroundColor: 'blue',
  },

  lesionFooter: {
    flexDirection: 'row',
    height: Metrics.screenDesignHeight * 50,
    justifyContent: 'space-between',
  },

  cardImageContainer: {
    marginHorizontal: Metrics.screenDesignWidth * 6,
    width: Metrics.windowDesignHeight * 260,
    height: Metrics.windowDesignHeight * ( 420 + 40 ),
    justifyContent: 'center',
    alignItems: 'center',

    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.white,
    overflow: 'hidden',
  },


  cardImage: {
    //resizeMode: 'stretch',
    marginHorizontal: Metrics.screenDesignWidth * 6,
    width: Metrics.windowDesignHeight * 240,
    height: Metrics.windowDesignHeight * ( 440 ),

    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.white,
    overflow: 'hidden',


    // backgroundColor: 'blue',
  },

  loadingImage: {
    resizeMode: 'stretch',
    height: 0,
    width: 0,
  },
});
