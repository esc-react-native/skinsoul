import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const INPUT_HEIGHT = 42 * Metrics.screenDesignHeight;
const INPUT_WIDTH = 250 * Metrics.screenDesignWidth;

const CONTAINER_PAD_VERTICAL =
  (Platform.OS === 'android' ? 3 : 3) * Metrics.screenDesignHeight;

const TEXT_INPUT_LEFT = 15 * Metrics.screenDesignWidth;

const LABEL_MARGIN_TOP =
  4 * (Platform.OS === 'android') ? 0 : Metrics.screenDesignHeight;

const LABEL_MARGIN_BOTTOM =
  4 * (Platform.OS === 'android') ? Metrics.screenDesignHeight : 0;

const LABEL_MARGIN_HORIZONTAL = 12 * Metrics.screenDesignWidth;
const LABEL_PAD_HORIZONTAL = 3 * Metrics.screenDesignWidth;

const LABEL_BACKGROUND_COLOR_DEFAULT =
  Platform.OS === 'android' ? Colors.transparent : Colors.background;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',

    height: Metrics.screenDesignHeight * 50,
    //width: Metrics.screenDesignWidth,
    paddingTop: CONTAINER_PAD_VERTICAL,
  },

  item: {
    height: Metrics.screenDesignHeight * 40,
    marginTop: Metrics.screenDesignHeight * 8,

    borderRadius: 2,
    borderColor: '#ECECEC',
    borderWidth: 1,

    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: Metrics.screenDesignWidth * 18,
  },

  inputText: {
    ...Fonts.style.normal,
    justifyContent: 'center',
    height: Metrics.screenDesignHeight * 40,
    fontSize: 16,
    color: '#6D6D6D',
  },

  labelContainer: {
    overflow: 'visible',
    position: 'absolute',
    width: INPUT_WIDTH,
  },

  label: {
    ...Fonts.style.normal,

    overflow: 'visible',
    position: 'absolute',
    color: Colors.app,
    backgroundColor: Colors.white,
    fontSize: 12,
    width: 'auto',
    marginHorizontal: Metrics.screenDesignWidth * 7,
    //marginTop: 20 * (Platform.OS === 'android') ? 0 : Metrics.screenDesignHeight,
    marginTop: 5 * Metrics.screenDesignHeight,
    marginBottom: LABEL_MARGIN_BOTTOM,
    paddingHorizontal: Metrics.screenDesignWidth * 5,
  },

  white: {
    backgroundColor: 'white',
  },
});
