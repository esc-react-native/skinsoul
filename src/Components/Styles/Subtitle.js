import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    //flexDirection: 'row',
  },

  text: {
    ...Fonts.style.normal,

    fontSize: 24,
    color: Colors.textDark,
  },
});
