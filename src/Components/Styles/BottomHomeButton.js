import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: Metrics.screenWidth,
//    height: 'auto',
  },

  text: {
    ...Fonts.style.normal,
    fontSize: 12,
    color: '#6D6D6D',
  },

});
