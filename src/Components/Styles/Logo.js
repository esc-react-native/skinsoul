import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    width: 80,
    height: 80,
  },

  image: {
    width: 80,
    height: 80,
  },
});
