import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const TITLE_HEIGHT = 80 * Metrics.screenDesignHeight;
//
export default StyleSheet.create({
  container: {
    height: TITLE_HEIGHT,
  },

  text: {
    ...Fonts.style.normal,

    fontSize: 70,
    color: Colors.white,
  },
});
