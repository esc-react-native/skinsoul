import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const LABEL_MARGIN_RIGHT = 14 * Metrics.screenDesignWidth;
const LABEL_HEIGHT = 50 * Metrics.screenDesignHeight;
const LABEL_WIDTH = 120 * Metrics.screenDesignWidth;

const TEXT_WIDTH = 150 * Metrics.screenDesignWidth;

const CONTAINER_MARGIN_BOTTOM = 4 * Metrics.screenDesignHeight;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: Metrics.screenDesignHeight * 4,
    //backgroundColor: 'red',
  },

  secondLineContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: Metrics.screenDesignHeight * 4,
    //backgroundColor: 'red',
  },

  labelView: {
    width: Metrics.screenDesignWidth * 120,
    height: Metrics.screenDesignHeight * 20,
    justifyContent: 'center',
    alignItems: 'flex-end',
    // backgroundColor: 'blue',
  },

  secondLineLabelView: {
    width: Metrics.screenDesignWidth * 120,
    height: Metrics.screenDesignHeight * 44,
    justifyContent: 'center',
    alignItems: 'flex-end',
    // backgroundColor: 'blue',
  },


  label: {
    ...Fonts.style.normal,
    marginRight: LABEL_MARGIN_RIGHT,
    fontSize: 14,
    color: '#6D6D6D',
    // backgroundColor: 'red',
    textAlign: 'right',
  },

  textView: {
    width: Metrics.screenDesignWidth * 120,
    justifyContent: 'center',
  },

  text: {
    ...Fonts.style.cardLabelText,
    color: Colors.app,
    //fontWeight: '700',
    fontSize: 16,
  },
});
