import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const LABEL_MARGIN_RIGHT = 14 * Metrics.screenDesignWidth;
const LABEL_HEIGHT = 50 * Metrics.screenDesignHeight;
const LABEL_WIDTH = 120 * Metrics.screenDesignWidth;

const TEXT_WIDTH = 150 * Metrics.screenDesignWidth;

const CONTAINER_MARGIN_VERTICAL = 7 * Metrics.screenDesignWidth;
const CONTAINER_PADDING_HORIZONTAL = 10 * Metrics.screenDesignWidth;
const CONTAINER_HEIGHT = 30 * Metrics.screenDesignHeight;
const CONTAINER_WIDTH = Metrics.screenWidth * 0.7;

const TEXT_AREA_WIDTH = CONTAINER_WIDTH - CONTAINER_PADDING_HORIZONTAL - LABEL_MARGIN_RIGHT;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: CONTAINER_PADDING_HORIZONTAL,
    height: CONTAINER_HEIGHT,
    width: CONTAINER_WIDTH,
    marginVertical: CONTAINER_MARGIN_VERTICAL,


  },

  secondLine: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingHorizontal: CONTAINER_PADDING_HORIZONTAL / 2,
    height: CONTAINER_HEIGHT * 2,

    borderColor: '#ECECEC',
    borderWidth: 1,
    borderRadius: 2,
  },

  label: {
    ...Fonts.style.cardLabelText,
    fontSize: 14,
    color: Colors.app,

    borderColor: '#FFF',
    borderWidth: 0,
    borderRadius: 0,
  },

  text: {
    ...Fonts.style.cardLabelText,
    color: Colors.cardTextNormal,
    fontSize: 14,
    textAlign: 'right',

    borderColor: '#FFF',
    borderWidth: 0,
    borderRadius: 0,
  },

  secondLineText: {
    fontSize: 13,
    textAlign: 'center',
  },

  textAreaContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    height: CONTAINER_HEIGHT * 3,

    borderColor: '#ECECEC',
    borderWidth: 1,
    borderRadius: 2,
  },

  ScrollView: {
    borderColor: 'white',
    borderWidth: 0,
    borderRadius: 0,
    paddingBottom: 250,
  },

  textAreaText: {
    ...Fonts.style.cardLabelText,
    fontSize: 14,
    color: Colors.cardTextNormal,

    width: TEXT_AREA_WIDTH,

    borderColor: '#FFF',
    borderWidth: 0,
    borderRadius: 0,

    textAlign: 'left',
  },

});
