import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    width: Metrics.screenDesignWidth * 64,
    // backgroundColor: 'red',
  },

  icon: {
    marginHorizontal: 4,
  },
});
