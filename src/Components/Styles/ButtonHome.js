import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

// const BUTTON_HEIGHT = 110 * Metrics.screenDesignHeight;
// const BUTTON_WIDTH = 145 * Metrics.screenDesignWidth;

const BUTTON_HEIGHT = 110;
const BUTTON_WIDTH = 145;

// const IMAGE_HEIGHT = 45 * Metrics.screenDesignHeight;
// const IMAGE_WIDTH = 45 * Metrics.screenDesignWidth;
// const IMAGE_MARGIN_LEFT = 13 * Metrics.screenDesignWidth;
// const IMAGE_MARGIN_TOP = 13 * Metrics.screenDesignHeight;

// const TEXT_MARGIN_VERTICAL = 15 * Metrics.screenDesignWidth;
// const TEXT_MARGIN_HORIZONTAL = 10 * Metrics.screenDesignWidth;
// const TEXT_MARGIN_BOTTOM = 8 * Metrics.screenDesignWidth;

const IMAGE_HEIGHT = 45;
const IMAGE_WIDTH = 45;
const IMAGE_MARGIN_LEFT = 13;
const IMAGE_MARGIN_TOP = 13;

const TEXT_MARGIN_VERTICAL = 15;
const TEXT_MARGIN_HORIZONTAL = 10;
const TEXT_MARGIN_BOTTOM = 8;


//
export default StyleSheet.create({
  container: {
    //height: BUTTON_HEIGHT,
    //width: BUTTON_WIDTH,
    //borderRadius: 15,
    //justifyContent: 'space-between',
    //backgroundColor: 'red',
  },

  button: {
    height: BUTTON_HEIGHT,
    width: BUTTON_WIDTH,
    borderRadius: 15,
    justifyContent: 'center',
    backgroundColor: 'white',

    elevation: Metrics.shadowElevation,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
  },

  image: {
    height: IMAGE_WIDTH,
    width: IMAGE_WIDTH,
    marginTop: IMAGE_MARGIN_TOP,
    marginLeft: IMAGE_MARGIN_LEFT,
    resizeMode: 'stretch',
    // backgroundColor: 'red',
  },

  imageLab: {
    height: IMAGE_WIDTH - 5,
    width: IMAGE_WIDTH - 5,
    marginTop: IMAGE_MARGIN_TOP,
    marginLeft: IMAGE_MARGIN_LEFT,
    resizeMode: 'stretch',
    // backgroundColor: 'red',
  },


  text: {
    ...Fonts.style.HomeButton,
    fontSize: 16,
    // marginVertical: TEXT_MARGIN_VERTICAL,
    marginHorizontal: TEXT_MARGIN_HORIZONTAL,
    marginBottom: TEXT_MARGIN_BOTTOM,
    // backgroundColor: 'green',
  },


  disabled: {
    backgroundColor: Colors.grayLight,
  },

  invertedText: {
    ...Fonts.style.LoginInput,
    fontSize: 18,
    textAlign: 'center',
    color: Colors.app,
    marginVertical: Metrics.marginVertical,
  },
});
