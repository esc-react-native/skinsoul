import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: 80,
    height: 'auto',
  },

  button: {
    width: 80,
    height: 60,
  },

  text: {
    ...Fonts.style.normal,

    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.text,
  },

});
