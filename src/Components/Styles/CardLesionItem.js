import {StyleSheet} from 'react-native';
import {Fonts, Metrics, AppStyles, Colors} from '../../Styles/index';

//
export default StyleSheet.create({
  ...AppStyles.screen,

  container: {
    //...AppStyles.screen.cardTitle,
    flexDirection: 'row',
  },

  cardItem: {
    flexDirection: 'row',
    //justifyContent: 'space-between',
    alignItems: 'center',

    borderColor: 'rgba(0, 0, 0, 0.25)',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,

    backgroundColor: Colors.app,
    marginTop: Metrics.screenDesignHeight * 10,
    height: Metrics.screenDesignHeight * 86,

    //backgroundColor: 'yellow',
  },

  cardItemTouch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    //backgroundColor: Colors.app,
    height: Metrics.screenDesignHeight * 86,

    // backgroundColor: 'blue',
  },

  cardImage: {
    resizeMode: 'stretch',
    marginHorizontal: Metrics.screenDesignWidth * 6,
    height: Metrics.screenDesignHeight * 72,
    width: Metrics.screenDesignHeight * 72,
    backgroundColor: 'white',
  },

  loadingImage: {
    resizeMode: 'stretch',
    height: 0,
    width: 0,
  },

  loadingText: {
    marginHorizontal: Metrics.screenDesignWidth * 6,
    height: Metrics.screenDesignHeight * 72,
    width: Metrics.screenDesignHeight * 72,
    color: Colors.white,
    textAlign: 'center',
    textAlignVertical: 'center',
    // backgroundColor: 'blue',
  },

  cardLabel: {
    //    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: Metrics.screenDesignWidth * 200,
    height: Metrics.screenDesignHeight * 86,
    marginRight: Metrics.screenDesignWidth * 6,
    // backgroundColor: 'cyan',
  },

  cardLabelTitle: {
    ...Fonts.style.normal,
    color: Colors.white,
    height: Metrics.screenDesignHeight * 20,
    fontSize: 12,
    // backgroundColor: 'red',
    alignItems: 'center',
    textAlignVertical: 'center',
  },

  cardLabelDescription: {
    ...Fonts.style.normal,
    color: Colors.white,
    // height: Metrics.screenDesignHeight * 45,
    height: Metrics.screenDesignHeight * 50,
    fontSize: 9,
    //backgroundColor: 'black',
    alignItems: 'center',
    textAlignVertical: 'center',
  },

  cardLabelFooter: {
    ...Fonts.style.cardLabelText,
    width: '60%',
    marginVertical: Metrics.marginVerticalSmall,
  },

  cardItemFooter: {
    marginLeft: Metrics.screenDesignWidth * 72,
    width: Metrics.screenDesignWidth * 100,
    height: Metrics.screenDesignHeight * 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    position: 'absolute',
    bottom: 0,
    //backgroundColor: 'red',
  },

  cardItemButton: {
    width: Metrics.screenDesignWidth * 100,
    height: Metrics.screenDesignHeight * 18,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,

    borderColor: '#DADADA',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
  },

  cardItemButtonText: {
    //...AppStyles.style.normal,

    color: Colors.app,
  },

});
