import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    height: Metrics.screenHeight,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    //backgroundColor: 'red',
  },

  modal: {
    height: Metrics.screenDesignHeight * 179,
    width: Metrics.screenDesignWidth * 248,
    backgroundColor: Colors.white,

    borderRadius: 5,
    borderColor: Colors.app,
    borderWidth: 1,
    flexDirection: 'column',
    //justifyContent: 'center',
    alignItems: 'center',

    paddingTop: Metrics.screenDesignHeight * 12,
  },

  question: {
    ...Fonts.style.normal,

    fontSize: 14,
    color: '#000',
    height: Metrics.screenDesignHeight * 40,
    width: Metrics.screenDesignWidth * 240,
    alignItems: 'center',
    textAlign: 'center',
  },

  disclaimer: {
    ...Fonts.style.normal,

    fontSize: 14,
    color: '#6D6D6D',
    height: Metrics.screenDesignHeight * 60,
    width: Metrics.screenDesignWidth * 240,
    alignItems: 'center',
    textAlign: 'center',
  },

  buttonBar: {
    height: Metrics.screenDesignHeight * 31,
    width: Metrics.screenDesignWidth * 248,
    flexDirection: 'row',

    borderTopColor: '#ECECEC',
    borderTopWidth: 1,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,

    //backgroundColor: 'red',
  },

  button: {
    height: Metrics.screenDesignHeight * 31,
    width: ( Metrics.screenDesignWidth * 248 ) / 2,
    flexDirection: 'column',
    justifyContent: 'center',

    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },

  buttonDivider: {
    borderRightColor: '#ECECEC',
    borderRightWidth: 1,
  },

  buttonText: {
    color: '#000',
    fontSize: 14,
    textAlign: 'center',
  },

});
