import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const INPUT_HEIGHT = 42 * Metrics.screenDesignHeight;
const INPUT_WIDTH = 250 * Metrics.screenDesignWidth;

const CONTAINER_PAD_VERTICAL =
  (Platform.OS === 'android' ? 3 : 3) * Metrics.screenDesignHeight;

const TEXT_INPUT_LEFT = 15 * Metrics.screenDesignWidth;

const LABEL_MARGIN_TOP =
  4 * (Platform.OS === 'android') ? 0 : Metrics.screenDesignHeight;

const LABEL_MARGIN_BOTTOM =
  4 * (Platform.OS === 'android') ? Metrics.screenDesignHeight : 0;

const LABEL_MARGIN_HORIZONTAL = 12 * Metrics.screenDesignWidth;
const LABEL_PAD_HORIZONTAL = 3 * Metrics.screenDesignWidth;

const LABEL_BACKGROUND_COLOR_DEFAULT =
  Platform.OS === 'android' ? Colors.transparent : Colors.background;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',

    height: Metrics.screenDesignHeight * 50,
    width: Metrics.screenWidth,
    paddingTop: CONTAINER_PAD_VERTICAL,

    //backgroundColor: 'red',
  },

  item: {
    justifyContent: 'center',
    height: Metrics.screenDesignHeight * 40,
    marginTop: Metrics.screenDesignHeight * 10,

    borderRadius: 2,
    borderColor: '#6D6D6D',
    borderWidth: 1,
    backgroundColor: Colors.transparent,

    // backgroundColor: 'red',
  },

  inputText: {
    ...Fonts.style.normal,

    width: INPUT_WIDTH,

    height: Metrics.screenDesignHeight * 35,
    paddingLeft: TEXT_INPUT_LEFT,
    fontSize: 14,
  },

  labelContainer: {
    overflow: 'visible',
    position: 'absolute',
    width: INPUT_WIDTH,
  },

  label: {
    ...Fonts.style.normal,

    overflow: 'visible',
    position: 'absolute',
    color: Colors.app,
    backgroundColor: Colors.background,
    fontSize: 12,
    width: 'auto',
    marginHorizontal: LABEL_MARGIN_HORIZONTAL,
    //marginTop: 20 * (Platform.OS === 'android') ? 0 : Metrics.screenDesignHeight,
    marginTop: 5 * Metrics.screenDesignHeight,
    marginBottom: LABEL_MARGIN_BOTTOM,
    paddingHorizontal: LABEL_PAD_HORIZONTAL,
  },

  white: {
    backgroundColor: 'white',
  },
});
