import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const BUTTON_WIDTH = 250 * Metrics.screenDesignWidth;
const BUTTON_HEIGHT = 42 * Metrics.screenDesignHeight;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: Metrics.screenWidth,
    // backgroundColor: 'cyan',
  },

  button: {
    backgroundColor: Colors.app,
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,

    justifyContent: 'center',
  },

  inverted: {
    backgroundColor: Colors.buttonText,
    //width: (Metrics.screenWidth * 2) / 3,
    borderColor: Colors.app,
    borderWidth: 1,
    // backgroundColor: 'purple',
  },

  disabled: {
    backgroundColor: Colors.grayLight,
  },

  text: {
    ...Fonts.style.LoginInput,

    textAlign: 'center',
    color: Colors.buttonText,
  },

  invertedText: {
    ...Fonts.style.LoginInput,

    textAlign: 'center',
    color: Colors.app,
  },

});
