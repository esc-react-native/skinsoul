import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    backgroundColor: Colors.button,
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    width: Metrics.screenWidth - Metrics.marginHorizontalLarge * 2,
    marginHorizontal: Metrics.marginHorizontalLarge,
  },

  text: {
    ...Fonts.style.normal,

    fontSize: 20,
    //width: Metrics.screenWidth,
    textAlign: 'center',
    color: Colors.buttonText,
  },

  background: {
    backgroundColor: Colors.background,
  }
});
