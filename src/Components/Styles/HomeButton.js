import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: Metrics.screenWidth,
    height: 'auto',
  },

  icon: {
    width: Metrics.screenWidth / 3,
    height: Metrics.screenWidth / 3,
  },

  text: {
    ...Fonts.style.normal,

    fontSize: Fonts.size.h5,
    color: Colors.text,
  },

});
