import {StyleSheet} from 'react-native';
import {Fonts, Metrics, AppStyles} from '../../Styles/index';

const ICON_CONTAINER_WIDTH = 60 * Metrics.screenDesignWidth;
const TITLE_CONTAINER_WIDTH = (301 - ICON_CONTAINER_WIDTH) * Metrics.screenDesignWidth;
//
export default StyleSheet.create({
  ...AppStyles.screen,

  container: {
    ...AppStyles.screen.cardTitle,
    flexDirection: 'row',
  },

  iconView: {
    width: ICON_CONTAINER_WIDTH,
    height: Metrics.screenDesignHeight * 59,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },

  titleView: {
    height: Metrics.screenDesignHeight * 59,
    justifyContent: 'center',
    //alignItems: 'center',
    width: TITLE_CONTAINER_WIDTH,
    //paddingLeft: ICON_CONTAINER_WIDTH / 2,
    // backgroundColor: 'blue',
  },

  title: {
    ...Fonts.style.normal,

    color: 'white',
    textAlign: 'center',
    fontSize: 22,

    // backgroundColor: 'red',
  },

  subtitle: {
    ...Fonts.style.normal,

    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
});
