import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const BUTTON_HEIGHT = 30 * Metrics.screenDesignHeight;
const BUTTON_WIDTH = 100 * Metrics.screenDesignWidth;
//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',

    width: Metrics.screenDesignWidth * 110,
    justifyContent: 'center',
    // backgroundColor: 'yellow',
  },

  iconText: {
    ...Fonts.style.normal,
    fontSize: 20,
    marginRight: 10,
    fontWeight: '900',
    color: Colors.app,
  },

  iconTextFwd: {
    ...Fonts.style.cardTextNormal,
    fontSize: 20,
    marginLeft: 10,
    fontWeight: '900',
    color: Colors.app,
  },


  text: {
    ...Fonts.style.normalBold,
    color: Colors.app,
  },

  button: {
    width: Metrics.screenDesignWidth * 110,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
    // backgroundColor: 'red',

    height: BUTTON_HEIGHT,
  },

  iconView: {
    marginLeft: Metrics.screenDesignWidth * 50,
  },
});
