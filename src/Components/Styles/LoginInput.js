import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

const INPUT_HEIGHT = 42 * Metrics.screenDesignHeight;
const INPUT_WIDTH = 250 * Metrics.screenDesignWidth;

const CONTAINER_PAD_VERTICAL =
  (Platform.OS === 'android' ? 3 : 3) * Metrics.screenDesignHeight;

const TEXT_INPUT_LEFT = 15 * Metrics.screenDesignWidth;

const LABEL_MARGIN_TOP =
  4 * (Platform.OS === 'android') ? 0 : Metrics.screenDesignHeight;

const LABEL_MARGIN_BOTTOM =
  4 * (Platform.OS === 'android') ? Metrics.screenDesignHeight : 0;

const LABEL_MARGIN_HORIZONTAL = 12 * Metrics.screenDesignWidth;
const LABEL_PAD_HORIZONTAL = 3 * Metrics.screenDesignWidth;

const LABEL_BACKGROUND_COLOR_DEFAULT =
  Platform.OS === 'android' ? Colors.transparent : Colors.background;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: Metrics.screenDesignHeight * 50,
    width: Metrics.screenWidth,
    paddingTop: CONTAINER_PAD_VERTICAL,
    //backgroundColor: 'red',
  },

  item: {
    justifyContent: 'center',
    paddingLeft: TEXT_INPUT_LEFT,

    height: INPUT_HEIGHT,
    width: INPUT_WIDTH,
    backgroundColor: Colors.white,

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},

    // backgroundColor: 'red',
  },

  inputText: {
    ...Fonts.style.LoginInput,
    borderRadius: 2,
    backgroundColor: Colors.background,
  },

  labelContainer: {
    overflow: 'visible',
    position: 'absolute',
    width: INPUT_WIDTH,
  },

  label: {
    ...Fonts.style.normal,

    overflow: 'visible',
    position: 'absolute',
    color: Colors.app,
    fontSize: 12,
    width: 'auto',
    marginHorizontal: LABEL_MARGIN_HORIZONTAL,
    marginTop: LABEL_MARGIN_TOP,
    marginBottom: LABEL_MARGIN_BOTTOM,
    backgroundColor: LABEL_BACKGROUND_COLOR_DEFAULT,
    paddingHorizontal: LABEL_PAD_HORIZONTAL,
  },

  white: {
    backgroundColor: 'white',
  },
});
