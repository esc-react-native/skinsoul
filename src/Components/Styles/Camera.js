import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../Styles';
import { autoShowTooltip } from '@aws-amplify/ui';

const styles = StyleSheet.create({
  ...AppStyles.screen,

  button: {
    marginHorizontal: Metrics.marginHorizontal,
    alignItems: 'center',
    height: 50,
    width: Metrics.screenWidth * 0.2,
    justifyContent: 'center',
  },

  icon: {
    color: Colors.iconLight,
  },

  image: {
    width: Metrics.screenWidth,
    height: (Metrics.bodyHeightNoHeader * 2) / 3,
    resizeMode: 'stretch',
  },
});

export default styles;
