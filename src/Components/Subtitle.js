/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text} from 'react-native';
import Styles from './Styles/Subtitle';


const Subtitle = props => {
  return (
    <View style={Styles.container}>
      <Text style={[Styles.text, props.style]}>{props.text}</Text>
    </View>
  );
};

export default Subtitle;
