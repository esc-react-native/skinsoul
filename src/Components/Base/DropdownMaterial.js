import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Styles from './Styles/DropdownMaterial';

import {Dropdown} from 'react-native-material-dropdown';

class DropdownMaterial extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };

    this.getOptions = this.getOptions.bind();
  }

  getOptions = () => {
    const options = this.props.options;

    return options;
  };

  render() {
    const options = this.getOptions();

    return (
      <View style={[Styles.container]}>
        <View key={this.props.placeholder} style={Styles.inputView}>
          <Dropdown
            pickerStyle={Styles.picker}
            containerStyle={Styles.pickerContainer}
            onChangeText={this.props.onValueChange}
            value={this.props.value}
            data={options}
            //baseColor={'red'}
            inputContainerStyle={{
              borderBottomColor: 'white',
              borderBottomWidth: 1,
            }}
            dropdownOffset={{top: 16, left: 0}}
            textColor={'#6D6D6D'}
            affixTextStyle={[Styles.font, this.props.styleFont]}
            itemTextStyle={[Styles.font, this.props.styleFont]}
            labelTextStyle={[Styles.font, this.props.styleFont]}
            style={[Styles.font, this.props.styleFont]}
            titleTextStyle={[Styles.font, this.props.styleFont]}

            rippleDuration={150}
            animationDuration={125}
            itemCount={this.props.options.length}
          />
        </View>
      </View>
    );
  }
}

export default DropdownMaterial;
