import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Icon, Item, Picker} from 'native-base';
import {Metrics} from '../../Styles';
import Styles from './Styles/Dropdown';

class Dropdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };

    // console.log('render');
    // console.log(this.renderOptions());
  }

  renderOptions = () => {
    const options = this.props.options;

    return options.map((option, i) => (
      <Picker.Item
        key={i}
        label={option.label}
        value={option.value}
        style={Styles.pickerItem}
      />
    ));
  };

  render() {
    console.log('Dropdown -> this.props.placeholder');
    console.log(this.props.placeholder);

    console.log('Dropdown -> this.props.value');
    console.log(this.props.value);

    return (
      <Item key={this.props.placeholder} style={Styles.container}>
        <Picker
          mode="dropdown"
          iosIcon={<Icon name="arrow-down" />}
          placeholder={this.props.placeholder}
          style={Styles.picker}
          placeholderStyle={Styles.placeholder}
          textStyle={Styles.textStyle}
          itemStyle={Styles.itemStyle}
          itemTextStyle={Styles.itemTextStyle}
          placeholderIconColor={this.props.placeholderIconColor}
          selectedValue={
            this.props.value === null
              ? this.props.placeholder
              : this.props.value
          }
          onValueChange={this.props.onValueChange}
          key={this.props.placeholder}>
          {this.renderOptions()}
        </Picker>
      </Item>
    );
  }
}

export default Dropdown;
