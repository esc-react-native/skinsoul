import React, {Component} from 'react';
import {View, Text, Switch, Image, CheckBox} from 'react-native';
import Styles from './Styles/Switchbox';
import {Colors} from '../../Styles';
//Multilanguage
import i18 from '../../Localize/I18n';

class Switchbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  render() {
    return (
      <View style={[Styles.container, this.props.styleContainer]}>
        <Text style={[Styles.text, Styles.shadowOff]}>{this.props.text}</Text>
        <View style={Styles.right}>
          {this.props.yesno && (
            <Text
              style={[
                Styles.rightText,
                this.props.checked ? Styles.onText : Styles.offText,
              ]}>
              {this.props.checked
                ? i18.t('yes').toUpperCase()
                : i18.t('no').toUpperCase()}
            </Text>
          )}
          <View style={[Styles.switchContainer]}>
            <Switch
              style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
              onValueChange={this.props.onPress}
              value={this.props.checked}
              trackColor={{true: 'rgba(44, 142, 173, 0.5)', false: 'gray'}}
              thumbColor={this.props.checked ? '#5E9FB4' : 'white'}
              ios_backgroundColor={
                this.props.checked ? 'rgba(44, 142, 173, 0.2)' : 'gray'
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

export default Switchbox;
