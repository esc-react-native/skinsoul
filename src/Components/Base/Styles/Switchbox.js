/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';

//
export default StyleSheet.create({
  container: {
    //width: Metrics.screenWidth,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    height: Metrics.screenDesignHeight * 40,
    width: Metrics.screenDesignWidth * 255,
    marginLeft: Metrics.screenDesignWidth * 12,


    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 2 },
  },

  shadowOff: {  
    borderColor:'#FFF', // if you need 
    borderWidth:0,
    //overflow: 'hidden',
    shadowColor: '#000',
    shadowRadius: 0,
    shadowOpacity: 0,
  },

  checkbox: {
    marginRight: Metrics.marginHorizontalLarge,
    transform: [{ scaleX: 0.5 }, { scaleY: 0.8 }],
  },

  switchContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    marginRight: Metrics.screenDesignWidth * 12,
  },

  right: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  rightText: {
    ...Fonts.style.normal,

    fontSize: 12,
    //lineHeight: 0,
    color: 'gray',
    marginRight: Metrics.marginHorizontal,
  },

  onText: {
    color: Colors.app,
  },

  offText: {
    color: Colors.grayLight,
  },



  text: {
    ...Fonts.style.normal,

    fontSize: 12,
    color: '#6D6D6D',

  },

});
