/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';

const BUTTON_HEIGHT = 24 * Metrics.screenDesignHeight;
const BUTTON_WIDTH =  205 * Metrics.screenDesignWidth;

//
export default StyleSheet.create({
  buttonContainer: {
    //flexDirection: 'row',
    //justifyContent: 'space-between',
    //alignItems: 'center',
  },

  button: {
    height: 25 * Metrics.screenDesignHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // backgroundColor: 'red',
  },


  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
  },

  circleRounded: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.app,
    alignItems: 'center',
    justifyContent: 'center',
  },

  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: Colors.app,
  },

  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  label: {
    ...Fonts.style.normalBold,

    fontSize: 16,
    color: 'gray',
//    fontWeight: 'bold',
  },

  text: {
    fontSize: 20,
    color: 'black',
    marginLeft: 10,
  },

});
