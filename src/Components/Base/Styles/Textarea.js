/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';

//
export default StyleSheet.create({
  container: {
    borderColor: Colors.coal,
    // backgroundColor: 'red',
  },

  textarea: {
    ...Fonts.style.textarea,

    height: Metrics.screenDesignHeight * 147,
    width: Metrics.screenDesignWidth * 255,
    marginLeft: Metrics.screenDesignWidth * 12,

    backgroundColor: 'white',
    color: 'black',
    borderWidth: 1, 
    borderRadius: 5, 


  },

});
