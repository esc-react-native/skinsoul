/* eslint-disable prettier/prettier */
import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';


const DROPDOWN_HEIGHT = Metrics.screenDesignHeight;
const DROPDOWN_WIDTH = 235 * Metrics.screenDesignWidth;
const DROPDOWN_PADDING_VERTICAL = 8 * Metrics.screenDesignHeight;
const DROPDOWN_PADDING_HORIZONTAL = 15 * Metrics.screenDesignHeight;

const ICON_WIDTH = 15 * Metrics.screenDesignWidth;
const TEXT_WIDTH = DROPDOWN_WIDTH - ICON_WIDTH - (DROPDOWN_PADDING_HORIZONTAL);

const ITEM_PADDING_VERTICAL = 7 * Metrics.screenDesignHeight;
const ITEM_PADDING_HORIZONTAL = 15 * Metrics.screenDesignWidth;
const ITEM_WIDTH = Metrics.screenWidth;

const CONTAINER_PAD_VERTICAL =
  (Platform.OS === 'android' ? 3 : 3) * Metrics.screenDesignHeight;

//
export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 2 },


    width: Metrics.screenDesignWidth * 255,
    height: Metrics.screenDesignHeight * 40,
    marginLeft: Metrics.screenDesignWidth * 12,
    paddingLeft: Metrics.screenDesignWidth * 10,
    //backgroundColor: 'yellow',
  },

  //.
  inputView: {
    width: Metrics.screenDesignWidth * 235,
    height: Metrics.screenDesignHeight * 40,
    justifyContent: 'center',
    paddingLeft: Metrics.screenDesignWidth * 10,

    // backgroundColor: 'blue',
  },

  pickerContainer: {
    width: Metrics.screenDesignWidth * 225,
    // backgroundColor: 'red',
  },

  placeholder:{
    ...Fonts.style.normal,

    fontSize: 14,

    width: TEXT_WIDTH,
    color: '#6D6D6D',
  },

  picker: {
    width: Metrics.screenWidth * 0.7,
  },

  font: {
    fontFamily: 'Montserrat-Bold',
  },

  subtitle: {
    ...Fonts.style.normal,

    color: Colors.textDark,
    fontSize: Fonts.size.h7,
    marginVertical: Metrics.marginVerticalSmall,
  },

});
