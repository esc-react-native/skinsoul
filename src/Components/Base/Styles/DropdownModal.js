/* eslint-disable prettier/prettier */
import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';

const DROPDOWN_HEIGHT = Metrics.screenDesignHeight;
const DROPDOWN_WIDTH = 235 * Metrics.screenDesignWidth;
const DROPDOWN_PADDING_VERTICAL = 8 * Metrics.screenDesignHeight;
const DROPDOWN_PADDING_HORIZONTAL = 15 * Metrics.screenDesignHeight;

const ICON_WIDTH = 15 * Metrics.screenDesignWidth;
const TEXT_WIDTH = DROPDOWN_WIDTH - ICON_WIDTH - (DROPDOWN_PADDING_HORIZONTAL);

const ITEM_PADDING_VERTICAL = 7 * Metrics.screenDesignHeight;
const ITEM_PADDING_HORIZONTAL = 15 * Metrics.screenDesignWidth;
const ITEM_WIDTH = Metrics.screenWidth;

const CONTAINER_PAD_VERTICAL =
  (Platform.OS === 'android' ? 3 : 3) * Metrics.screenDesignHeight;

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',

    height: Metrics.screenDesignHeight * 50,
    paddingTop: CONTAINER_PAD_VERTICAL,

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 2 },

    paddingHorizontal: Metrics.screenDesignWidth * 12,

  },

  inputView: {
    height: Metrics.screenDesignHeight * 40,
    marginTop: Metrics.screenDesignHeight * 8,

    borderRadius: 2,
    borderColor: '#ECECEC',
    borderWidth: 1,

    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: Metrics.screenDesignWidth * 18,
  },

  placeholder:{
    ...Fonts.style.normal,

    fontSize: 14,

    width: TEXT_WIDTH,
    color: '#6D6D6D',

    //backgroundColor: 'blue',
  },

  icon:{
    width: ICON_WIDTH,
  },

  iconView:{
    width: 30 * Metrics.screenDesignWidth,
    alignItems: 'center',
  },


  listContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingVertical: ITEM_PADDING_VERTICAL,
    paddingHorizontal: ITEM_PADDING_HORIZONTAL,

    width: ITEM_WIDTH,
    borderTopColor: 'gray',
    borderBottomColor: 'gray',
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },

  labelItem: {
    ...Fonts.style.normal,

    fontSize: 18,
  },

  header: {
    height: 22 + 40,
    backgroundColor: Colors.grayLighter,
    paddingTop: 22,
    paddingRight: ITEM_PADDING_HORIZONTAL,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },

  item: {
    justifyContent: 'center',
    // paddingLeft: TEXT_INPUT_LEFT,

    // height: INPUT_HEIGHT,
    // width: INPUT_WIDTH,
    backgroundColor: Colors.background,

    borderColor: '#6D6D6D',
    borderWidth: 1,
    borderRadius: 2,

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
  },

  inputText: {
    ...Fonts.style.LoginInput,
    borderRadius: 2,
    backgroundColor: Colors.background,
  },

  labelContainer: {
    overflow: 'visible',
    position: 'absolute',
    // width: INPUT_WIDTH,
  },

  label: {
    ...Fonts.style.normal,

    overflow: 'visible',
    position: 'absolute',
    color: Colors.app,
    fontSize: 12,
    width: 'auto',
    // marginHorizontal: LABEL_MARGIN_HORIZONTAL,
    // marginTop: LABEL_MARGIN_TOP,
    // marginBottom: LABEL_MARGIN_BOTTOM,
    // backgroundColor: LABEL_BACKGROUND_COLOR_DEFAULT,
    // paddingHorizontal: LABEL_PAD_HORIZONTAL,
  },

  white: {
    backgroundColor: 'white',
  },

});
