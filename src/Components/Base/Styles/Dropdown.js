/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';

//
export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    elevation: 1,
    shadowRadius: 2,
    shadowColor: 'gray',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 2 },


    width: Metrics.screenDesignWidth * 255,
    height: Metrics.screenDesignHeight * 40,
    marginLeft: Metrics.screenDesignWidth * 12,
    paddingLeft: Metrics.screenDesignWidth * 10,
    // backgroundColor: 'red',
  },

  picker: {
    width: Metrics.screenDesignWidth * 255,
  },

  placeholder: {
    color: '#6D6D6D'
  },

  textStyle: {
    color: '#6D6D6D',
    flexDirection: 'column',
  },

  itemStyle: {
    flexDirection: 'column',
    backgroundColor:'red',
    
  },

  itemTextStyle: {
    color: '#6D6D6D',
    flexDirection: 'column',
  },

  subtitle: {
    ...Fonts.style.normal,

    color: Colors.textDark,
    fontSize: Fonts.size.h7,
    ///marginVertical: Metrics.marginVerticalSmall,
  },

  pickerItem: {
    backgroundColor: 'red',
  }

});
