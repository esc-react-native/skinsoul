/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../Styles/index';

//
export default StyleSheet.create({
  container: {
    backgroundColor: Colors.button,
    alignItems: 'center',
    justifyContent: 'center',
    height: Metrics.buttonMediumHeight,
    width: ( Metrics.screenWidth * 2 / 3 ) - Metrics.marginHorizontalLarge * 2,
    marginHorizontal: Metrics.marginHorizontalLarge,
  },

  text: {
    ...Fonts.style.normal,

    fontSize: 20,
    textAlign: 'center',
    color: Colors.buttonText,
  },

  buttonLarge: {
    width: ( Metrics.screenWidth ) - (Metrics.marginHorizontal * 2),
    height: Metrics.buttonLargeHeight,
  },

  buttonRounded: {
    borderRadius: 20,
    shadowRadius: 20,
  },

  background: {
    backgroundColor: Colors.background,
  },

  buttonDisabled: {
    backgroundColor: Colors.buttodinDisabled,
    color: Colors.textDark,
  },
});
