import React, {Component} from 'react';
import {TouchableOpacity, Text, Image} from 'react-native';
import Styles from './Styles/Button';

class ButtonComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  render() {
    let inside;

    if (this.props.text) {
      inside = (
        <Text
          style={[
            Styles.text,
            this.props.styleText,
            this.props.disabled ? Styles.buttonDisabled : this.props.styleText,
          ]}>
          {this.props.isLoading
            ? this.props.textLoading || 'Loading...'
            : this.props.text}
        </Text>
      );
    } else if (this.props.image)
      inside = <Image style={[Styles.image, this.props.styleImage]} />;

    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={[
          Styles.container,

          this.props.large ? Styles.buttonLarge : this.props.styleButton,
          this.props.rounded ? Styles.buttonRounded : this.props.styleButton,
          this.props.disabled ? Styles.buttonDisabled : this.props.styleButton,

          this.props.styleButton,
        ]}
        disabled={this.props.disabled || this.props.isLoading}>
        {inside}
      </TouchableOpacity>
    );
  }
}

export default ButtonComponent;
