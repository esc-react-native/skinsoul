import React, {Component} from 'react';
import Styles from './Styles/Textarea';

import {View} from 'react-native';
import {Container, Header, Content, Textarea, Form} from 'native-base';
export default class TextArea extends Component {
  render() {
    return (
      <View style={[Styles.container, this.props.styleContainer]}>
        <Textarea
          style={Styles.textarea}
          rowSpan={3}
          bordered
          placeholder={this.props.placeholder}
          placeholderTextColor={'#2C8EAD'}
          onChangeText={this.props.onChangeText}
          value={this.props.value}
          scrollEnabled={false}
        />
      </View>
    );
  }
}
