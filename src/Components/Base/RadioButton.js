import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import Styles from './Styles/RadioButton';

export default class RadioButtons extends Component {
  state = {
    value: null,
  };

  handlePress = value => {
    this.setState({value});

    this.props.onPress(value);
  };

  render() {
    const {options} = this.props;

    return (
      <View style={[this.props.style]}>
        {options.map(item => {
          return (
            <View
              key={item.value}
              style={[Styles.buttonContainer, this.props.styleContainer]}>
              <TouchableOpacity
                style={[Styles.button]}
                onPress={() => this.handlePress(item.value)}>
                <View style={[Styles.textContainer]}>
                  <Text style={[Styles.label]}>{item.value.toUpperCase()}</Text>
                  <Text style={[Styles.text]}>{item.label}</Text>
                </View>
                <View style={[Styles.textContainer]}>
                  <TouchableOpacity
                    style={[
                      this.props.value === item.value
                        ? Styles.circleRounded
                        : Styles.circle,
                    ]}
                    onPress={() => this.handlePress(item.value)}>
                    {this.props.value === item.value && (
                      <View style={Styles.checkedCircle} />
                    )}
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  }
}
