import React, {Component} from 'react';
import {View, Text, Platform} from 'react-native';
import {Item, Picker} from 'native-base';
import {Metrics} from '../../Styles';
import Styles from './Styles/DropdownModal';
import Icon from 'react-native-vector-icons/FontAwesome';
import {SafeAreaView} from 'react-navigation';

import {Modal, TouchableOpacity, Alert, FlatList} from 'react-native';

export default class DropdownModal extends Component {
  constructor(props) {
    super(props);

    // console.log(`Value: -${props.value}- `);
    // //console.log(`Value: ${props.value} ${props.value.length}`);
    // console.log(`placeholder: ${props.placeholder}`);

    this.state = {
      isLoading: true,
      modalVisible: false,

      listOptions: [],

      value:
        props.value === undefined ||
        props.value === null ||
        props.value.length === 0
          ? props.placeholder !== undefined && props.placeholder !== null
            ? props.placeholder
            : 'Select'
          : props.options.filter(value => value.value === props.value)[0].label,
    };

    this.setOptions = this.setOptions.bind();
    this.handlePress = this.handlePress.bind();
  }

  //.
  componentDidMount() {
    this.setOptions();
  }

  //.
  setOptions = () => {
    const options = this.props.options;

    let listOptions = [];

    for (let i = 0; i < options.length; i++) {
      // console.log(`option[${i}] = ${JSON.stringify(options[i])}`);

      listOptions.push({
        label: options[i].label,
        value: options[i].value,
        key: options[i].id,
      });

      // console.log(`data[${i}] = ${JSON.stringify(listOptions)}`);
    }

    this.setState({listOptions: listOptions, isLoading: false});
  };

  //.
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  //.
  handlePress = item => {
    this.setState(
      {value: item.label, modalVisible: false},
      this.props.onValueChange(item.value),
    );
  };

  render() {
    // console.log(`state.value: ${this.state.value}`);
    // console.log(`data: ${JSON.stringify(this.state.listOptions)}`);
    // console.log(`isLoading: ${this.state.isLoading}`);

    return (
      <View>
        <View>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <View>
              <View style={Styles.header}>
                <TouchableOpacity
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Icon name="window-close" color={'#000'} size={32} />
                </TouchableOpacity>
              </View>
              <View>
                <FlatList
                  // ItemSeparatorComponent={this.renderSeparator}
                  ListFooterComponent={
                    <View style={{height: 0, marginBottom: 120}} />
                  }
                  contentInset={{top: 0, bottom: 20, left: 0, right: 0}}
                  contentInsetAdjustmentBehavior="automatic"
                  data={this.state.listOptions}
                  renderItem={({item, index, separators}) => (
                    <TouchableOpacity
                      style={[Styles.listContainer, this.props.itemStyle]}
                      onPress={() => this.handlePress(item)}
                      onShowUnderlay={separators.highlight}
                      onHideUnderlay={separators.unhighlight}>
                      <Text style={Styles.labelItem}>{item.label}</Text>
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
          </Modal>
        </View>
        <View style={[Styles.container, this.props.styleContainer]}>
          <TouchableOpacity
            style={[Styles.inputView, this.props.styleView]}
            onPress={() => {
              this.setModalVisible(true);
            }}>
            <Text style={[Styles.placeholder, this.props.stylePlaceholder]}>
              {!this.props.autoCapitalize && this.state.value}
              {this.props.autoCapitalize && this.state.value.toUpperCase()}
            </Text>
            <View style={[Styles.iconView, this.props.styleIcon]}>
              <Icon
                name="chevron-down"
                color={'#666666'}
                size={16}
                style={Styles.icon}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
