import React from 'react';
import {View, Text} from 'react-native';
import Styles from './Styles/CardTitle';

import Icon from 'react-native-vector-icons/FontAwesome';

const CardTitle = props => {
  return (
    <View style={[Styles.container, props.styleContainer]}>
      {!props.noIcon && (
        <View style={[Styles.iconView]}>
          {props.iconName && (
            <Icon name={props.iconName} color={'#FFFFFF'} size={30} />
          )}
        </View>
      )}
      <View style={Styles.titleView}>
        <Text style={[Styles.title]}>{props.title}</Text>
        {props.subtitle && (
          <Text style={[Styles.subtitle, props.styleSubtitle]}>
            {props.subtitle}
          </Text>
        )}
      </View>
    </View>
  );
};

export default CardTitle;
