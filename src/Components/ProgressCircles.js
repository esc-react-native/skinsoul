/* eslint-disable keyword-spacing */
import React from 'react';
import {View, Text, Image} from 'react-native';
import Styles from './Styles/ProgressCircles';
import {Colors} from '../Styles';

import Icon from 'react-native-vector-icons/FontAwesome';

const ICON_SIZE = 12;

const ProgressCircles = props => {
  return (
    <View style={[Styles.container]}>
      <Icon
        name="circle"
        style={Styles.icon}
        color={
          props.step === 0 ? Colors.progressCircleOn : Colors.progressCircleOff
        }
        size={ICON_SIZE}
      />
      <Icon
        name="circle"
        style={Styles.icon}
        color={
          props.step === 1 ? Colors.progressCircleOn : Colors.progressCircleOff
        }
        size={ICON_SIZE}
      />
      <Icon
        name="circle"
        style={Styles.icon}
        color={
          props.step === 2 ? Colors.progressCircleOn : Colors.progressCircleOff
        }
        size={ICON_SIZE}
      />
      <Icon
        name="circle"
        style={Styles.icon}
        color={
          props.step === 3 ? Colors.progressCircleOn : Colors.progressCircleOff
        }
        size={ICON_SIZE}
      />
    </View>
  );
};

export default ProgressCircles;
