import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Button from './src/components/button';
import {Auth} from 'aws-amplify';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      session: {},
    };

    this.handleSignOut = this.handleSignOut.bind();
  }

  componentDidMount = async () => {
    console.log('Autenticado???');
    console.log(Auth.currentAuthenticatedUser());
    let session = await Auth.currentAuthenticatedUser();
    this.setState(
      {isLoading: true},
      this.setState(
        {session: await Auth.currentAuthenticatedUser()},
        this.setState({isLoading: false}, console.log(session)),
      ),
    );

    if (session.username) this.props.navigation.navigate('Home');
  };

  handleSignOut = async () => {
    console.log('SignOut...');
    try {
      await Auth.signOut();
      this.props.navigation.navigate('Authentication');
    } catch (_err) {
      console.log(_err);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>
          We have {this.props.screenProps.currentFriends.length} friends!
        </Text>
        <Button
          title="Add some friends"
          onPress={() => this.props.navigation.navigate('Friends')}
        />
        <Button title="Salir" onPress={this.handleSignOut} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
