/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App/App';
import {name as appName} from './app.json';

//console.disableYellowBox = true;
import {HideNavigationBar} from 'react-native-navigation-bar-color';

HideNavigationBar();

AppRegistry.registerComponent(appName, () => App);
